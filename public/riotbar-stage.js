var mainBundle = (function () {
  'use strict';
  function t(t) {
    var e = this.constructor;
    return this.then(
      function (n) {
        return e.resolve(t()).then(function () {
          return n;
        });
      },
      function (n) {
        return e.resolve(t()).then(function () {
          return e.reject(n);
        });
      }
    );
  }
  function e(t) {
    return Boolean(t && 'undefined' != typeof t.length);
  }
  function n() {}
  function r(t, e) {
    return function () {
      t.apply(e, arguments);
    };
  }
  function a(t) {
    if (!(this instanceof a)) throw new TypeError('Promises must be constructed via new');
    if ('function' != typeof t) throw new TypeError('not a function');
    (this._state = 0), (this._handled = !1), (this._value = void 0), (this._deferreds = []), u(t, this);
  }
  function o(t, e) {
    for (; 3 === t._state; ) t = t._value;
    return 0 === t._state
      ? void t._deferreds.push(e)
      : ((t._handled = !0),
        void a._immediateFn(function () {
          var n = 1 === t._state ? e.onFulfilled : e.onRejected;
          if (null === n) return void (1 === t._state ? i : s)(e.promise, t._value);
          var r;
          try {
            r = n(t._value);
          } catch (a) {
            return void s(e.promise, a);
          }
          i(e.promise, r);
        }));
  }
  function i(t, e) {
    try {
      if (e === t) throw new TypeError('A promise cannot be resolved with itself.');
      if (e && ('object' == typeof e || 'function' == typeof e)) {
        var n = e.then;
        if (e instanceof a) return (t._state = 3), (t._value = e), void l(t);
        if ('function' == typeof n) return void u(r(n, e), t);
      }
      (t._state = 1), (t._value = e), l(t);
    } catch (o) {
      s(t, o);
    }
  }
  function s(t, e) {
    (t._state = 2), (t._value = e), l(t);
  }
  function l(t) {
    2 === t._state &&
      0 === t._deferreds.length &&
      a._immediateFn(function () {
        t._handled || a._unhandledRejectionFn(t._value);
      });
    for (var e = 0, n = t._deferreds.length; e < n; e++) o(t, t._deferreds[e]);
    t._deferreds = null;
  }
  function c(t, e, n) {
    (this.onFulfilled = 'function' == typeof t ? t : null),
      (this.onRejected = 'function' == typeof e ? e : null),
      (this.promise = n);
  }
  function u(t, e) {
    var n = !1;
    try {
      t(
        function (t) {
          n || ((n = !0), i(e, t));
        },
        function (t) {
          n || ((n = !0), s(e, t));
        }
      );
    } catch (r) {
      if (n) return;
      (n = !0), s(e, r);
    }
  }
  function p(t) {
    return t && DataView.prototype.isPrototypeOf(t);
  }
  function d(t) {
    if (('string' != typeof t && (t = String(t)), /[^a-z0-9\-#$%&'*+.^_`|~]/i.test(t)))
      throw new TypeError('Invalid character in header field name');
    return t.toLowerCase();
  }
  function f(t) {
    return 'string' != typeof t && (t = String(t)), t;
  }
  function h(t) {
    var e = {
      next: function () {
        var e = t.shift();
        return { done: void 0 === e, value: e };
      }
    };
    return (
      Qt.iterable &&
        (e[Symbol.iterator] = function () {
          return e;
        }),
      e
    );
  }
  function b(t) {
    (this.map = {}),
      t instanceof b
        ? t.forEach(function (t, e) {
            this.append(e, t);
          }, this)
        : Array.isArray(t)
        ? t.forEach(function (t) {
            this.append(t[0], t[1]);
          }, this)
        : t &&
          Object.getOwnPropertyNames(t).forEach(function (e) {
            this.append(e, t[e]);
          }, this);
  }
  function m(t) {
    return t.bodyUsed ? Promise.reject(new TypeError('Already read')) : void (t.bodyUsed = !0);
  }
  function g(t) {
    return new Promise(function (e, n) {
      (t.onload = function () {
        e(t.result);
      }),
        (t.onerror = function () {
          n(t.error);
        });
    });
  }
  function v(t) {
    var e = new FileReader(),
      n = g(e);
    return e.readAsArrayBuffer(t), n;
  }
  function _(t) {
    var e = new FileReader(),
      n = g(e);
    return e.readAsText(t), n;
  }
  function w(t) {
    for (var e = new Uint8Array(t), n = new Array(e.length), r = 0; r < e.length; r++) n[r] = String.fromCharCode(e[r]);
    return n.join('');
  }
  function y(t) {
    if (t.slice) return t.slice(0);
    var e = new Uint8Array(t.byteLength);
    return e.set(new Uint8Array(t)), e.buffer;
  }
  function k() {
    return (
      (this.bodyUsed = !1),
      (this._initBody = function (t) {
        (this._bodyInit = t),
          t
            ? 'string' == typeof t
              ? (this._bodyText = t)
              : Qt.blob && Blob.prototype.isPrototypeOf(t)
              ? (this._bodyBlob = t)
              : Qt.formData && FormData.prototype.isPrototypeOf(t)
              ? (this._bodyFormData = t)
              : Qt.searchParams && URLSearchParams.prototype.isPrototypeOf(t)
              ? (this._bodyText = t.toString())
              : Qt.arrayBuffer && Qt.blob && p(t)
              ? ((this._bodyArrayBuffer = y(t.buffer)), (this._bodyInit = new Blob([this._bodyArrayBuffer])))
              : Qt.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(t) || ee(t))
              ? (this._bodyArrayBuffer = y(t))
              : (this._bodyText = t = Object.prototype.toString.call(t))
            : (this._bodyText = ''),
          this.headers.get('content-type') ||
            ('string' == typeof t
              ? this.headers.set('content-type', 'text/plain;charset=UTF-8')
              : this._bodyBlob && this._bodyBlob.type
              ? this.headers.set('content-type', this._bodyBlob.type)
              : Qt.searchParams &&
                URLSearchParams.prototype.isPrototypeOf(t) &&
                this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8'));
      }),
      Qt.blob &&
        ((this.blob = function () {
          var t = m(this);
          if (t) return t;
          if (this._bodyBlob) return Promise.resolve(this._bodyBlob);
          if (this._bodyArrayBuffer) return Promise.resolve(new Blob([this._bodyArrayBuffer]));
          if (this._bodyFormData) throw new Error('could not read FormData body as blob');
          return Promise.resolve(new Blob([this._bodyText]));
        }),
        (this.arrayBuffer = function () {
          return this._bodyArrayBuffer ? m(this) || Promise.resolve(this._bodyArrayBuffer) : this.blob().then(v);
        })),
      (this.text = function () {
        var t = m(this);
        if (t) return t;
        if (this._bodyBlob) return _(this._bodyBlob);
        if (this._bodyArrayBuffer) return Promise.resolve(w(this._bodyArrayBuffer));
        if (this._bodyFormData) throw new Error('could not read FormData body as text');
        return Promise.resolve(this._bodyText);
      }),
      Qt.formData &&
        (this.formData = function () {
          return this.text().then(R);
        }),
      (this.json = function () {
        return this.text().then(JSON.parse);
      }),
      this
    );
  }
  function x(t) {
    var e = t.toUpperCase();
    return ne.indexOf(e) > -1 ? e : t;
  }
  function L(t, e) {
    e = e || {};
    var n = e.body;
    if (t instanceof L) {
      if (t.bodyUsed) throw new TypeError('Already read');
      (this.url = t.url),
        (this.credentials = t.credentials),
        e.headers || (this.headers = new b(t.headers)),
        (this.method = t.method),
        (this.mode = t.mode),
        (this.signal = t.signal),
        n || null == t._bodyInit || ((n = t._bodyInit), (t.bodyUsed = !0));
    } else this.url = String(t);
    if (
      ((this.credentials = e.credentials || this.credentials || 'same-origin'),
      (!e.headers && this.headers) || (this.headers = new b(e.headers)),
      (this.method = x(e.method || this.method || 'GET')),
      (this.mode = e.mode || this.mode || null),
      (this.signal = e.signal || this.signal),
      (this.referrer = null),
      ('GET' === this.method || 'HEAD' === this.method) && n)
    )
      throw new TypeError('Body not allowed for GET or HEAD requests');
    this._initBody(n);
  }
  function R(t) {
    var e = new FormData();
    return (
      t
        .trim()
        .split('&')
        .forEach(function (t) {
          if (t) {
            var n = t.split('='),
              r = n.shift().replace(/\+/g, ' '),
              a = n.join('=').replace(/\+/g, ' ');
            e.append(decodeURIComponent(r), decodeURIComponent(a));
          }
        }),
      e
    );
  }
  function C(t) {
    var e = new b(),
      n = t.replace(/\r?\n[\t ]+/g, ' ');
    return (
      n.split(/\r?\n/).forEach(function (t) {
        var n = t.split(':'),
          r = n.shift().trim();
        if (r) {
          var a = n.join(':').trim();
          e.append(r, a);
        }
      }),
      e
    );
  }
  function M(t, e) {
    e || (e = {}),
      (this.type = 'default'),
      (this.status = void 0 === e.status ? 200 : e.status),
      (this.ok = this.status >= 200 && this.status < 300),
      (this.statusText = 'statusText' in e ? e.statusText : 'OK'),
      (this.headers = new b(e.headers)),
      (this.url = e.url || ''),
      this._initBody(t);
  }
  function E(t, e) {
    return new Promise(function (n, r) {
      function a() {
        i.abort();
      }
      var o = new L(t, e);
      if (o.signal && o.signal.aborted) return r(new ae('Aborted', 'AbortError'));
      var i = new XMLHttpRequest();
      (i.onload = function () {
        var t = { status: i.status, statusText: i.statusText, headers: C(i.getAllResponseHeaders() || '') };
        t.url = 'responseURL' in i ? i.responseURL : t.headers.get('X-Request-URL');
        var e = 'response' in i ? i.response : i.responseText;
        n(new M(e, t));
      }),
        (i.onerror = function () {
          r(new TypeError('Network request failed'));
        }),
        (i.ontimeout = function () {
          r(new TypeError('Network request failed'));
        }),
        (i.onabort = function () {
          r(new ae('Aborted', 'AbortError'));
        }),
        i.open(o.method, o.url, !0),
        'include' === o.credentials ? (i.withCredentials = !0) : 'omit' === o.credentials && (i.withCredentials = !1),
        'responseType' in i && Qt.blob && (i.responseType = 'blob'),
        o.headers.forEach(function (t, e) {
          i.setRequestHeader(e, t);
        }),
        o.signal &&
          (o.signal.addEventListener('abort', a),
          (i.onreadystatechange = function () {
            4 === i.readyState && o.signal.removeEventListener('abort', a);
          })),
        i.send('undefined' == typeof o._bodyInit ? null : o._bodyInit);
    });
  }
  function S(t, e) {
    return (e = { exports: {} }), t(e, e.exports), e.exports;
  }
  function F(t) {
    return (t && t['default']) || t;
  }
  function A(t) {
    return (A =
      'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
        ? function (t) {
            return typeof t;
          }
        : function (t) {
            return t && 'function' == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype
              ? 'symbol'
              : typeof t;
          })(t);
  }
  function P(t) {
    this.wrapped = t;
  }
  function T(t) {
    function e(t, e) {
      return new Promise(function (r, i) {
        var s = { key: t, arg: e, resolve: r, reject: i, next: null };
        o ? (o = o.next = s) : ((a = o = s), n(t, e));
      });
    }
    function n(e, a) {
      try {
        var o = t[e](a),
          i = o.value,
          s = i instanceof P;
        Promise.resolve(s ? i.wrapped : i).then(
          function (t) {
            return s ? void n('next', t) : void r(o.done ? 'return' : 'normal', t);
          },
          function (t) {
            n('throw', t);
          }
        );
      } catch (l) {
        r('throw', l);
      }
    }
    function r(t, e) {
      switch (t) {
        case 'return':
          a.resolve({ value: e, done: !0 });
          break;
        case 'throw':
          a.reject(e);
          break;
        default:
          a.resolve({ value: e, done: !1 });
      }
      (a = a.next), a ? n(a.key, a.arg) : (o = null);
    }
    var a, o;
    (this._invoke = e), 'function' != typeof t['return'] && (this['return'] = void 0);
  }
  function B(t, e) {
    if (!(t instanceof e)) throw new TypeError('Cannot call a class as a function');
  }
  function N(t, e) {
    for (var n = 0; n < e.length; n++) {
      var r = e[n];
      (r.enumerable = r.enumerable || !1),
        (r.configurable = !0),
        'value' in r && (r.writable = !0),
        Object.defineProperty(t, r.key, r);
    }
  }
  function H(t, e, n) {
    return e && N(t.prototype, e), n && N(t, n), t;
  }
  function U(t, e, n) {
    return (
      e in t ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (t[e] = n), t
    );
  }
  function O(t, e) {
    var n = Object.keys(t);
    if (Object.getOwnPropertySymbols) {
      var r = Object.getOwnPropertySymbols(t);
      e &&
        (r = r.filter(function (e) {
          return Object.getOwnPropertyDescriptor(t, e).enumerable;
        })),
        n.push.apply(n, r);
    }
    return n;
  }
  function D(t) {
    for (var e = 1; e < arguments.length; e++) {
      var n = null != arguments[e] ? arguments[e] : {};
      e % 2
        ? O(n, !0).forEach(function (e) {
            U(t, e, n[e]);
          })
        : Object.getOwnPropertyDescriptors
        ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n))
        : O(n).forEach(function (e) {
            Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
          });
    }
    return t;
  }
  function I(t, e) {
    if ('function' != typeof e && null !== e) throw new TypeError('Super expression must either be null or a function');
    (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })),
      e && j(t, e);
  }
  function z(t) {
    return (z = Object.setPrototypeOf
      ? Object.getPrototypeOf
      : function (t) {
          return t.__proto__ || Object.getPrototypeOf(t);
        })(t);
  }
  function j(t, e) {
    return (j =
      Object.setPrototypeOf ||
      function (t, e) {
        return (t.__proto__ = e), t;
      })(t, e);
  }
  function G(t) {
    if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    return t;
  }
  function Z(t, e) {
    return !e || ('object' != typeof e && 'function' != typeof e) ? G(t) : e;
  }
  function V(t, e) {
    return X(t) || q(t, e) || Y();
  }
  function W(t) {
    return K(t) || J(t) || $();
  }
  function K(t) {
    if (Array.isArray(t)) {
      for (var e = 0, n = new Array(t.length); e < t.length; e++) n[e] = t[e];
      return n;
    }
  }
  function X(t) {
    if (Array.isArray(t)) return t;
  }
  function J(t) {
    if (Symbol.iterator in Object(t) || '[object Arguments]' === Object.prototype.toString.call(t))
      return Array.from(t);
  }
  function q(t, e) {
    var n = [],
      r = !0,
      a = !1,
      o = void 0;
    try {
      for (
        var i, s = t[Symbol.iterator]();
        !(r = (i = s.next()).done) && (n.push(i.value), !e || n.length !== e);
        r = !0
      );
    } catch (l) {
      (a = !0), (o = l);
    } finally {
      try {
        r || null == s['return'] || s['return']();
      } finally {
        if (a) throw o;
      }
    }
    return n;
  }
  function $() {
    throw new TypeError('Invalid attempt to spread non-iterable instance');
  }
  function Y() {
    throw new TypeError('Invalid attempt to destructure non-iterable instance');
  }
  function Q(t, e) {
    for (var n in e) t[n] = e[n];
    return t;
  }
  function tt(t) {
    var e = t.parentNode;
    e && e.removeChild(t);
  }
  function et(t, e, n) {
    var r,
      a = arguments,
      o = {};
    for (r in e) 'key' !== r && 'ref' !== r && (o[r] = e[r]);
    if (arguments.length > 3) for (n = [n], r = 3; r < arguments.length; r++) n.push(a[r]);
    if ((null != n && (o.children = n), 'function' == typeof t && null != t.defaultProps))
      for (r in t.defaultProps) void 0 === o[r] && (o[r] = t.defaultProps[r]);
    return nt(t, o, e && e.key, e && e.ref);
  }
  function nt(t, e, n, r) {
    var a = {
      type: t,
      props: e,
      key: n,
      ref: r,
      __k: null,
      __: null,
      __b: 0,
      __e: null,
      __d: null,
      __c: null,
      constructor: void 0
    };
    return ue.vnode && ue.vnode(a), a;
  }
  function rt() {
    return {};
  }
  function at(t) {
    return t.children;
  }
  function ot(t, e) {
    (this.props = t), (this.context = e);
  }
  function it(t, e) {
    if (null == e) return t.__ ? it(t.__, t.__.__k.indexOf(t) + 1) : null;
    for (var n; e < t.__k.length; e++) if (null != (n = t.__k[e]) && null != n.__e) return n.__e;
    return 'function' == typeof t.type ? it(t) : null;
  }
  function st(t) {
    var e, n;
    if (null != (t = t.__) && null != t.__c) {
      for (t.__e = t.__c.base = null, e = 0; e < t.__k.length; e++)
        if (null != (n = t.__k[e]) && null != n.__e) {
          t.__e = t.__c.base = n.__e;
          break;
        }
      return st(t);
    }
  }
  function lt(t) {
    ((!t.__d && (t.__d = !0) && 1 === de.push(t)) || he !== ue.debounceRendering) &&
      ((he = ue.debounceRendering) || fe)(ct);
  }
  function ct() {
    var t, e, n, r, a, o, i;
    for (
      de.sort(function (t, e) {
        return e.__v.__b - t.__v.__b;
      });
      (t = de.pop());

    )
      t.__d &&
        ((n = void 0),
        (r = void 0),
        (o = (a = (e = t).__v).__e),
        (i = e.__P) &&
          ((n = []),
          (r = mt(i, a, Q({}, a), e.__n, void 0 !== i.ownerSVGElement, null, n, null == o ? it(a) : o)),
          gt(n, a),
          r != o && st(a)));
  }
  function ut(t, e, n, r, a, o, i, s, l) {
    var c,
      u,
      p,
      d,
      f,
      h,
      b,
      m = (n && n.__k) || ve,
      g = m.length;
    if (
      (s == ge && (s = null != o ? o[0] : g ? it(n, 0) : null),
      (c = 0),
      (e.__k = pt(e.__k, function (n) {
        if (null != n) {
          if (((n.__ = e), (n.__b = e.__b + 1), null === (p = m[c]) || (p && n.key == p.key && n.type === p.type)))
            m[c] = void 0;
          else
            for (u = 0; u < g; u++) {
              if ((p = m[u]) && n.key == p.key && n.type === p.type) {
                m[u] = void 0;
                break;
              }
              p = null;
            }
          if (
            ((d = mt(t, n, (p = p || ge), r, a, o, i, s, l)),
            (u = n.ref) && p.ref != u && (b || (b = []), p.ref && b.push(p.ref, null, n), b.push(u, n.__c || d, n)),
            null != d)
          ) {
            if ((null == h && (h = d), null != n.__d)) (d = n.__d), (n.__d = null);
            else if (o == p || d != s || null == d.parentNode) {
              t: if (null == s || s.parentNode !== t) t.appendChild(d);
              else {
                for (f = s, u = 0; (f = f.nextSibling) && u < g; u += 2) if (f == d) break t;
                t.insertBefore(d, s);
              }
              'option' == e.type && (t.value = '');
            }
            (s = d.nextSibling), 'function' == typeof e.type && (e.__d = d);
          }
        }
        return c++, n;
      })),
      (e.__e = h),
      null != o && 'function' != typeof e.type)
    )
      for (c = o.length; c--; ) null != o[c] && tt(o[c]);
    for (c = g; c--; ) null != m[c] && wt(m[c], m[c]);
    if (b) for (c = 0; c < b.length; c++) _t(b[c], b[++c], b[++c]);
  }
  function pt(t, e, n) {
    if ((null == n && (n = []), null == t || 'boolean' == typeof t)) e && n.push(e(null));
    else if (Array.isArray(t)) for (var r = 0; r < t.length; r++) pt(t[r], e, n);
    else
      n.push(
        e
          ? e(
              'string' == typeof t || 'number' == typeof t
                ? nt(null, t, null, null)
                : null != t.__e || null != t.__c
                ? nt(t.type, t.props, t.key, null)
                : t
            )
          : t
      );
    return n;
  }
  function dt(t, e, n, r, a) {
    var o;
    for (o in n) o in e || ht(t, o, null, n[o], r);
    for (o in e)
      (a && 'function' != typeof e[o]) || 'value' === o || 'checked' === o || n[o] === e[o] || ht(t, o, e[o], n[o], r);
  }
  function ft(t, e, n) {
    '-' === e[0]
      ? t.setProperty(e, n)
      : (t[e] = 'number' == typeof n && !1 === _e.test(e) ? n + 'px' : null == n ? '' : n);
  }
  function ht(t, e, n, r, a) {
    var o, i, s, l, c;
    if ((a ? 'className' === e && (e = 'class') : 'class' === e && (e = 'className'), 'key' === e || 'children' === e));
    else if ('style' === e)
      if (((o = t.style), 'string' == typeof n)) o.cssText = n;
      else {
        if (('string' == typeof r && ((o.cssText = ''), (r = null)), r)) for (i in r) (n && i in n) || ft(o, i, '');
        if (n) for (s in n) (r && n[s] === r[s]) || ft(o, s, n[s]);
      }
    else
      'o' === e[0] && 'n' === e[1]
        ? ((l = e !== (e = e.replace(/Capture$/, ''))),
          (c = e.toLowerCase()),
          (e = (c in t ? c : e).slice(2)),
          n ? (r || t.addEventListener(e, bt, l), ((t.l || (t.l = {}))[e] = n)) : t.removeEventListener(e, bt, l))
        : 'list' !== e && 'tagName' !== e && 'form' !== e && 'type' !== e && !a && e in t
        ? (t[e] = null == n ? '' : n)
        : 'function' != typeof n &&
          'dangerouslySetInnerHTML' !== e &&
          (e !== (e = e.replace(/^xlink:?/, ''))
            ? null == n || !1 === n
              ? t.removeAttributeNS('http://www.w3.org/1999/xlink', e.toLowerCase())
              : t.setAttributeNS('http://www.w3.org/1999/xlink', e.toLowerCase(), n)
            : null == n || !1 === n
            ? t.removeAttribute(e)
            : t.setAttribute(e, n));
  }
  function bt(t) {
    this.l[t.type](ue.event ? ue.event(t) : t);
  }
  function mt(t, e, n, r, a, o, i, s, l) {
    var c,
      u,
      p,
      d,
      f,
      h,
      b,
      m,
      g,
      v,
      _ = e.type;
    if (void 0 !== e.constructor) return null;
    (c = ue.__b) && c(e);
    try {
      t: if ('function' == typeof _) {
        if (
          ((m = e.props),
          (g = (c = _.contextType) && r[c.__c]),
          (v = c ? (g ? g.props.value : c.__) : r),
          n.__c
            ? (b = (u = e.__c = n.__c).__ = u.__E)
            : ('prototype' in _ && _.prototype.render
                ? (e.__c = u = new _(m, v))
                : ((e.__c = u = new ot(m, v)), (u.constructor = _), (u.render = yt)),
              g && g.sub(u),
              (u.props = m),
              u.state || (u.state = {}),
              (u.context = v),
              (u.__n = r),
              (p = u.__d = !0),
              (u.__h = [])),
          null == u.__s && (u.__s = u.state),
          null != _.getDerivedStateFromProps &&
            (u.__s == u.state && (u.__s = Q({}, u.__s)), Q(u.__s, _.getDerivedStateFromProps(m, u.__s))),
          (d = u.props),
          (f = u.state),
          p)
        )
          null == _.getDerivedStateFromProps && null != u.componentWillMount && u.componentWillMount(),
            null != u.componentDidMount && u.__h.push(u.componentDidMount);
        else {
          if (
            (null == _.getDerivedStateFromProps &&
              m !== d &&
              null != u.componentWillReceiveProps &&
              u.componentWillReceiveProps(m, v),
            !u.__e && null != u.shouldComponentUpdate && !1 === u.shouldComponentUpdate(m, u.__s, v))
          ) {
            for (
              u.props = m,
                u.state = u.__s,
                u.__d = !1,
                u.__v = e,
                e.__e = n.__e,
                e.__k = n.__k,
                u.__h.length && i.push(u),
                c = 0;
              c < e.__k.length;
              c++
            )
              e.__k[c] && (e.__k[c].__ = e);
            break t;
          }
          null != u.componentWillUpdate && u.componentWillUpdate(m, u.__s, v),
            null != u.componentDidUpdate &&
              u.__h.push(function () {
                u.componentDidUpdate(d, f, h);
              });
        }
        (u.context = v),
          (u.props = m),
          (u.state = u.__s),
          (c = ue.__r) && c(e),
          (u.__d = !1),
          (u.__v = e),
          (u.__P = t),
          (c = u.render(u.props, u.state, u.context)),
          (e.__k = pt(null != c && c.type == at && null == c.key ? c.props.children : c)),
          null != u.getChildContext && (r = Q(Q({}, r), u.getChildContext())),
          p || null == u.getSnapshotBeforeUpdate || (h = u.getSnapshotBeforeUpdate(d, f)),
          ut(t, e, n, r, a, o, i, s, l),
          (u.base = e.__e),
          u.__h.length && i.push(u),
          b && (u.__E = u.__ = null),
          (u.__e = null);
      } else e.__e = vt(n.__e, e, n, r, a, o, i, l);
      (c = ue.diffed) && c(e);
    } catch (t) {
      ue.__e(t, e, n);
    }
    return e.__e;
  }
  function gt(t, e) {
    ue.__c && ue.__c(e, t),
      t.some(function (t) {
        try {
          (e = t.__h),
            (t.__h = []),
            e.some(function (e) {
              e.call(t);
            });
        } catch (e) {
          ue.__e(e, t.__v);
        }
      });
  }
  function vt(t, e, n, r, a, o, i, s) {
    var l,
      c,
      u,
      p,
      d,
      f = n.props,
      h = e.props;
    if (((a = 'svg' === e.type || a), null == t && null != o))
      for (l = 0; l < o.length; l++)
        if (null != (c = o[l]) && (null === e.type ? 3 === c.nodeType : c.localName === e.type)) {
          (t = c), (o[l] = null);
          break;
        }
    if (null == t) {
      if (null === e.type) return document.createTextNode(h);
      (t = a ? document.createElementNS('http://www.w3.org/2000/svg', e.type) : document.createElement(e.type)),
        (o = null);
    }
    if (null === e.type) null != o && (o[o.indexOf(t)] = null), f !== h && t.data != h && (t.data = h);
    else if (e !== n) {
      if (
        (null != o && (o = ve.slice.call(t.childNodes)),
        (u = (f = n.props || ge).dangerouslySetInnerHTML),
        (p = h.dangerouslySetInnerHTML),
        !s)
      ) {
        if (f === ge) for (f = {}, d = 0; d < t.attributes.length; d++) f[t.attributes[d].name] = t.attributes[d].value;
        (p || u) && ((p && u && p.__html == u.__html) || (t.innerHTML = (p && p.__html) || ''));
      }
      dt(t, h, f, a, s),
        (e.__k = e.props.children),
        p || ut(t, e, n, r, 'foreignObject' !== e.type && a, o, i, ge, s),
        s ||
          ('value' in h && void 0 !== h.value && h.value !== t.value && (t.value = null == h.value ? '' : h.value),
          'checked' in h && void 0 !== h.checked && h.checked !== t.checked && (t.checked = h.checked));
    }
    return t;
  }
  function _t(t, e, n) {
    try {
      'function' == typeof t ? t(e) : (t.current = e);
    } catch (t) {
      ue.__e(t, n);
    }
  }
  function wt(t, e, n) {
    var r, a, o;
    if (
      (ue.unmount && ue.unmount(t),
      (r = t.ref) && ((r.current && r.current !== t.__e) || _t(r, null, e)),
      n || 'function' == typeof t.type || (n = null != (a = t.__e)),
      (t.__e = t.__d = null),
      null != (r = t.__c))
    ) {
      if (r.componentWillUnmount)
        try {
          r.componentWillUnmount();
        } catch (t) {
          ue.__e(t, e);
        }
      r.base = r.__P = null;
    }
    if ((r = t.__k)) for (o = 0; o < r.length; o++) r[o] && wt(r[o], e, n);
    null != a && tt(a);
  }
  function yt(t, e, n) {
    return this.constructor(t, n);
  }
  function kt(t, e, n) {
    var r, a, o;
    ue.__ && ue.__(t, e),
      (a = (r = n === be) ? null : (n && n.__k) || e.__k),
      (t = et(at, null, [t])),
      (o = []),
      mt(
        e,
        ((r ? e : n || e).__k = t),
        a || ge,
        ge,
        void 0 !== e.ownerSVGElement,
        n && !r ? [n] : a ? null : ve.slice.call(e.childNodes),
        o,
        n || ge,
        r
      ),
      gt(o, t);
  }
  function xt(t, e) {
    kt(t, e, be);
  }
  function Lt(t) {
    var e = {},
      n = {
        __c: '__cC' + me++,
        __: t,
        Consumer: function (t, e) {
          return t.children(e);
        },
        Provider: function (t) {
          var r,
            a = this;
          return (
            this.getChildContext ||
              ((r = []),
              (this.getChildContext = function () {
                return (e[n.__c] = a), e;
              }),
              (this.shouldComponentUpdate = function (e) {
                t.value !== e.value &&
                  r.some(function (t) {
                    (t.context = e.value), lt(t);
                  });
              }),
              (this.sub = function (t) {
                r.push(t);
                var e = t.componentWillUnmount;
                t.componentWillUnmount = function () {
                  r.splice(r.indexOf(t), 1), e && e.call(t);
                };
              })),
            t.children
          );
        }
      };
    return (n.Consumer.contextType = n), n;
  }
  function Rt(t) {
    ue.__h && ue.__h(ye);
    var e = ye.__H || (ye.__H = { t: [], u: [] });
    return t >= e.t.length && e.t.push({}), e.t[t];
  }
  function Ct(t) {
    return Mt(Ht, t);
  }
  function Mt(t, e, n) {
    var r = Rt(we++);
    return (
      r.__c ||
        ((r.__c = ye),
        (r.i = [
          n ? n(e) : Ht(void 0, e),
          function (e) {
            var n = t(r.i[0], e);
            r.i[0] !== n && ((r.i[0] = n), r.__c.setState({}));
          }
        ])),
      r.i
    );
  }
  function Et(t, e) {
    var n = Rt(we++);
    Nt(n.o, e) && ((n.i = t), (n.o = e), ye.__H.u.push(n));
  }
  function St(t, e) {
    var n = Rt(we++);
    return Nt(n.o, e) ? ((n.o = e), (n.v = t), (n.i = t())) : n.i;
  }
  function Ft(t, e) {
    return St(function () {
      return t;
    }, e);
  }
  function At(t) {
    var e = ye.context[t.__c];
    if (!e) return t.__;
    var n = Rt(we++);
    return null == n.i && ((n.i = !0), e.sub(ye)), e.props.value;
  }
  function Pt() {
    hn.some(function (t) {
      t.__P && (t.__H.u.forEach(Tt), t.__H.u.forEach(Bt), (t.__H.u = []));
    }),
      (hn = []);
  }
  function Tt(t) {
    t.m && t.m();
  }
  function Bt(t) {
    var e = t.i();
    'function' == typeof e && (t.m = e);
  }
  function Nt(t, e) {
    return (
      !t ||
      e.some(function (e, n) {
        return e !== t[n];
      })
    );
  }
  function Ht(t, e) {
    return 'function' == typeof e ? e(t) : e;
  }
  function Ut(t, e) {
    for (var n in e) t[n] = e[n];
    return t;
  }
  function Ot(t, e) {
    for (var n in t) if ('__source' !== n && !(n in e)) return !0;
    for (var r in e) if ('__source' !== r && t[r] !== e[r]) return !0;
    return !1;
  }
  function Dt(t) {
    return t && (((t = Ut({}, t)).__c = null), (t.__k = t.__k && t.__k.map(Dt))), t;
  }
  function It(t) {
    (this.__u = 0), (this.__b = null);
  }
  function zt(t) {
    var e = t.__.__c;
    return e && e.o && e.o(t);
  }
  function jt() {
    (this.u = null), (this.i = null);
  }
  function Gt(t) {
    var e = this,
      n = t.container,
      r = et(kn, { context: e.context }, t.vnode);
    return (
      e.s && e.s !== n && (e.h.parentNode && e.s.removeChild(e.h), wt(e.v), (e.p = !1)),
      t.vnode
        ? e.p
          ? ((n.__k = e.__k), kt(r, n), (e.__k = n.__k))
          : ((e.h = document.createTextNode('')),
            xt('', n),
            n.appendChild(e.h),
            (e.p = !0),
            (e.s = n),
            kt(r, n, e.h),
            (e.__k = this.h.__k))
        : e.p && (e.h.parentNode && e.s.removeChild(e.h), wt(e.v)),
      (e.v = r),
      (e.componentWillUnmount = function () {
        e.h.parentNode && e.s.removeChild(e.h), wt(e.v);
      }),
      null
    );
  }
  function Zt(t, e) {
    return et(Gt, { vnode: t, container: e });
  }
  function Vt(t, e) {
    t['UNSAFE_' + e] &&
      !t[e] &&
      Object.defineProperty(t, e, {
        configurable: !1,
        get: function () {
          return this['UNSAFE_' + e];
        },
        set: function (t) {
          this['UNSAFE_' + e] = t;
        }
      });
  }
  function Wt(t) {
    switch ((t = t.toLowerCase())) {
      case 'mobile':
        return 'platform_phone';
      case 'mac':
        return 'platform_macintosh';
      default:
        return 'platform_' + t;
    }
  }
  function Kt(t) {
    var e = t.authenticatedLinks,
      n = t.accountHandler,
      r = Ct(!1),
      a = V(r, 2),
      o = a[0],
      i = a[1],
      s = At(Hn),
      l = Ft(
        function (t) {
          (window.innerWidth > 1024 && window.RiotBar.config.mobileResponsive) ||
            (i(!o), xe.toggleClass(t.currentTarget.parentElement, 'active', o));
        },
        [i, o]
      ),
      c = s.name || '내 정보';
    return et(
      at,
      null,
      et(
        'div',
        { id: 'riotbar-account-bar', onClick: l, className: o ? 'active' : '' },
        et(
          'div',
          { className: 'riotbar-summoner-info' },
          et('div', { className: 'riotbar-summoner-name' }, c, et(Je, { color: Ge.getAccentColor() }))
        )
      ),
      et(
        'div',
        { id: 'riotbar-account-dropdown' },
        et(
          'div',
          { className: 'riotbar-account-info' },
          et('div', { id: 'riotbar-account-dropdown-plugins' }),
          et('div', { className: 'riotbar-summoner-info' }, et('div', { className: 'riotbar-summoner-name' }, c))
        ),
        et('div', { id: 'riotbar-account-dropdown-links' }, et(Nn, { accountHandler: n, authenticatedLinks: e }))
      )
    );
  }
  function Xt(t) {
    var e = t.authenticatedLinks,
      n = window.RiotBar.__accountUtils.accountComponentProps(),
      r = n.anonymousLinks,
      a = RiotBar.config.account;
    if (!a.enabled) return null;
    var o = [Ze.global.theme + '-theme'],
      i = At(Hn),
      s = function (t) {
        var e = t.target,
          n = e.getAttribute('data-riotbar-account-action');
        if (n) {
          t.preventDefault();
          try {
            RiotBar.account[n]();
          } catch (r) {
            xe.logError(r);
          }
        }
      };
    return et(
      'div',
      { id: 'riotbar-account', className: o.join(' ') },
      i.isAuthenticated
        ? et(Kt, { accountHandler: s, authenticatedLinks: e })
        : et(Un, { accountHandler: s, anonymousLinks: r })
    );
  }
  function Jt(t) {
    var e = t.children,
      n = Ct(window.RiotBar.account.getAuthState()),
      r = V(n, 2),
      a = r[0],
      o = r[1];
    return (
      Et(
        function () {
          return (
            document.addEventListener(zn.type, function () {
              var t = Object.create(RiotBar.account.getAuthState());
              o(t);
            }),
            function () {
              document.removeEventListener(zn.type);
            }
          );
        },
        [o]
      ),
      et(Hn.Provider, { value: a }, e)
    );
  }
  function qt() {
    var t = xe.window.riotBarConfig || {},
      e = t.account || {},
      n = e.authMode || 'prod';
    return 'stage' === n ? 'stage.login.i.' : 'login.';
  }
  function $t(t, e) {
    for (var n = t.split('&'), r = !1, a = 0; a < n.length; a++)
      0 === n[a].indexOf(e) && (r = n[a].substr(e.length + 1));
    return r;
  }
  var Yt = setTimeout;
  (a.prototype['catch'] = function (t) {
    return this.then(null, t);
  }),
    (a.prototype.then = function (t, e) {
      var r = new this.constructor(n);
      return o(this, new c(t, e, r)), r;
    }),
    (a.prototype['finally'] = t),
    (a.all = function (t) {
      return new a(function (n, r) {
        function a(t, e) {
          try {
            if (e && ('object' == typeof e || 'function' == typeof e)) {
              var s = e.then;
              if ('function' == typeof s)
                return void s.call(
                  e,
                  function (e) {
                    a(t, e);
                  },
                  r
                );
            }
            (o[t] = e), 0 === --i && n(o);
          } catch (l) {
            r(l);
          }
        }
        if (!e(t)) return r(new TypeError('Promise.all accepts an array'));
        var o = Array.prototype.slice.call(t);
        if (0 === o.length) return n([]);
        for (var i = o.length, s = 0; s < o.length; s++) a(s, o[s]);
      });
    }),
    (a.resolve = function (t) {
      return t && 'object' == typeof t && t.constructor === a
        ? t
        : new a(function (e) {
            e(t);
          });
    }),
    (a.reject = function (t) {
      return new a(function (e, n) {
        n(t);
      });
    }),
    (a.race = function (t) {
      return new a(function (n, r) {
        if (!e(t)) return r(new TypeError('Promise.race accepts an array'));
        for (var o = 0, i = t.length; o < i; o++) a.resolve(t[o]).then(n, r);
      });
    }),
    (a._immediateFn =
      ('function' == typeof setImmediate &&
        function (t) {
          setImmediate(t);
        }) ||
      function (t) {
        Yt(t, 0);
      }),
    (a._unhandledRejectionFn = function (t) {
      'undefined' != typeof console && console && console.warn('Possible Unhandled Promise Rejection:', t);
    });
  var Qt = {
    searchParams: 'URLSearchParams' in self,
    iterable: 'Symbol' in self && 'iterator' in Symbol,
    blob:
      'FileReader' in self &&
      'Blob' in self &&
      (function () {
        try {
          return new Blob(), !0;
        } catch (t) {
          return !1;
        }
      })(),
    formData: 'FormData' in self,
    arrayBuffer: 'ArrayBuffer' in self
  };
  if (Qt.arrayBuffer)
    var te = [
        '[object Int8Array]',
        '[object Uint8Array]',
        '[object Uint8ClampedArray]',
        '[object Int16Array]',
        '[object Uint16Array]',
        '[object Int32Array]',
        '[object Uint32Array]',
        '[object Float32Array]',
        '[object Float64Array]'
      ],
      ee =
        ArrayBuffer.isView ||
        function (t) {
          return t && te.indexOf(Object.prototype.toString.call(t)) > -1;
        };
  (b.prototype.append = function (t, e) {
    (t = d(t)), (e = f(e));
    var n = this.map[t];
    this.map[t] = n ? n + ', ' + e : e;
  }),
    (b.prototype['delete'] = function (t) {
      delete this.map[d(t)];
    }),
    (b.prototype.get = function (t) {
      return (t = d(t)), this.has(t) ? this.map[t] : null;
    }),
    (b.prototype.has = function (t) {
      return this.map.hasOwnProperty(d(t));
    }),
    (b.prototype.set = function (t, e) {
      this.map[d(t)] = f(e);
    }),
    (b.prototype.forEach = function (t, e) {
      for (var n in this.map) this.map.hasOwnProperty(n) && t.call(e, this.map[n], n, this);
    }),
    (b.prototype.keys = function () {
      var t = [];
      return (
        this.forEach(function (e, n) {
          t.push(n);
        }),
        h(t)
      );
    }),
    (b.prototype.values = function () {
      var t = [];
      return (
        this.forEach(function (e) {
          t.push(e);
        }),
        h(t)
      );
    }),
    (b.prototype.entries = function () {
      var t = [];
      return (
        this.forEach(function (e, n) {
          t.push([n, e]);
        }),
        h(t)
      );
    }),
    Qt.iterable && (b.prototype[Symbol.iterator] = b.prototype.entries);
  var ne = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'];
  (L.prototype.clone = function () {
    return new L(this, { body: this._bodyInit });
  }),
    k.call(L.prototype),
    k.call(M.prototype),
    (M.prototype.clone = function () {
      return new M(this._bodyInit, {
        status: this.status,
        statusText: this.statusText,
        headers: new b(this.headers),
        url: this.url
      });
    }),
    (M.error = function () {
      var t = new M(null, { status: 0, statusText: '' });
      return (t.type = 'error'), t;
    });
  var re = [301, 302, 303, 307, 308];
  M.redirect = function (t, e) {
    if (re.indexOf(e) === -1) throw new RangeError('Invalid status code');
    return new M(null, { status: e, headers: { location: t } });
  };
  var ae = self.DOMException;
  try {
    new ae();
  } catch (oe) {
    (ae = function (t, e) {
      (this.message = t), (this.name = e);
      var n = Error(t);
      this.stack = n.stack;
    }),
      (ae.prototype = Object.create(Error.prototype)),
      (ae.prototype.constructor = ae);
  }
  (E.polyfill = !0),
    self.fetch || ((self.fetch = E), (self.Headers = b), (self.Request = L), (self.Response = M)),
    window && !window.Promise && (window.Promise = a),
    String.prototype.includes ||
      (String.prototype.includes = function () {
        return String.prototype.indexOf.apply(this, arguments) !== -1;
      }),
    Array.prototype.includes ||
      (Array.prototype.includes = function (t) {
        return !!~this.indexOf(t);
      }),
    (function (t) {
      t.forEach(function (t) {
        t.hasOwnProperty('prepend') ||
          Object.defineProperty(t, 'prepend', {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            value: function () {
              var t = Array.prototype.slice.call(arguments),
                e = document.createDocumentFragment();
              t.forEach(function (t) {
                var n = t instanceof Node;
                e.appendChild(n ? t : document.createTextNode(String(t)));
              }),
                this.insertBefore(e, this.firstChild);
            }
          });
      });
    })([Element.prototype, Document.prototype, DocumentFragment.prototype]),
    Array.prototype.find ||
      Object.defineProperty(Array.prototype, 'find', {
        value: function (t) {
          if (null == this) throw TypeError('"this" is null or not defined');
          var e = Object(this),
            n = e.length >>> 0;
          if ('function' != typeof t) throw TypeError('predicate must be a function');
          for (var r = arguments[1], a = 0; a < n; ) {
            var o = e[a];
            if (t.call(r, o, a, e)) return o;
            a++;
          }
        },
        configurable: !0,
        writable: !0
      });
  var ie =
      (Object.freeze({}),
      '@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/8a533745-7bc5-4dcd-8552-d9952208de6f.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/8a533745-7bc5-4dcd-8552-d9952208de6f.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/ffbb4591-1a9c-443b-9b6e-6ceb6a3ca76b.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/e44c8656-a389-4ecb-838c-3c135565d6b3.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/fcdcba61-8d4e-41db-9c06-24a5238587f1.ttf") format("truetype");\n  font-weight: 400;\n  font-style: normal;\n}\n@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/54af9b67-908f-49ac-8aa0-d3959c0e28dc.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/54af9b67-908f-49ac-8aa0-d3959c0e28dc.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/b821b539-3f6c-4ee1-8d8d-c331fb7aedce.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/7368a75b-895f-4b33-ac15-1364f4ff3f9f.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/9cd874e4-d629-4f8a-8760-b877ec0e5d9e.ttf") format("truetype");\n  font-weight: 400;\n  font-style: italic;\n}\n@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/0f774eeb-e868-4bd6-9055-79542afd5208.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/0f774eeb-e868-4bd6-9055-79542afd5208.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/cdb13a7a-2f13-4f7c-b7a3-01b4ccef574d.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/40a8594b-08b2-4a38-97d9-958c95360b20.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/bcc8880e-b967-43ce-a210-d1404cbdc736.ttf") format("truetype");\n  font-weight: 500;\n  font-style: normal;\n}\n@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/653875a7-77e2-4995-97f2-4c9de734eb69.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/653875a7-77e2-4995-97f2-4c9de734eb69.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/445f40df-cbad-41e8-92eb-b4438eb872fc.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/ed8af8bb-2ddb-4128-a83b-837173705425.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/7401384a-83d5-4f49-a886-089029ce641c.ttf") format("truetype");\n  font-weight: 500;\n  font-style: italic;\n}\n@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/5cd9741a-b33e-4cd7-a197-e850a6e920b2.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/5cd9741a-b33e-4cd7-a197-e850a6e920b2.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/601fdeac-544b-4132-8e0d-f24e0a72e489.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/680a604b-6cec-4a82-8d1c-3a77fb66cee5.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/ef6af46e-3064-450c-9902-48bb726bd026.ttf") format("truetype");\n  font-weight: 800;\n  font-style: normal;\n}\n@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/96eee7b7-99e1-4bc6-9099-86d14aa4b25a.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/96eee7b7-99e1-4bc6-9099-86d14aa4b25a.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/ec5c34fc-3ad0-4147-9b77-e978a00b7653.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/e2a9f569-6858-48b4-b5c0-30c014123cd1.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/fb00b17a-2053-49b7-9d53-68cf5c842ba1.ttf") format("truetype");\n  font-weight: 800;\n  font-style: italic;\n}\n@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/69d25c38-dbab-4326-9230-923d3c18889b.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/69d25c38-dbab-4326-9230-923d3c18889b.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/7bc581c3-bb28-4f5d-a9c5-3018fcfbfbd9.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/4e811424-cd97-4afb-bf5a-965c3b39905d.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/124573c7-0c5e-4c89-8a3a-4ee9aee5d3a9.ttf") format("truetype");\n  font-weight: 900;\n  font-style: normal;\n}\n@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/b3dc84d3-a366-4d54-85cd-a4a909be2322.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/b3dc84d3-a366-4d54-85cd-a4a909be2322.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/e5071d6e-c3d6-4c88-8042-a4c33b65387f.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/53426c00-fe27-497e-bafe-d62c9c2f02b5.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/1d0866b5-0d7c-4fde-988c-c45899a3503f.ttf") format("truetype");\n  font-weight: 900;\n  font-style: italic;\n}\n@font-face {\n  font-family: "FF Mark W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/b9275bbe-5494-4561-8869-49b8b4213b0e.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/b9275bbe-5494-4561-8869-49b8b4213b0e.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/e6f5bc67-2b84-4b0a-b32a-0ec6c2b8634e.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/b428d7d4-bc34-4bdf-a27b-13bf549f613c.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/ffmark/2864b8c0-7389-464a-845c-23d708d5665c.ttf") format("truetype");\n  font-weight: 950;\n  font-style: normal;\n}\n@font-face {\n  font-family: "Cairo";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Regular.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Regular.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Regular.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Regular.ttf") format("truetype"), url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Regular.svg#") format("svg");\n  font-weight: 1 699;\n}\n@font-face {\n  font-family: "Cairo";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Bold.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Bold.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Bold.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Bold.ttf") format("truetype"), url("https://lolstatic-a.akamaihd.net/awesomefonts/1.0.0/Fonts/Cairo-Bold.svg#") format("svg");\n  font-weight: 700 999;\n}\n@font-face {\n  font-family: "Neue Frutiger World W05_n4";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/2b60fc6c-fa8d-43f1-a9b8-6c5c77815ab6.eot?#iefix") format("eot");\n}\n@font-face {\n  font-family: "Neue Frutiger World W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/2b60fc6c-fa8d-43f1-a9b8-6c5c77815ab6.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/2b60fc6c-fa8d-43f1-a9b8-6c5c77815ab6.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/8bdf6867-c434-4e1e-b0cd-8653db148cd9.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/75ffdee7-26c9-43d8-9fcd-6383663c6891.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/f455f05f-2737-4d93-b58a-f15fc8dbd9ec.ttf") format("truetype");\n  font-weight: 400;\n  font-style: normal;\n}\n@font-face {\n  font-family: "Neue Frutiger World W05_i4";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/43d95c29-a881-4e0b-92d0-4d82b49bb518.eot?#iefix") format("eot");\n}\n@font-face {\n  font-family: "Neue Frutiger World W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/43d95c29-a881-4e0b-92d0-4d82b49bb518.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/43d95c29-a881-4e0b-92d0-4d82b49bb518.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/37724264-30a8-41d0-922a-c16f9941551e.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/62af9f13-d1bd-4e6e-bab1-57fc299d990c.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/93db667a-1eb8-41e9-8552-260c3665e7a5.ttf") format("truetype");\n  font-weight: 400;\n  font-style: italic;\n}\n@font-face {\n  font-family: "Neue Frutiger World W05_n7";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/39e3636b-a1e0-405f-b107-b8c085dbcab4.eot?#iefix") format("eot");\n}\n@font-face {\n  font-family: "Neue Frutiger World W05";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/39e3636b-a1e0-405f-b107-b8c085dbcab4.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/39e3636b-a1e0-405f-b107-b8c085dbcab4.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/cfbc054a-704d-4ef8-bdff-935a38de18ed.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/829467b2-67b5-4c02-b47c-25da7513a62f.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/neuefruitegerworld/8a7d27b5-8d29-46fe-84c5-2f6e0b345b83.ttf") format("truetype");\n  font-weight: 700;\n  font-style: normal;\n}\n@font-face {\n  font-family: "RixSGo";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.eot?#iefix") format("eot");\n}\n@font-face {\n  font-family: "RixSGo";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.ttf") format("truetype");\n  font-weight: 400;\n  font-style: normal;\n}\n@font-face {\n  font-family: "RixSGo";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoM.ttf") format("truetype");\n  font-weight: 400;\n  font-style: italic;\n}\n@font-face {\n  font-family: "RixSGo";\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoB.eot?#iefix");\n  src: url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoB.eot?#iefix") format("eot"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoB.woff2") format("woff2"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoB.woff") format("woff"), url("https://lolstatic-a.akamaihd.net/awesomefonts/Fonts/rixsg/RixSGoB.ttf") format("truetype");\n  font-weight: 700;\n  font-style: normal;\n}'),
    se = Object.freeze({
      default: ie
    }),
    le =
      'undefined' != typeof globalThis
        ? globalThis
        : 'undefined' != typeof window
        ? window
        : 'undefined' != typeof global
        ? global
        : 'undefined' != typeof self
        ? self
        : {},
    ce = S(function (t) {
      !(function (e, n) {
        t.exports ? (t.exports = n()) : (e.svg4everybody = n());
      })(le, function () {
        function t(t, e, n) {
          if (n) {
            var r = document.createDocumentFragment(),
              a = !e.hasAttribute('viewBox') && n.getAttribute('viewBox');
            a && e.setAttribute('viewBox', a);
            for (var o = n.cloneNode(!0); o.childNodes.length; ) r.appendChild(o.firstChild);
            t.appendChild(r);
          }
        }
        function e(e) {
          (e.onreadystatechange = function () {
            if (4 === e.readyState) {
              var n = e._cachedDocument;
              n ||
                ((n = e._cachedDocument = document.implementation.createHTMLDocument('')),
                (n.body.innerHTML = e.responseText),
                (e._cachedTarget = {})),
                e._embeds.splice(0).map(function (r) {
                  var a = e._cachedTarget[r.id];
                  a || (a = e._cachedTarget[r.id] = n.getElementById(r.id)), t(r.parent, r.svg, a);
                });
            }
          }),
            e.onreadystatechange();
        }
        function n(n) {
          function a() {
            for (var n = 0; n < h.length; ) {
              var s = h[n],
                l = s.parentNode,
                c = r(l),
                u = s.getAttribute('data-href') || s.getAttribute('xlink:href') || s.getAttribute('href');
              if ((!u && i.attributeName && (u = s.getAttribute(i.attributeName)), c && u)) {
                if (o)
                  if (!i.validate || i.validate(u, c, s)) {
                    l.removeChild(s);
                    var p = u.split('#'),
                      m = p.shift(),
                      g = p.join('#');
                    if (m.length) {
                      var v = d[m];
                      v || ((v = d[m] = new XMLHttpRequest()), v.open('GET', m), v.send(), (v._embeds = [])),
                        v._embeds.push({ parent: l, svg: c, id: g }),
                        e(v);
                    } else t(l, c, document.getElementById(g));
                  } else ++n, ++b;
              } else ++n;
            }
            (!h.length || h.length - b > 0) && f(a, 67);
          }
          var o,
            i = Object(n),
            s = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/,
            l = /\bAppleWebKit\/(\d+)\b/,
            c = /\bEdge\/12\.(\d+)\b/,
            u = /\bEdge\/.(\d+)\b/,
            p = window.top !== window.self;
          o =
            'polyfill' in i
              ? i.polyfill
              : s.test(navigator.userAgent) ||
                (navigator.userAgent.match(c) || [])[1] < 10547 ||
                (navigator.userAgent.match(l) || [])[1] < 537 ||
                (u.test(navigator.userAgent) && p);
          var d = {},
            f = window.requestAnimationFrame || setTimeout,
            h = document.getElementsByTagName('use'),
            b = 0;
          o && a();
        }
        function r(t) {
          for (var e = t; 'svg' !== e.nodeName.toLowerCase() && (e = e.parentNode); );
          return e;
        }
        return n;
      });
    });
  'function' == typeof Symbol &&
    Symbol.asyncIterator &&
    (T.prototype[Symbol.asyncIterator] = function () {
      return this;
    }),
    (T.prototype.next = function (t) {
      return this._invoke('next', t);
    }),
    (T.prototype['throw'] = function (t) {
      return this._invoke('throw', t);
    }),
    (T.prototype['return'] = function (t) {
      return this._invoke('return', t);
    });
  var ue,
    pe,
    de,
    fe,
    he,
    be,
    me,
    ge = {},
    ve = [],
    _e = /acit|ex(?:s|g|n|p|$)|rph|grid|ows|mnc|ntw|ine[ch]|zoo|^ord/i;
  (ue = {
    __e: function (t, e) {
      for (var n, r; (e = e.__); )
        if ((n = e.__c) && !n.__)
          try {
            if (
              (n.constructor &&
                null != n.constructor.getDerivedStateFromError &&
                ((r = !0), n.setState(n.constructor.getDerivedStateFromError(t))),
              null != n.componentDidCatch && ((r = !0), n.componentDidCatch(t)),
              r)
            )
              return lt((n.__E = n));
          } catch (e) {
            t = e;
          }
      throw t;
    }
  }),
    (pe = function (t) {
      return null != t && void 0 === t.constructor;
    }),
    (ot.prototype.setState = function (t, e) {
      var n;
      (n = this.__s !== this.state ? this.__s : (this.__s = Q({}, this.state))),
        'function' == typeof t && (t = t(n, this.props)),
        t && Q(n, t),
        null != t && this.__v && ((this.__e = !1), e && this.__h.push(e), lt(this));
    }),
    (ot.prototype.forceUpdate = function (t) {
      this.__v && ((this.__e = !0), t && this.__h.push(t), lt(this));
    }),
    (ot.prototype.render = at),
    (de = []),
    (fe = 'function' == typeof Promise ? Promise.prototype.then.bind(Promise.resolve()) : setTimeout),
    (be = ge),
    (me = 0);
  var we,
    ye,
    ke,
    xe = S(function (t) {
      var e = (t.exports = {
        window: window,
        deepOverride: function (t, n, r) {
          var a = {};
          void 0 === r && (r = 1 / 0);
          for (var o in t)
            if (t.hasOwnProperty(o)) {
              var i = null !== t[o],
                s = 'object' === A(t[o]);
              (s = s && !e.isArray(t[o])),
                void 0 === n
                  ? (a[o] = t[o])
                  : r > 0 && i && s
                  ? (a[o] = e.deepOverride(t[o], n[o], r - 1))
                  : n.hasOwnProperty(o)
                  ? void 0 !== n[o] && (a[o] = n[o])
                  : (a[o] = t[o]);
            }
          return a;
        },
        isArray: function (t) {
          return Array.isArray ? Array.isArray(t) : '[object Array]' === Object.prototype.toString.call(t);
        },
        isEmptyObject: function (t) {
          var e;
          for (e in t) if (t.hasOwnProperty(e)) return !1;
          return null !== t;
        },
        map: function (t, e) {
          if (null !== t) {
            var n = [];
            if (Object.keys)
              for (var r = Object.keys(t), a = r.length, o = 0; o < a; o++) {
                var i = r[o];
                n.push(e(i, t[i]));
              }
            else for (var s in t) t.hasOwnProperty(s) && n.push(e(s, t[s]));
            return n;
          }
        },
        filter: function (t, e) {
          if (null !== t && this.isArray(t)) {
            for (var n = [], r = 0; r < t.length; r++) e(t[r]) && n.push(t[r]);
            return n;
          }
        },
        nextTick: function (t) {
          setTimeout(t, 0);
        },
        appendStyles: function (t) {
          var e = document.head || document.getElementsByTagName('head')[0],
            n = document.createElement('style');
          if (((n.type = 'text/css'), e.appendChild(n), n.styleSheet)) {
            n.styleSheet.cssText = t;
            try {
              n.innerHTML = t;
            } catch (r) {}
          } else n.appendChild(document.createTextNode(t));
        },
        appendScript: function (t) {
          var e = document.head || document.getElementsByTagName('head')[0],
            n = document.createElement('script');
          (n.type = 'text/javascript'), (n.src = t), e.appendChild(n);
        },
        ensureScript: function (t, n) {
          var r = n.interval ? n.interval : 10,
            a = !1,
            o = !1,
            i = function s() {
              for (var i = !0, l = t.split('.'), c = window, u = 0; i && u < l.length; u++)
                (i = c.hasOwnProperty ? c.hasOwnProperty(l[u]) : !!c[l[u]]), (c = c[l[u]]);
              i ? ((a = !0), o && o.call()) : (n.url && (e.appendScript(n.url), delete n.url), setTimeout(s, r));
            };
          return (
            i(),
            {
              then: function (t) {
                a ? t.call() : (o = t);
              }
            }
          );
        },
        hasClass: function (t, e) {
          var n = ' ' + t.className + ' ',
            r = ' ' + e + ' ';
          return n.indexOf(r) > -1;
        },
        toggleClass: function (t, n, r) {
          if (t) {
            var a = 'boolean' == typeof r ? !r : e.hasClass(t, n);
            a ? e.removeClass(t, n) : e.addClass(t, n);
          }
        },
        addClass: function (t, e) {
          if (t) {
            var n = t.className.split(/\s+/);
            n.indexOf(e) === -1 && (n.splice(n.length, 0, e), (t.className = n.join(' ')));
          }
        },
        removeClass: function (t, e) {
          if (t)
            for (var n = t.className.split(/\s+/), r = n.length; r >= 0; r--)
              n[r] === e && (n.splice(r, 1), (t.className = n.join(' ')));
        },
        getCurrentDomain: function (t) {
          var n = e.window.location.hostname,
            r = /([a-z]+\.[a-z]{2,4}(\.[a-z]{2,4})?)$/i,
            a = n.match(r);
          return null !== a ? a[1] : t;
        },
        createCORSRequest: function (t, e) {
          var n = new XMLHttpRequest();
          return (
            'withCredentials' in n
              ? n.open(t, e, !0)
              : 'undefined' != typeof XDomainRequest
              ? ((n = new XDomainRequest()), n.open(t, e))
              : (n = null),
            n
          );
        },
        addEvent: function (t, n, r) {
          t &&
            (t.attachEvent
              ? ((t['e' + n + r] = r),
                (t[n + r] = function () {
                  t['e' + n + r](e.window.event);
                }),
                t.attachEvent('on' + n, t[n + r]))
              : t.addEventListener(n, r, !1));
        },
        removeEvent: function (t, e, n) {
          t &&
            (t.detachEvent ? (t.detachEvent('on' + e, t[e + n]), (t[e + n] = null)) : t.removeEventListener(e, n, !1));
        },
        setCookie: function (t, e, n) {
          (t = encodeURIComponent(t)), (e = encodeURIComponent(e));
          var r = t + '=' + e + ';';
          (n = n || {}),
            n.expires && (r = r + 'expires=' + n.expires + ';'),
            n.path && (r = r + 'path=' + n.path + ';'),
            n.domain && 'localhost' != n.domain && (r = r + 'domain=' + n.domain + ';'),
            n.secure && (r += 'secure;'),
            (document.cookie = r);
        },
        getCookie: function (t) {
          t = encodeURIComponent(t);
          var e = new RegExp(t + '=([^;]*)'),
            n = e.exec(document.cookie);
          return !!n && decodeURIComponent(n[1]);
        },
        delCookie: function (t) {
          e.setCookie(t, '', { expires: 'Thu, 01 Jan 1970 00:00:01 GMT' });
        },
        createElementFromHTML: function (t) {
          var e = document.createElement('div');
          return (e.innerHTML = t.trim()), e.firstChild;
        },
        isDescendantOfId: function (t, e) {
          do {
            if (!t) return !1;
            if (t.id === e) return !0;
          } while ((t = t.parentElement));
          return !1;
        },
        isDescendantOfClass: function (t, e) {
          do {
            if (!t) return !1;
            if (this.hasClass(t, e)) return !0;
          } while ((t = t.parentElement));
          return !1;
        },
        isDescendantOfEl: function (t, e) {
          if (!e) return !1;
          do {
            if (!t) return !1;
            if (t === e) return !0;
          } while ((t = t.parentElement));
          return !1;
        },
        determineRenderIntoElement: function (t) {
          var e = !1;
          return (
            'string' == typeof t && null !== document.getElementById(t)
              ? (e = document.getElementById(t))
              : t instanceof Object && (e = t),
            e
          );
        },
        logError: function (t) {
          console && console.error ? console.error(t) : void 0;
        }
      });
    }),
    Le =
      (xe.window,
      xe.deepOverride,
      xe.isArray,
      xe.isEmptyObject,
      xe.map,
      xe.filter,
      xe.nextTick,
      xe.appendStyles,
      xe.appendScript,
      xe.ensureScript,
      xe.hasClass,
      xe.toggleClass,
      xe.addClass,
      xe.removeClass,
      xe.getCurrentDomain,
      xe.createCORSRequest,
      xe.addEvent,
      xe.removeEvent,
      xe.setCookie,
      xe.getCookie,
      xe.delCookie,
      xe.createElementFromHTML,
      xe.isDescendantOfId,
      xe.isDescendantOfClass,
      xe.isDescendantOfEl,
      xe.determineRenderIntoElement,
      xe.logError,
      '/* Breakpoints */\n/* Common Mixins */\n/*\n * SVG styles and reset.\n * Pointer events are set to none due to an IE event.\n * Wrap all SVGs in a container and bind events to that container\n */\n.theme-button {\n  color: black;\n  line-height: 28px;\n  font-size: 14px;\n  font-weight: 600;\n  text-align: center;\n  border-radius: 2px;\n}\n\n#riotbar-bar svg, #riotbar-subbar .riotbar-footer svg, #riotbar-application-switcher svg {\n  pointer-events: none;\n}\n#riotbar-bar svg.hide, #riotbar-subbar .riotbar-footer svg.hide, #riotbar-application-switcher svg.hide {\n  display: none;\n}\n#riotbar-bar svg.block, #riotbar-subbar .riotbar-footer svg.block, #riotbar-application-switcher svg.block {\n  display: block;\n}\n#riotbar-bar svg.inline-block, #riotbar-subbar .riotbar-footer svg.inline-block, #riotbar-application-switcher svg.inline-block {\n  display: inline-block;\n}\n#riotbar-bar svg.float-left, #riotbar-subbar .riotbar-footer svg.float-left, #riotbar-application-switcher svg.float-left {\n  float: left;\n}\n#riotbar-bar svg.float-right, #riotbar-subbar .riotbar-footer svg.float-right, #riotbar-application-switcher svg.float-right {\n  float: right;\n}\n#riotbar-bar svg use, #riotbar-subbar .riotbar-footer svg use, #riotbar-application-switcher svg use {\n  pointer-events: none;\n}\n\n.riotbar-root {\n  position: static;\n}\n\nbody.riotbar-present {\n  margin: 0px;\n  overflow-x: hidden;\n  padding: 0px;\n  position: static;\n}\n\n.riotbar-clearfix {\n  zoom: 1;\n  /* ie 6/7 */\n}\n.riotbar-clearfix:before, .riotbar-clearfix:after {\n  content: "";\n  display: table;\n}\n.riotbar-clearfix:after {\n  clear: both;\n}\n\n.riotbar-base-element {\n  /* RiotBar Reset\n     based on Eric Meyer\'s CSS Reset, but targeted within .riotbar\n        - http://meyerweb.com/eric/tools/css/reset/\n     Note\n       - removed html & body selectors, which don\'t apply for .riotbar\n       - To optimize the generated css, we have commented out some unused selectors\n  */\n  /* HTML5 display-role reset for older browsers */\n  /*\n    @TODO: do we want to size some things to the size of the content?\n    - the locale switcher would be a good example of something where this would apply.\n\n    We can creat a new class like this which can be applied to elements like the `riotbar-subbar` ...\n       ... which perhaps should be renamed the `riotbar-document-bar`\n\n    .riotbar-docwidth {\n      width: 1000px;\n      width: 100%;\n      margin: 0 auto;\n      padding: 0 10px;\n    }\n\n    This width attribute of this css would then become a configuration option,\n    which apps could override.  We would use js to set the width property of the class.\n    This way our templates and css can easily adapt to the various desktop page widths that we encounter.\n    The configuration option might not be a simple width.  Perhaps, if we provide a breakpoint declaration\n    system in the configs, then we cana more complex breakpoint system rule s\n\n\n   */\n}\n.riotbar-base-element,\n.riotbar-base-element div, .riotbar-base-element span,\n.riotbar-base-element h1, .riotbar-base-element h2, .riotbar-base-element h3, .riotbar-base-element h4, .riotbar-base-element h5, .riotbar-base-element h6,\n.riotbar-base-element a,\n.riotbar-base-element img,\n.riotbar-base-element ol, .riotbar-base-element ul, .riotbar-base-element li,\n.riotbar-base-element strong, .riotbar-base-element em {\n  margin: 0;\n  padding: 0;\n  border: 0;\n  outline: 0;\n  font-size: 100%;\n  /* These custom font references are actually loaded in when the `customFonts` plugin is enabled */\n  /* Main font by default */\n  font-family: "FF Mark W05";\n  vertical-align: baseline;\n  /* prevent text selection */\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -khtml-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  /* Fix webkit fonts */\n  -webkit-font-smoothing: antialiased;\n}\n.riotbar-base-element:lang(ar),\n.riotbar-base-element div:lang(ar), .riotbar-base-element span:lang(ar),\n.riotbar-base-element h1:lang(ar), .riotbar-base-element h2:lang(ar), .riotbar-base-element h3:lang(ar), .riotbar-base-element h4:lang(ar), .riotbar-base-element h5:lang(ar), .riotbar-base-element h6:lang(ar),\n.riotbar-base-element a:lang(ar),\n.riotbar-base-element img:lang(ar),\n.riotbar-base-element ol:lang(ar), .riotbar-base-element ul:lang(ar), .riotbar-base-element li:lang(ar),\n.riotbar-base-element strong:lang(ar), .riotbar-base-element em:lang(ar) {\n  font-family: "Cairo", Tahoma, sans-serif;\n}\n.riotbar-base-element:lang(ru),\n.riotbar-base-element div:lang(ru), .riotbar-base-element span:lang(ru),\n.riotbar-base-element h1:lang(ru), .riotbar-base-element h2:lang(ru), .riotbar-base-element h3:lang(ru), .riotbar-base-element h4:lang(ru), .riotbar-base-element h5:lang(ru), .riotbar-base-element h6:lang(ru),\n.riotbar-base-element a:lang(ru),\n.riotbar-base-element img:lang(ru),\n.riotbar-base-element ol:lang(ru), .riotbar-base-element ul:lang(ru), .riotbar-base-element li:lang(ru),\n.riotbar-base-element strong:lang(ru), .riotbar-base-element em:lang(ru) {\n  font-family: "Neue Frutiger World W05", Tahoma, sans-serif;\n}\n.riotbar-base-element:lang(ko), .riotbar-base-element:lang(kr),\n.riotbar-base-element div:lang(ko),\n.riotbar-base-element div:lang(kr), .riotbar-base-element span:lang(ko), .riotbar-base-element span:lang(kr),\n.riotbar-base-element h1:lang(ko),\n.riotbar-base-element h1:lang(kr), .riotbar-base-element h2:lang(ko), .riotbar-base-element h2:lang(kr), .riotbar-base-element h3:lang(ko), .riotbar-base-element h3:lang(kr), .riotbar-base-element h4:lang(ko), .riotbar-base-element h4:lang(kr), .riotbar-base-element h5:lang(ko), .riotbar-base-element h5:lang(kr), .riotbar-base-element h6:lang(ko), .riotbar-base-element h6:lang(kr),\n.riotbar-base-element a:lang(ko),\n.riotbar-base-element a:lang(kr),\n.riotbar-base-element img:lang(ko),\n.riotbar-base-element img:lang(kr),\n.riotbar-base-element ol:lang(ko),\n.riotbar-base-element ol:lang(kr), .riotbar-base-element ul:lang(ko), .riotbar-base-element ul:lang(kr), .riotbar-base-element li:lang(ko), .riotbar-base-element li:lang(kr),\n.riotbar-base-element strong:lang(ko),\n.riotbar-base-element strong:lang(kr), .riotbar-base-element em:lang(ko), .riotbar-base-element em:lang(kr) {\n  font-family: "RixSGo", Tahoma, sans-serif;\n}\n.riotbar-base-element ol, .riotbar-base-element ul {\n  list-style: none;\n}\n.riotbar-base-element,\n.riotbar-base-element * {\n  box-sizing: border-box;\n  position: static;\n  /* default to ensure overrides from other websites don\'t mess with us */\n}\n.riotbar-base-element .riotbar-pagewidth {\n  position: relative;\n  width: 100%;\n  margin: 0 auto;\n  padding: 0px;\n}\n.riotbar-base-element a,\n.riotbar-base-element a:link,\n.riotbar-base-element a:visited {\n  text-decoration: none;\n  color: #F9F9F9;\n}\n\n#riotbar-bar-wrapper {\n  position: absolute;\n  top: 0;\n  left: 0;\n  margin: 0;\n  border: 0;\n  width: 100%;\n}\n\n#riotbar-bar {\n  background-color: #111111;\n  border-bottom: 2px solid #333333;\n  /* These custom font references are actually loaded in when the `customFonts` plugin is enabled */\n  /* Main font by default */\n  font-family: "FF Mark W05";\n  font-size: 14px !important;\n  font-weight: bold;\n  height: 80px;\n  line-height: normal;\n  position: relative;\n  text-align: left;\n  width: 100%;\n  z-index: 3000000;\n}\n#riotbar-bar .sub-menu-header-icon {\n  color: #F9F9F9;\n}\n#riotbar-bar .sub-menu-header-icon svg, #riotbar-bar .sub-menu-header-icon span {\n  display: none;\n  float: left;\n}\n#riotbar-bar:lang(ar) {\n  font-family: "Cairo", Tahoma, sans-serif;\n}\n#riotbar-bar:lang(ru) {\n  font-family: "Neue Frutiger World W05", Tahoma, sans-serif;\n}\n#riotbar-bar:lang(ko), #riotbar-bar:lang(kr) {\n  font-family: "RixSGo", Tahoma, sans-serif;\n}\n#riotbar-bar .riotbar-navbar-separator {\n  border-left: 2px solid #7E7E7E;\n  display: inline-block;\n  font-size: 14px;\n  height: 30px;\n  margin-left: 0.25em;\n  margin-right: 0.25em;\n  margin-top: 24px;\n  vertical-align: top;\n  opacity: 0.4;\n}\n#riotbar-bar .riotbar-navbar-separator.riotbar-title-separator {\n  margin-right: 0.85em;\n}\n#riotbar-bar .riotbar-bar-content {\n  text-align: center;\n  height: 80px;\n}\n#riotbar-bar .riotbar-left-content, #riotbar-bar .riotbar-content {\n  opacity: 1;\n  transition: opacity 0.5s cubic-bezier(0.06, 0.81, 0, 0.98) 0s;\n}\n#riotbar-bar.sidebar-open .riotbar-bar-content, #riotbar-bar.sidebar-open .riotbar-left-content {\n  opacity: 0.25;\n}\n#riotbar-bar .riotbar-right-mobile-content {\n  display: none;\n  right: 70px;\n}\n@media (max-width: 1024px) {\n  #riotbar-bar .riotbar-right-mobile-content {\n    display: block;\n  }\n  #riotbar-bar .riotbar-right-mobile-content #riotbar-service-status-messages {\n    right: -10px;\n  }\n}\n#riotbar-bar .riotbar-left-content, #riotbar-bar .riotbar-right-content, #riotbar-bar .riotbar-right-mobile-content {\n  bottom: 0;\n  position: absolute;\n  top: 0;\n}\n@media (max-width: 1024px) {\n  #riotbar-bar.riotbar-mobile-responsive .riotbar-right-content .riotbar-navmenu-dropdown {\n    transform: translateX(100%);\n    width: 315px;\n    overflow-y: auto;\n  }\n}\n#riotbar-bar .riotbar-right-content {\n  background-color: #111111;\n  right: 0px;\n  padding-right: 16px;\n}\n@media (max-width: 1024px) {\n  #riotbar-bar .riotbar-right-content #riotbar-service-status, #riotbar-bar .riotbar-right-content #riotbar-account {\n    display: none;\n  }\n}\n#riotbar-bar .riotbar-right-content .riotbar-right-content-icons {\n  margin: 0px 12px;\n  float: left;\n  white-space: nowrap;\n  color: #F9F9F9;\n}\n#riotbar-bar .riotbar-right-content .riotbar-right-content-icons .lang-switch-trigger {\n  cursor: pointer;\n  display: block;\n}\n#riotbar-bar .riotbar-right-content .riotbar-right-content-icons .riotbar-navmenu-right-icon {\n  float: left;\n}\n#riotbar-bar .riotbar-right-content .riotbar-right-content-icons .riotbar-navmenu-right-icon .lang-switch-trigger svg {\n  margin-top: 32px;\n  pointer-events: none;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown {\n  background: #212121;\n  border-top: 2px solid #262626;\n  box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.5);\n  right: -5px;\n  margin: 0;\n  opacity: 0;\n  overflow: visible;\n  padding-bottom: 18px;\n  position: fixed;\n  height: 100%;\n  text-align: left;\n  transform: translateY(-5%);\n  top: 0px;\n  visibility: hidden;\n  width: 460px;\n  z-index: 10;\n  transition: transform 300ms, opacity 300ms;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .lang-switch-dropdown {\n  z-index: 11;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown a {\n  padding-left: 18px;\n  padding-right: 18px;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown.riotbar-click-active {\n  visibility: visible;\n  transform: translateX(0%);\n  opacity: 1;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown.slide-out {\n  visibility: visible;\n  transform: translateX(100%);\n  opacity: 1;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .sub-menu-header {\n  height: 78px;\n  background-color: #111111;\n  border-bottom: 2px solid #262626;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .sub-menu-header a {\n  cursor: pointer;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .sub-menu-header .sub-menu-header-text {\n  font-size: 18px;\n  position: absolute;\n  top: 22px;\n  left: 40px;\n  color: #F9F9F9;\n  text-overflow: ellipsis;\n  max-width: 200px;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .sub-menu-header .sub-menu-back {\n  position: absolute;\n  top: 20px;\n  height: 30px;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .sub-menu-header .sub-menu-back svg {\n  display: inline-block;\n  margin-top: 12px;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .sub-menu-header .sub-menu-close {\n  position: absolute;\n  top: 21px;\n  right: 31px;\n  width: 32px;\n  height: 32px;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .sub-menu-header .sub-menu-close svg {\n  position: absolute;\n  top: 0;\n  right: 0;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .side-menu-icon {\n  position: absolute;\n  top: 28px;\n  right: 30px;\n  width: 10px;\n  height: 10px;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-dropdown .side-menu-icon svg {\n  display: block;\n}\n#riotbar-bar .riotbar-right-content .riotbar-navmenu-touchpoints, #riotbar-bar .riotbar-right-content .riotbar-navmenu-links {\n  transition: opacity 0.2s linear;\n}\n#riotbar-bar .riotbar-right-content.riotbar-click-active .riotbar-navmenu-touchpoints {\n  display: block;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link {\n  position: relative;\n  display: block;\n  font-size: 16px;\n  font-weight: 800;\n  margin: 0;\n  padding-top: 20px;\n  padding-bottom: 20px;\n  text-transform: uppercase;\n  transition: color 1s cubic-bezier(0.06, 0.81, 0, 0.98);\n  border-bottom: 1px solid #111111;\n  cursor: pointer;\n  letter-spacing: 0.1em;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link.disabled {\n  color: #7E7E7E;\n  cursor: default;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link.disabled svg {\n  position: absolute;\n  right: 25px;\n  top: 25px;\n  fill: #7E7E7E;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link.riotbar-active-link {\n  background-color: #333333;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link:lang(ar) {\n  letter-spacing: 0;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link.riotbar-active-link {\n  color: #F9F9F9;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link:last-child {\n  margin-bottom: 0;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link.show-auth-sub-menu {\n  text-transform: none;\n  background-color: #171717;\n  letter-spacing: 0;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link.show-auth-sub-menu svg.arrow-down {\n  margin-left: 10px;\n  margin-bottom: 3px;\n  transform: rotate(270deg);\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link .sideMenuIcons {\n  position: absolute;\n  top: 0px;\n  right: 0px;\n  width: 100px;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link .sideMenuIcons .lang-switch-trigger {\n  width: 32px;\n  height: 32px;\n  position: absolute;\n  right: 31px;\n  top: 5px;\n  padding: 0;\n  border-radius: 16px;\n  background-color: #212121;\n}\n#riotbar-bar .riotbar-right-content a.riotbar-navmenu-link .sideMenuIcons .lang-switch-trigger svg {\n  position: absolute;\n  top: 9px;\n  left: 9px;\n}\n#riotbar-bar .riotbar-right-content.riotbar-menu-transition .riotbar-navmenu-touchpoints, #riotbar-bar .riotbar-right-content.riotbar-menu-transition .riotbar-navmenu-links {\n  opacity: 0;\n}\n#riotbar-bar .riotbar-right-content.riotbar-show-links .riotbar-navmenu-links {\n  display: block;\n}\n#riotbar-bar #riotbar-mobile-nav {\n  display: none;\n  margin-right: 10px;\n  margin-left: 10px;\n}\n#riotbar-bar #riotbar-mobile-nav a {\n  margin-top: 23px;\n  display: inline-block;\n  cursor: pointer;\n}\n@media (max-width: 1024px) {\n  #riotbar-bar #riotbar-mobile-nav {\n    display: inline !important;\n  }\n}\n\n#riotbar-subbar {\n  position: relative;\n  top: 0px;\n  width: 100%;\n  z-index: 10;\n  /* this z-index is questionable, but is required to place it above the lolkit header */\n  pointer-events: none;\n  text-align: left;\n}\n#riotbar-subbar:lang(ar) {\n  direction: rtl;\n}\n#riotbar-subbar .riotbar-subcontent {\n  pointer-events: auto;\n}\n\n@media screen and (max-width: 1024px) {\n  #riotbar-bar .riotbar-right-content a.riotbar-navmenu-link .sideMenuIcons .lang-switch-trigger {\n    top: 12px;\n    right: 30px;\n  }\n}\n/** Global Keyframes **/\n/* Keyframes */\n/* Basic Fade-in effect */\n@keyframes fadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n@-moz-keyframes fadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n@-webkit-keyframes fadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n/* Slide in from left animation */\n@keyframes slideInFromLeft {\n  0% {\n    left: -76vw;\n  }\n  100% {\n    left: 0;\n  }\n}\n@-moz-keyframes slideInFromLeft {\n  0% {\n    left: -76vw;\n  }\n  100% {\n    left: 0;\n  }\n}\n@-webkit-keyframes slideInFromLeft {\n  0% {\n    left: -76vw;\n  }\n  100% {\n    left: 0;\n  }\n}'),
    Re = {
      authInfo: null,
      isLoggedIn: null,
      homepageUrl: null,
      init: function () {
        var t = window.RiotBar;
        (this.authInfo = t.plugins.account.extensions.getGlobalAccount()), (this.isLoggedIn = this.authInfo.summoner);
      },
      getHomepageUrl: function () {
        return window.RiotBar.config && window.RiotBar.config.navigation && window.RiotBar.config.navigation.homepageUrl
          ? window.RiotBar.config.navigation.homepageUrl
          : null;
      }
    },
    Ce = (function () {
      function t() {
        B(this, t);
      }
      return (
        H(t, [
          {
            key: 'getName',
            value: function () {
              return '';
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return '';
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return '#fff';
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return null;
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              return null;
            }
          },
          {
            key: 'getStyles',
            value: function () {
              return '';
            }
          }
        ]),
        t
      );
    })(),
    Me =
      '.cool {\n  display: none;\n}\n\n/* Breakpoints */\n/* Common Mixins */\n.theme-button {\n  background-color: #d13639 !important;\n}\n.theme-button:hover {\n  background-color: #ffffff !important;\n  color: #333333 !important;\n}\n.theme-button:active {\n  background-color: #ffffff !important;\n  color: #333333 !important;\n}\n\n#riotbar-bar.forge-theme .riotbar-active-link, #riotbar-bar.forge-theme .riotbar-navbar-link:hover {\n  border-color: #d13639;\n}\n#riotbar-bar.forge-theme #riotbar-navbar {\n  left: 200px;\n}\n#riotbar-bar.forge-theme .nav-dropdown-active a {\n  border-color: #d13639;\n}\n#riotbar-bar.forge-theme .riotbar-navbar-subnav-menu .riotbar-active-sub-item {\n  border-bottom-color: #d13639;\n}\n#riotbar-bar.forge-theme .sub-menu-header-icon svg {\n  margin-top: 20px;\n  margin-left: 15px;\n  display: block;\n}\n\n@media screen and (max-width: 1024px) {\n  #riotbar-bar.forge-theme .riotbar-explore-label svg {\n    display: block;\n    margin-top: 22px;\n  }\n}',
    Ee = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'getName',
            value: function () {
              return 'forge';
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return 'forge';
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return et(
                'svg',
                {
                  width: '190',
                  height: '32',
                  viewBox: '0 0 190 32',
                  fill: 'none',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et('path', {
                  d:
                    'M17.8768 0L18.0634 12.4642L22.4088 8.68865L15.9501 31.8081L13.718 17.1375L10.9127 19.6111L8.36175 6.48908L17.8768 0ZM18.5587 30.5576L32.8771 17.0415L25.5398 13.7525L18.5587 30.5576ZM13.6264 30.5576L6.97095 14.4343L0.440985 18.8026L13.6264 30.5576ZM19.7629 32L33.949 28.6081L29.2882 24.5859L19.7629 32ZM0 28.6013L12.1814 32.0034L3.59233 25.4081L0 28.6013ZM65.3336 14.006L64.8112 24.6715H61.0255L62.0737 14.006H65.3336ZM79.8624 22.2081L80.1066 16.3392L73.4274 16.4557L73.0882 22.0471L79.8624 22.2081ZM83.2681 14.006L83.3834 24.6681L69.2685 24.6715L70.4931 14.006H83.2681ZM47.3312 19.6214L53.7968 18.2818L53.5186 16.3049L47.4262 16.5413L47.3312 19.6214ZM56.2798 14.006L57.4909 19.6762L55.0756 20.2587L57.7521 24.6681H53.868L52.0294 20.9439L47.3177 22.0814L47.2464 24.6715H43.4607L44.5293 14.006H56.2798ZM86.5857 14.006L86.5077 16.8497L91.3619 16.74L90.9039 24.6715H94.6896L94.6387 16.6647L99.4014 16.5585L99.4624 14.006H86.5857ZM135.121 22.2116L134.714 16.3221L128.337 16.4694L128.171 22.0437L135.121 22.2116ZM137.845 14.006L138.734 24.6681L124.646 24.6715L125.114 14.006H137.845ZM177.293 14.006L177.792 24.6715L190 24.6681L189.78 22.3246L181.397 22.1259L181.248 20.4368L187.32 20.3443L187.222 18.2338L180.885 18.1413L180.733 16.3872L189.203 16.1747L188.993 13.9957L177.293 14.006ZM160.244 14.006L160.532 24.6715L173.86 24.6681L173.087 18.1002L166.835 18.3058L166.947 20.2792L170.274 20.3854L170.4 22.2287L164.118 22.0951L163.633 16.4522L172.842 16.2124L172.571 14.0026L160.244 14.006ZM108.767 14.006L108.608 24.6715H112.485L112.431 21.2283L119.541 21.0398V18.8368L112.39 18.7443L112.346 16.6475L121.288 16.4488L121.278 14.0026L108.767 14.006ZM146.305 19.6385L152.971 18.2921L152.431 16.2604L146.047 16.5105L146.305 19.6385ZM155.159 14.006L156.645 19.6865L154.226 20.2724L157.259 24.6681H153.375L151.183 20.961L146.546 22.0437L146.76 24.6715H142.974L142.733 14.006H155.159Z',
                  fill: '#F9F9F9'
                })
              );
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              return et(
                'svg',
                { width: '34', height: '32', viewBox: '0 0 34 32', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
                et('path', {
                  d:
                    'M17.9,0l0.2,12.5l4.3-3.8L16,31.8l-2.2-14.7l-2.8,2.5L8.4,6.5L17.9,0z M18.6,30.6L32.9,17l-7.3-3.3L18.6,30.6zM13.7,30.6L7,14.4l-6.5,4.4L13.7,30.6z M19.8,32L34,28.6l-4.7-4L19.8,32z M0,28.6L12.2,32l-8.6-6.6L0,28.6z',
                  fill: '#F9F9F9'
                })
              );
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return '#d13639';
            }
          },
          {
            key: 'getStyles',
            value: function () {
              return Me;
            }
          }
        ]),
        e
      );
    })(Ce),
    Se =
      '/* Breakpoints */\n/* Common Mixins */\n.theme-button {\n  background-color: #0bc4e2 !important;\n}\n.theme-button:hover {\n  background-color: #00b2cf !important;\n}\n.theme-button:active {\n  background-color: #00a0ba !important;\n}\n\n#riotbar-bar.lol-theme .riotbar-active-link, #riotbar-bar.lol-theme .riotbar-navbar-link:hover {\n  border-color: #0bc4e2;\n}\n#riotbar-bar.lol-theme #riotbar-navbar {\n  left: 200px;\n}\n#riotbar-bar.lol-theme .nav-dropdown-active a {\n  border-color: #0bc4e2;\n}\n#riotbar-bar.lol-theme .riotbar-navbar-subnav-menu .riotbar-active-sub-item {\n  border-bottom-color: #0bc4e2;\n}\n#riotbar-bar.lol-theme .sub-menu-header-icon svg {\n  margin-top: 20px;\n  margin-left: 15px;\n  display: block;\n}\n\n@media screen and (max-width: 1024px) {\n  #riotbar-bar.lol-theme .riotbar-explore-label svg {\n    display: block;\n    margin-top: 22px;\n  }\n}',
    Fe = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'getName',
            value: function () {
              return 'lol';
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return 'lol';
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return et(
                'svg',
                {
                  className: 'league',
                  width: '30',
                  height: '32',
                  viewBox: '0 0 30 32',
                  fill: 'none',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et(
                  'g',
                  { 'clip-path': 'url(#clip0)' },
                  et('path', {
                    d:
                      'M1.80644 9.75049C0.655032 11.8373 0 14.2271 0 16.7683C0 19.3095 0.655032 21.7015 1.80644 23.7883V9.75049Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M15 2.02222C13.7829 2.02222 12.602 2.16921 11.4688 2.43647V4.75718C12.5907 4.44093 13.7738 4.26721 15 4.26721C22.0218 4.26721 27.7153 9.84627 27.7153 16.7305C27.7153 19.8307 26.5571 22.6659 24.6464 24.8463L24.2838 26.118L23.4814 28.9331C27.4184 26.2761 30.0023 21.8195 30.0023 16.7705C30 8.62355 23.2843 2.02222 15 2.02222Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M11.4688 24.4209H22.9737H23.2253C25.1723 22.4209 26.3713 19.7126 26.3713 16.7305C26.3713 10.5746 21.2806 5.58569 15 5.58569C13.767 5.58569 12.5816 5.78168 11.4688 6.1358V24.4209Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d: 'M10.1088 0H1.55029L3.16634 3.29844V28.7038L1.55029 32H21.1922L22.9737 25.7572H10.1088V0Z',
                    fill: '#C28F2C'
                  })
                ),
                et(
                  'defs',
                  null,
                  et('clipPath', { id: 'clip0' }, et('rect', { width: '30', height: '32', fill: 'white' }))
                )
              );
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              return et(
                'svg',
                {
                  className: 'league',
                  width: '85',
                  height: '32',
                  viewBox: '0 0 85 32',
                  fill: 'none',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et(
                  'g',
                  { clipPath: 'url(#clip0)' },
                  et('path', {
                    d:
                      'M59.7261 17.2695V31.749H56.5229L49.6035 22.34V31.749H46.5347V18.6228L45.8335 17.2695H49.801L56.6573 26.8087V17.2695H59.7261Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M27.8345 31.9991C28.8909 32.0048 29.8667 31.8267 30.7619 31.467C31.6582 31.1072 32.4307 30.6082 33.0828 29.9721C33.7337 29.336 34.2444 28.5811 34.6148 27.7098C34.8813 27.0828 35.0531 26.4205 35.1314 25.7238C35.1618 25.4532 35.2143 24.3614 35.1582 23.7025H27.8649L26.5665 26.3131H31.7704C31.5694 27.1376 31.1148 27.8171 30.409 28.3287C29.7019 28.8415 28.8559 29.095 27.8707 29.0904C27.2373 29.087 26.646 28.9637 26.0979 28.7204C25.5498 28.4772 25.073 28.1483 24.6675 27.7338C24.2608 27.3192 23.9441 26.8327 23.7139 26.2743C23.4848 25.7158 23.3715 25.1197 23.375 24.487C23.3785 23.8544 23.4977 23.2594 23.7326 22.7032C23.9675 22.1471 24.29 21.664 24.7014 21.2529C25.1116 20.8418 25.5884 20.522 26.1318 20.2902C26.6752 20.0595 27.263 19.9453 27.8976 19.9476C28.7565 19.9522 29.515 20.1417 30.1752 20.5163C30.8355 20.8909 31.3462 21.4299 31.7085 22.1334L34.588 20.7858C33.9885 19.6141 33.1026 18.6948 31.9305 18.0279C30.7584 17.3621 29.4261 17.0252 27.9338 17.0172C26.8774 17.0115 25.877 17.2034 24.9328 17.5905C23.9874 17.9776 23.1646 18.5098 22.4646 19.1882C21.7646 19.8654 21.2072 20.6579 20.7947 21.5635C20.381 22.4691 20.173 23.4387 20.1671 24.4699C20.1613 25.5011 20.3576 26.473 20.7538 27.3832C21.1511 28.2933 21.6922 29.0904 22.3782 29.7745C23.0641 30.4586 23.8752 30.9999 24.8089 31.3962C25.7415 31.7924 26.7512 31.9934 27.8345 31.9991Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d: 'M3.90672 17.2686H0L0.737403 18.7657V30.2542L0 31.7503H8.9657L9.77906 28.917H3.90672V17.2686Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M10.8613 31.7503H19.6003V28.9181H14.0423V25.7411H18.3394L19.1177 23.0185H14.0423V20.0893H19.6003V17.2686H10.8613V31.7503Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M36.2681 31.7503H45.007V28.9181H39.4491V25.7411H43.7473L44.5244 23.0185H39.4491V20.0893H45.007V17.2686H36.2681V31.7503Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M41.8662 14.9822C42.9227 14.9879 43.8985 14.8098 44.7936 14.45C45.69 14.0903 46.4624 13.5912 47.1145 12.9551C47.7654 12.3191 48.2761 11.5642 48.6466 10.6928C48.913 10.0659 49.0848 9.40352 49.1631 8.7069C49.1935 8.43625 49.2461 7.3445 49.19 6.68556H41.8966L40.5983 9.29503H45.8022C45.6011 10.1196 45.1466 10.799 44.4407 11.3107C43.7337 11.8234 42.8876 12.0769 41.9024 12.0724C41.269 12.069 40.6777 11.9456 40.1296 11.7024C39.5816 11.4591 39.1048 11.1302 38.6992 10.7157C38.2926 10.3011 37.9759 9.81464 37.7456 9.25621C37.5166 8.69777 37.4032 8.10164 37.4067 7.46897C37.4102 6.83631 37.5294 6.24132 37.7643 5.68517C37.9992 5.12901 38.3218 4.64595 38.7331 4.23483C39.1433 3.82371 39.6201 3.50395 40.1635 3.27212C40.7069 3.04144 41.2948 2.92724 41.9293 2.92952C42.7883 2.93409 43.5467 3.12366 44.207 3.49824C44.8673 3.87281 45.3779 4.41184 45.7402 5.11531L48.6197 3.76775C48.0202 2.59606 47.1344 1.67675 45.9623 1.00982C44.7901 0.344031 43.4579 0.00714122 41.9656 -0.000852784C40.9091 -0.00656279 39.9088 0.185293 38.9645 0.572432C38.0191 0.95957 37.1964 1.49174 36.4964 2.17009C35.7964 2.8473 35.2389 3.63985 34.8264 4.54545C34.4127 5.45106 34.2047 6.42062 34.1989 7.45184C34.193 8.48307 34.3893 9.45491 34.7855 10.3651C35.1828 11.2753 35.7239 12.0724 36.4099 12.7564C37.0959 13.4405 37.9069 13.9818 38.8406 14.3781C39.7732 14.7766 40.7817 14.9776 41.8662 14.9822Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d: 'M3.90672 0.252441H0L0.737403 1.7496V13.2381L0 14.7342H8.9657L9.77906 11.8997H3.90672V0.252441Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M10.8613 14.7342H19.6003V11.9008H14.0423V8.72494H18.3394L19.1177 6.00241H14.0423V3.07318H19.6003V0.252441H10.8613V14.7342Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M63.5688 14.7342H72.3067V11.9008H66.7498V8.72494H71.0469L71.824 6.00241H66.7498V3.07318H72.3067V0.252441H63.5688V14.7342Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M29.397 0.252441H24.8779L25.8338 2.09906L20.6147 14.7353H23.8659L24.9737 11.91H30.8519L31.9913 14.7353H35.3055L29.397 0.252441ZM26.0594 9.14063L27.9677 4.27457L29.8013 9.14063H26.0594Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M80.6094 4.23486V10.7477H82.2466V8.54591H83.9365L84.3455 7.11385H82.2466V5.71033H84.647V4.23486H80.6094Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M76.5625 4.10547C74.6553 4.10547 73.1045 5.62433 73.1045 7.4915C73.1045 9.35867 74.6553 10.8775 76.5625 10.8775C78.4697 10.8775 80.0204 9.35867 80.0204 7.4915C80.0204 5.62433 78.4685 4.10547 76.5625 4.10547ZM76.5625 9.30614C75.5423 9.30614 74.7125 8.49189 74.7125 7.4915C74.7125 6.49111 75.5423 5.67686 76.5625 5.67686C77.5827 5.67686 78.4124 6.49111 78.4124 7.4915C78.4124 8.49189 77.5827 9.30614 76.5625 9.30614Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M56.2401 14.9854C55.3099 14.9854 54.4778 14.8438 53.7451 14.5617C53.0124 14.2796 52.3918 13.8765 51.8847 13.3523C51.3775 12.8293 50.986 12.1955 50.7114 11.4509C50.4367 10.7075 50.2988 9.88065 50.2988 8.97161V0.252441H53.5125V8.80717C53.5125 9.78586 53.7451 10.5567 54.2102 11.122C54.6753 11.6873 55.352 11.9694 56.2401 11.9694C57.1283 11.9694 57.8049 11.6873 58.27 11.122C58.7351 10.5567 58.9677 9.78586 58.9677 8.80717V0.252441H62.1814V8.97276C62.1814 9.88179 62.0435 10.7086 61.7689 11.452C61.4943 12.1955 61.1028 12.8293 60.5956 13.3535C60.0884 13.8776 59.4679 14.2796 58.7351 14.5628C58.0012 14.8438 57.1703 14.9854 56.2401 14.9854Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M80.8372 23.1624L78.8365 22.7866C77.9367 22.6176 77.4412 22.1243 77.4412 21.3968C77.4412 20.4056 78.4813 19.8129 79.4863 19.8129C79.8287 19.8129 81.5325 19.8997 81.8831 21.5601L84.6399 20.2446C84.2332 19.0637 83.0155 17.0161 79.4664 17.0161C76.6045 17.0161 74.2754 19.0706 74.2754 21.5955C74.2754 23.7368 75.7408 25.307 78.1938 25.7958L80.1956 26.191C81.1843 26.3839 81.7745 26.9333 81.7745 27.6596C81.7745 28.5994 80.9599 29.1704 79.6487 29.1841C78.0582 29.2001 76.7727 28.0444 76.5741 26.907L73.9248 28.3516C74.5325 30.285 76.4046 32.0026 79.6289 32.0026C81.6576 32.0026 82.9104 31.2363 83.6034 30.5934C84.4927 29.77 85.0022 28.636 85.0022 27.4826C84.9999 25.3036 83.4047 23.6489 80.8372 23.1624Z',
                    fill: '#C28F2C'
                  }),
                  et('path', {
                    d:
                      'M73.0452 21.6891C72.6514 20.8086 72.1138 20.0457 71.4325 19.3982C70.7501 18.7519 69.9519 18.2425 69.038 17.8702C68.123 17.4979 66.8211 17.2661 66.1258 17.2661H61.1147V31.7467H66.1165C67.1437 31.7467 68.1148 31.5651 69.0298 31.1951C69.9449 30.824 70.743 30.3158 71.4267 29.6705C72.1092 29.0242 72.6491 28.2624 73.0429 27.382C73.4379 26.5026 73.6354 25.5536 73.6366 24.5361C73.6354 23.5186 73.4391 22.5696 73.0452 21.6891ZM70.0454 26.2251C69.8198 26.7481 69.5137 27.2015 69.1268 27.5864C68.7389 27.9712 68.2784 28.2773 67.7432 28.5034C67.208 28.7295 66.6377 28.8426 66.0335 28.8426H64.2817V20.2205H66.0393C66.6447 20.2205 67.2138 20.3347 67.749 20.5619C68.2831 20.7892 68.7435 21.0952 69.1315 21.4801C69.5183 21.8661 69.8233 22.3229 70.0489 22.8528C70.2733 23.3827 70.3866 23.9434 70.3854 24.5338C70.3843 25.1402 70.2709 25.7032 70.0454 26.2251Z',
                    fill: '#C28F2C'
                  })
                ),
                et(
                  'defs',
                  null,
                  et('clipPath', { id: 'clip0' }, et('rect', { width: '85', height: '32', fill: 'white' }))
                )
              );
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return '#0BC4E2';
            }
          },
          {
            key: 'getStyles',
            value: function () {
              return Se;
            }
          }
        ]),
        e
      );
    })(Ce),
    Ae =
      '/* Style overrides for Riot Theme */\n.theme-button {\n  background-color: #d13639 !important;\n}\n.theme-button:hover {\n  background-color: #bd2629 !important;\n}\n.theme-button:active {\n  background-color: #a81417 !important;\n}\n\n#riotbar-bar.riot-theme #riotbar-navbar {\n  left: 230px;\n}\n#riotbar-bar.riot-theme .riotbar-active-link,\n#riotbar-bar.riot-theme .riotbar-navbar-link:hover {\n  border-color: #d13639;\n}\n#riotbar-bar.riot-theme .nav-dropdown-active a {\n  border-color: #d13639;\n}\n#riotbar-bar.riot-theme .riotbar-navbar-subnav-menu .riotbar-active-sub-item {\n  border-bottom-color: #d13639;\n}\n#riotbar-bar.riot-theme .second-logo svg {\n  margin-top: 12px;\n}\n#riotbar-bar.riot-theme .sub-menu-header-icon svg {\n  margin-top: 22px;\n  margin-left: 15px;\n  display: block;\n}',
    Pe = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'getName',
            value: function () {
              return 'riot';
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return 'riot';
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return et(
                'svg',
                { width: '49', height: '25', viewBox: '0 0 49 25', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
                et(
                  'g',
                  { id: 'Group' },
                  et('path', {
                    id: 'Vector',
                    d:
                      'M46.4216 21.8277L42.0042 21.0975L42.0229 20.5338L46.2508 20.3809L46.1431 19.3037H40.708L40.4296 22.008L44.9218 22.7702L44.9429 23.2677L40.2682 23.4457L40.1535 24.5229H46.6953L46.4216 21.8277ZM4.21575 21.4238L4.14789 22.4005L5.75761 22.453L5.73655 23.4434L2.36972 23.3476L2.51712 20.4836L7.14739 20.3786L7.07954 19.3015H1.09927L0.563477 24.5206H7.37668L7.2012 21.3736L4.21575 21.4238ZM33.0595 23.3407L33.0268 22.4416L36.2135 22.4051L36.2439 21.4238L32.9894 21.385L32.9566 20.4744L37.2453 20.3763L37.2804 19.3015H31.4966L31.3235 24.5206H37.388L37.3459 23.4434L33.0595 23.3407Z',
                    fill: 'white'
                  }),
                  et('path', {
                    id: 'Vector_2',
                    d:
                      'M28.3894 24.5206L27.8536 19.3015H26.3141L24.1008 21.7889L21.8898 19.3015H20.3502L19.8145 24.5206H21.623L21.7962 21.1362L24.1008 23.4434L26.4077 21.1362L26.5809 24.5206H28.3894Z',
                    fill: 'white'
                  }),
                  et(
                    'g',
                    { id: 'Group_2' },
                    et('path', {
                      id: 'Vector_3',
                      d: 'M20.7883 0.195312H17.1688L16.8389 16.3319H21.2118L20.7883 0.195312Z',
                      fill: 'white'
                    }),
                    et('path', {
                      id: 'Vector_4',
                      d:
                        'M35.505 0.203369H24.2206L23.7808 16.3331H36.3075L35.505 0.203369ZM32.0048 12.8233L28.0109 12.9488L27.8776 3.45762L32.0539 3.58085L32.0048 12.8233Z',
                      fill: 'white'
                    }),
                    et('path', {
                      id: 'Vector_5',
                      d:
                        'M48.9996 3.72569L48.5971 0.195312H37.4766L37.5701 3.34002L41.1288 3.46097L41.4915 16.3319H45.8737L44.5635 3.57736L48.9996 3.72569Z',
                      fill: 'white'
                    }),
                    et('path', {
                      id: 'Vector_6',
                      d:
                        'M10.0888 16.3319H14.8384L11.743 9.08858L14.2137 7.77182L12.5244 0.195312H0.397748L0 3.69146L2.71171 3.60246L1.40382 16.3319H5.64804L5.76268 12.2744L8.82067 10.645L10.0888 16.3319ZM6.01303 3.4952L9.50152 3.3811L9.89927 6.59655L5.87265 8.45188L6.01303 3.4952Z',
                      fill: 'white'
                    })
                  ),
                  et('path', {
                    id: 'Vector_7',
                    d:
                      'M15.4226 24.5206H17.2546L15.8064 19.3015H11.5083L10.0601 24.5206H11.892L12.1751 23.2038L15.1489 23.2403L15.4226 24.5206ZM12.3974 22.1677L12.7647 20.4607L14.5312 20.3832L14.9009 22.097L12.3974 22.1677Z',
                    fill: 'white'
                  })
                )
              );
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              return et(
                'svg',
                {
                  width: '109',
                  height: '35',
                  viewBox: '0 0 109 35',
                  fill: 'none',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et('path', {
                  d: 'M17.7136 31.6814L16.1378 27.5918L31.3569 28.3812L30.6733 34.9265L17.7136 31.6814Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M21.3595 0L0.0616455 10.143L3.35453 25.427H7.70011L6.9491 14.4946L7.61666 14.2843L10.2291 25.427H14.7416L14.5747 12.1101L15.2423 11.8998L17.5242 25.427H22.3031L23.4296 9.34061L24.0971 9.13031L25.2782 25.427H31.5976L33.8217 3.05746L21.3595 0Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M105.462 31.1253L99.4029 30.09L99.4286 29.2908L105.228 29.074L105.08 27.5469H97.6249L97.2429 31.3809L103.405 32.4615L103.434 33.1668L97.0215 33.4192L96.8642 34.9463H105.838L105.462 31.1253ZM47.5673 30.5526L47.4742 31.9374L49.6823 32.0118L49.6534 33.416L45.035 33.2801L45.2372 29.2196L51.5887 29.0708L51.4956 27.5437H43.2923L42.5573 34.9431H51.9032L51.6625 30.4815L47.5673 30.5526ZM87.1332 33.2704L87.0883 31.9956L91.4595 31.9439L91.5013 30.5526L87.0369 30.4976L86.992 29.2067L92.8749 29.0676L92.923 27.5437H84.9893L84.7518 34.9431H93.0707L93.0129 33.416L87.1332 33.2704Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M80.7274 34.9431L79.9924 27.5437H77.8806L74.8445 31.0703L71.8116 27.5437H69.6998L68.9648 34.9431H71.4457L71.6832 30.145L74.8445 33.416L78.009 30.145L78.2465 34.9431H80.7274Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d: 'M70.2996 0.453735H65.3346L64.882 23.3313H70.8805L70.2996 0.453735Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M90.4879 0.461792H75.0088L74.4055 23.3296H91.5887L90.4879 0.461792ZM85.6866 18.3536L80.2081 18.5315L80.0252 5.07548L85.754 5.25019L85.6866 18.3536Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M109 5.4589L108.448 0.453735H93.1935L93.3219 4.91212L98.2035 5.0836L98.7009 23.3313H104.712L102.915 5.2486L109 5.4589Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M55.6236 23.3313H62.1387L57.8927 13.0621L61.2818 11.1953L58.9646 0.453735H42.3301L41.7845 5.41037L45.5042 5.28419L43.7101 23.3313H49.5321L49.6893 17.5787L53.8841 15.2686L55.6236 23.3313ZM50.0327 5.13213L54.818 4.97036L55.3636 9.52904L49.8402 12.1594L50.0327 5.13213Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M62.9415 34.9431H65.4545L63.4679 27.5437H57.5721L55.5855 34.9431H58.0985L58.4868 33.0762L62.566 33.128L62.9415 34.9431ZM58.7917 31.6074L59.2956 29.1873L61.7187 29.0773L62.2258 31.5071L58.7917 31.6074Z',
                  fill: '#E6E6E6'
                })
              );
            }
          },
          {
            key: 'getStyles',
            value: function () {
              return Ae;
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return '#D13639';
            }
          }
        ]),
        e
      );
    })(Ce),
    Te =
      '/* Style overrides for Merch Theme */\n.theme-button {\n  background-color: #D13639 !important;\n}\n.theme-button:hover {\n  background-color: #BD2629 !important;\n}\n.theme-button:active {\n  background-color: #A81417 !important;\n}\n\n#riotbar-navmenu.merch-theme #riotbar-navbar {\n  left: 230px;\n}\n#riotbar-navmenu.merch-theme .riotbar-active-link,\n#riotbar-navmenu.merch-theme .riotbar-navbar-link:hover {\n  border-color: #D13639;\n}\n#riotbar-navmenu.merch-theme .nav-dropdown-active a {\n  border-color: #D13639;\n}\n#riotbar-navmenu.merch-theme .second-logo svg {\n  margin-top: 12px;\n}\n#riotbar-navmenu.merch-theme .sub-menu-header-icon .merch {\n  display: block;\n  float: left;\n  margin-top: 20px;\n  margin-left: 15px;\n  font-size: 22px;\n}',
    Be = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'getName',
            value: function () {
              return 'merch';
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return 'merch';
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return et(
                'svg',
                { width: '49', height: '25', viewBox: '0 0 49 25', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
                et(
                  'g',
                  { id: 'Group' },
                  et('path', {
                    id: 'Vector',
                    d:
                      'M46.4216 21.8277L42.0042 21.0975L42.0229 20.5338L46.2508 20.3809L46.1431 19.3037H40.708L40.4296 22.008L44.9218 22.7702L44.9429 23.2677L40.2682 23.4457L40.1535 24.5229H46.6953L46.4216 21.8277ZM4.21575 21.4238L4.14789 22.4005L5.75761 22.453L5.73655 23.4434L2.36972 23.3476L2.51712 20.4836L7.14739 20.3786L7.07954 19.3015H1.09927L0.563477 24.5206H7.37668L7.2012 21.3736L4.21575 21.4238ZM33.0595 23.3407L33.0268 22.4416L36.2135 22.4051L36.2439 21.4238L32.9894 21.385L32.9566 20.4744L37.2453 20.3763L37.2804 19.3015H31.4966L31.3235 24.5206H37.388L37.3459 23.4434L33.0595 23.3407Z',
                    fill: 'white'
                  }),
                  et('path', {
                    id: 'Vector_2',
                    d:
                      'M28.3894 24.5206L27.8536 19.3015H26.3141L24.1008 21.7889L21.8898 19.3015H20.3502L19.8145 24.5206H21.623L21.7962 21.1362L24.1008 23.4434L26.4077 21.1362L26.5809 24.5206H28.3894Z',
                    fill: 'white'
                  }),
                  et(
                    'g',
                    { id: 'Group_2' },
                    et('path', {
                      id: 'Vector_3',
                      d: 'M20.7883 0.195312H17.1688L16.8389 16.3319H21.2118L20.7883 0.195312Z',
                      fill: 'white'
                    }),
                    et('path', {
                      id: 'Vector_4',
                      d:
                        'M35.505 0.203369H24.2206L23.7808 16.3331H36.3075L35.505 0.203369ZM32.0048 12.8233L28.0109 12.9488L27.8776 3.45762L32.0539 3.58085L32.0048 12.8233Z',
                      fill: 'white'
                    }),
                    et('path', {
                      id: 'Vector_5',
                      d:
                        'M48.9996 3.72569L48.5971 0.195312H37.4766L37.5701 3.34002L41.1288 3.46097L41.4915 16.3319H45.8737L44.5635 3.57736L48.9996 3.72569Z',
                      fill: 'white'
                    }),
                    et('path', {
                      id: 'Vector_6',
                      d:
                        'M10.0888 16.3319H14.8384L11.743 9.08858L14.2137 7.77182L12.5244 0.195312H0.397748L0 3.69146L2.71171 3.60246L1.40382 16.3319H5.64804L5.76268 12.2744L8.82067 10.645L10.0888 16.3319ZM6.01303 3.4952L9.50152 3.3811L9.89927 6.59655L5.87265 8.45188L6.01303 3.4952Z',
                      fill: 'white'
                    })
                  ),
                  et('path', {
                    id: 'Vector_7',
                    d:
                      'M15.4226 24.5206H17.2546L15.8064 19.3015H11.5083L10.0601 24.5206H11.892L12.1751 23.2038L15.1489 23.2403L15.4226 24.5206ZM12.3974 22.1677L12.7647 20.4607L14.5312 20.3832L14.9009 22.097L12.3974 22.1677Z',
                    fill: 'white'
                  })
                )
              );
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              return et(
                'svg',
                {
                  width: '109',
                  height: '35',
                  viewBox: '0 0 109 35',
                  fill: 'none',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et('path', {
                  d: 'M17.7136 31.6814L16.1378 27.5918L31.3569 28.3812L30.6733 34.9265L17.7136 31.6814Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M21.3595 0L0.0616455 10.143L3.35453 25.427H7.70011L6.9491 14.4946L7.61666 14.2843L10.2291 25.427H14.7416L14.5747 12.1101L15.2423 11.8998L17.5242 25.427H22.3031L23.4296 9.34061L24.0971 9.13031L25.2782 25.427H31.5976L33.8217 3.05746L21.3595 0Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M105.462 31.1253L99.4029 30.09L99.4286 29.2908L105.228 29.074L105.08 27.5469H97.6249L97.2429 31.3809L103.405 32.4615L103.434 33.1668L97.0215 33.4192L96.8642 34.9463H105.838L105.462 31.1253ZM47.5673 30.5526L47.4742 31.9374L49.6823 32.0118L49.6534 33.416L45.035 33.2801L45.2372 29.2196L51.5887 29.0708L51.4956 27.5437H43.2923L42.5573 34.9431H51.9032L51.6625 30.4815L47.5673 30.5526ZM87.1332 33.2704L87.0883 31.9956L91.4595 31.9439L91.5013 30.5526L87.0369 30.4976L86.992 29.2067L92.8749 29.0676L92.923 27.5437H84.9893L84.7518 34.9431H93.0707L93.0129 33.416L87.1332 33.2704Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M80.7274 34.9431L79.9924 27.5437H77.8806L74.8445 31.0703L71.8116 27.5437H69.6998L68.9648 34.9431H71.4457L71.6832 30.145L74.8445 33.416L78.009 30.145L78.2465 34.9431H80.7274Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d: 'M70.2996 0.453735H65.3346L64.882 23.3313H70.8805L70.2996 0.453735Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M90.4879 0.461792H75.0088L74.4055 23.3296H91.5887L90.4879 0.461792ZM85.6866 18.3536L80.2081 18.5315L80.0252 5.07548L85.754 5.25019L85.6866 18.3536Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M109 5.4589L108.448 0.453735H93.1935L93.3219 4.91212L98.2035 5.0836L98.7009 23.3313H104.712L102.915 5.2486L109 5.4589Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M55.6236 23.3313H62.1387L57.8927 13.0621L61.2818 11.1953L58.9646 0.453735H42.3301L41.7845 5.41037L45.5042 5.28419L43.7101 23.3313H49.5321L49.6893 17.5787L53.8841 15.2686L55.6236 23.3313ZM50.0327 5.13213L54.818 4.97036L55.3636 9.52904L49.8402 12.1594L50.0327 5.13213Z',
                  fill: '#E6E6E6'
                }),
                et('path', {
                  d:
                    'M62.9415 34.9431H65.4545L63.4679 27.5437H57.5721L55.5855 34.9431H58.0985L58.4868 33.0762L62.566 33.128L62.9415 34.9431ZM58.7917 31.6074L59.2956 29.1873L61.7187 29.0773L62.2258 31.5071L58.7917 31.6074Z',
                  fill: '#E6E6E6'
                })
              );
            }
          },
          {
            key: 'getStyles',
            value: function () {
              return Te;
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return '#D13639';
            }
          }
        ]),
        e
      );
    })(Ce),
    Ne =
      '/* Style overrides for TFT theme */\n/* Breakpoints */\n/* Common Mixins */\n.theme-button {\n  background-color: #0bc4e2 !important;\n}\n.theme-button:hover {\n  background-color: #00b2cf !important;\n}\n.theme-button:active {\n  background-color: #00a0ba !important;\n}\n\n#riotbar-bar.tft-theme .riotbar-active-link, #riotbar-bar.tft-theme .riotbar-navbar-link:hover {\n  border-color: #0bc4e2;\n}\n#riotbar-bar.tft-theme #riotbar-navbar {\n  left: 200px;\n}\n#riotbar-bar.tft-theme .nav-dropdown-active a {\n  border-color: #0bc4e2;\n}\n#riotbar-bar.tft-theme .riotbar-navbar-subnav-menu .riotbar-active-sub-item {\n  border-bottom-color: #0bc4e2;\n}\n#riotbar-bar.tft-theme .sub-menu-header-icon svg {\n  margin-top: 20px;\n  margin-left: 15px;\n  display: block;\n}\n\n@media screen and (max-width: 1024px) {\n  #riotbar-bar.tft-theme .riotbar-explore-label svg {\n    display: block;\n    margin-top: 22px;\n  }\n}',
    He = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'getName',
            value: function () {
              return 'tft';
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return 'tft';
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return et(
                'svg',
                {
                  width: '33',
                  height: '32',
                  viewBox: '0 0 582.1 537.9',
                  fill: 'none',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et(
                  'linearGradient',
                  {
                    id: 'riotbar_theme_tft_bug_render',
                    gradientUnits: 'userSpaceOnUse',
                    x1: '313.4',
                    y1: '553.1069',
                    x2: '313.4',
                    y2: '15.293'
                  },
                  et('stop', { offset: '0', style: 'stop-color:#D5893A' }),
                  et('stop', { offset: '1', style: 'stop-color:#FBB63F' })
                ),
                et('path', {
                  style: 'fill: url(#riotbar_theme_tft_bug_render)',
                  d:
                    'M381.5,513.8l-0.3,0.2c0.1,0,0.2,0,0.2,0.1l-68,39.1l0,0l0,0L80.6,418.7v-80.1c0,0,0,0,0,0v-0.7c5.9,4.8,14,10.8,25,17.6v0.6c0,0,0,0,0,0v47.5l0,0L267,497.3V464C332,504,381.2,513.7,381.5,513.8z M579.3,404.3c0,0,57.5,104.3-93.3,103.9c-61.3-0.2-119.8-14.4-169.4-32.3c-85.7-30.7-144.7-72.4-144.7-72.4c0.5,0.2,1,0.4,1.5,0.6c-0.8-0.5-1.3-0.9-1.4-1c34.3,13.8,65.9,25.1,95.1,34.3v-20.9c-79.6-26.9-139.9-65.6-180.8-98.1c0.1-0.2,0.2-0.3,0.2-0.5c-18.7-14.9-33.4-28.5-43.9-39C29.2,265.5,22.4,257,22.4,257l22,4.3c-28.7-64,35.6-90.2,36-90.4V149l0,0L313.4,15.3l232.9,135.1v203.8c36.8,24.7,58,59.8,58,59.8L579.3,404.3z M546.3,419.3l-65.1,37.3c-19.7,2.5-43.9,0.8-48.7-0.9l88.7-51v-240L313.4,44.2L105.6,163.6l0,0v97.9c-19.7-20.5-24.9-44.5-24.9-44.5s0,0,0,0c0,0.2,0,0.4,0,0.7V217c-1,0.6-23.8,15.1,10.4,62.1c44.8,61.4,146.1,119.4,176.1,135.7V255.9H166.4l-37.1-82.7h368.1l-37.1,82.7H359.8v205.9c84.2,18.1,145.1,13,171.8,3.4C571.6,450.8,546.3,419.3,546.3,419.3z'
                })
              );
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              return et(
                'svg',
                {
                  width: '114',
                  height: '35',
                  viewBox: '0 0 899.7 275.6',
                  fill: 'none',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et(
                  'linearGradient',
                  {
                    id: 'riotbar_theme_tft_wordmark_render',
                    gradientUnits: 'userSpaceOnUse',
                    x1: '448.7424',
                    y1: '278.3945',
                    x2: '448.7424',
                    y2: '-10.7844'
                  },
                  et('stop', { offset: '0', style: 'stop-color:#D58A3A' }),
                  et('stop', { offset: '1', style: 'stop-color:#FCB740' })
                ),
                et('path', {
                  style: 'fill:url(#riotbar_theme_tft_wordmark_render)',
                  d:
                    'M790.7,149.9h2.2v8h-1.4v-6.2h0l-2.4,6.2H788l-2.4-6.2h0v6.2h-1.5v-8h2.2l2.2,5.7L790.7,149.9z M776,151.2h2.4v6.7h1.5v-6.7h2.4v-1.3H776V151.2z M521.5,260.2h28.8V149.9h-28.8V260.2z M521.5,124.3h28.8V14h-28.8V124.3z M619.9,234.5c-16.2,0-27.1-11.8-27.1-29.5c0-17.8,10.9-29.4,27.1-29.4c9.8,0,17.6,4.2,22.3,11.5h29.6c-6.5-23.5-24.9-38.9-53-38.9c-32.4,0-55.4,23.5-55.4,56.9c0,33.4,23,56.6,55.4,56.9c30.4,0.2,48.9-16.1,53.8-41h-29.1C639.1,229.4,630.7,234.5,619.9,234.5zM734.3,192.3c-12.4-3.1-18.9-5.5-18.9-11c0-4.3,2.7-9.5,16.5-9.5c13.7,0,17.8,5.5,17.8,12.4h27.7c0-18.7-10.1-36.1-45.5-36.1c-35.4,0-46.3,17.3-46.3,33.5c0,20.2,13.7,27.4,43.8,35.4c12.6,3.4,21.3,5.9,21.3,11.1c0,5.8-7.8,9.8-18.8,9.8c-14.4,0-21.4-6.4-21.4-17.1h-27.9c0,22.7,13.7,41,49.3,41c35.3,0,49.1-14.8,49.1-35.3C781,206.2,766.2,200.3,734.3,192.3zM214,149.9H113v25.8h32.9v84.4h29v-84.4h27.6L214,149.9z M356.1,234.5c-16.2,0-27.1-11.8-27.1-29.5c0-17.8,10.9-29.4,27.1-29.4c9.8,0,17.6,4.2,22.3,11.5H408c-6.5-23.5-24.9-38.9-53-38.9c-32.4,0-55.4,23.5-55.4,56.9c0,33.4,23,56.6,55.4,56.9c30.4,0.2,48.9-16.1,53.8-41h-29.1C375.3,229.4,366.9,234.5,356.1,234.5z M674.3,71.6L674.3,71.6l-28.3,0l0,0c0,21.2-14.3,28.3-26.3,28.3c-16.2,0-27-11.8-27-29.4c0-17.8,10.9-29.4,27-29.4c9.5,0,17.1,4,21.9,10.8h29.7c-6.7-23.1-25-38.2-52.7-38.2c-32.4,0-55.4,23.4-55.4,56.8c0,33.3,23,56.8,55.4,56.8c12.7,0,21.9-2,31.1-7.3l24.5,4.3V71.6L674.3,71.6L674.3,71.6L674.3,71.6z M113,124.3h63.4L188,98.5h-46.3v-16h32.1l11.3-25.2h-43.4V39.9H188V14H5.1l11.6,25.8h35.8v104.6l29,5.4v-110h36.7L113,51.4V124.3z M191.3,124.3L235,26.9L229.7,14h25.1l49.4,110.3h-29.5l-7.5-17.4l-42.7,8.1l-3.8,9.3H191.3zM235.3,88.8l0.2,0l22.2-4.2L247.1,60L235.3,88.8z M432.8,124.3h28.7V82.5h32.1l11.3-25.2h-43.4V39.9h46.3V14h-75V124.3zM418.4,175.7H446v84.4h29v-84.4h32.9v-25.8h-101L418.4,175.7z M755.3,14v39.7l-37.2,7V14h-28.8v110.3h28.8V85.9l37.2-7v45.4h28.8V52.8l-5.2-12.9H816v110l29-5.2V39.9h35.8L892.4,14H755.3z M339.6,68.2l24.6,29.3h2l24.7-29.3l0,56.1h28.2V14h-15.4l-38.5,47.4L327.8,14h-16.6v110.3l28.4,0L339.6,68.2z M254.3,149.9l49.4,110.3h-29.5l-7.5-17.4l-42.7,8.1l-3.8,9.3h-29.5l43.7-97.4l-5.3-12.9H254.3z M257.2,220.5l-10.6-24.6l-11.8,28.8l0.2,0L257.2,220.5z'
                })
              );
            }
          },
          {
            key: 'getStyles',
            value: function () {
              return Ne;
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return '#39C7FF';
            }
          }
        ]),
        e
      );
    })(Ce),
    Ue =
      '/* Style overrides for ProjectA theme */\n/* Breakpoints */\n/* Common Mixins */\n.theme-button {\n  background-color: #ff4655 !important;\n}\n.theme-button:hover {\n  background-color: #ff4655 !important;\n}\n.theme-button:active {\n  background-color: #ff4655 !important;\n}\n\n#riotbar-bar.projecta-theme .riotbar-active-link, #riotbar-bar.projecta-theme .riotbar-navbar-link:hover {\n  border-color: #ff4655;\n}\n#riotbar-bar.projecta-theme #riotbar-navbar {\n  left: 200px;\n}\n#riotbar-bar.projecta-theme .nav-dropdown-active a {\n  border-color: #ff4655;\n}\n#riotbar-bar.projecta-theme .riotbar-navbar-subnav-menu .riotbar-active-sub-item {\n  border-bottom-color: #ff4655;\n}\n#riotbar-bar.projecta-theme .sub-menu-header-icon svg {\n  margin-top: 20px;\n  margin-left: 15px;\n  display: block;\n}\n\n@media screen and (max-width: 1024px) {\n  #riotbar-bar.projecta-theme .riotbar-explore-label svg {\n    display: block;\n    margin-top: 22px;\n  }\n}',
    Oe = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'getName',
            value: function () {
              return 'projecta';
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return 'projecta';
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return et(
                'svg',
                {
                  fill: 'none',
                  height: '35',
                  viewBox: '0 0 100 100',
                  width: '35',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et('path', {
                  d:
                    'M99.25 48.66V10.28c0-.59-.75-.86-1.12-.39l-41.92 52.4a.627.627 0 00.49 1.02h30.29c.82 0 1.59-.37 2.1-1.01l9.57-11.96c.38-.48.59-1.07.59-1.68zM1.17 50.34L32.66 89.7c.51.64 1.28 1.01 2.1 1.01h30.29c.53 0 .82-.61.49-1.02L1.7 9.89c-.37-.46-1.12-.2-1.12.39v38.38c0 .61.21 1.2.59 1.68z',
                  fill: '#fff'
                })
              );
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              return et(
                'svg',
                {
                  fill: 'none',
                  height: '35',
                  viewBox: '0 0 690 98.9',
                  width: '109',
                  xmlns: 'http://www.w3.org/2000/svg'
                },
                et('path', {
                  d:
                    'M615.11 19.15h24.69l.08 75.59c0 .97.79 1.77 1.77 1.77l14.14-.01c.98 0 1.77-.79 1.77-1.77l-.08-75.58h30.96c.91 0 1.43-1.06.85-1.77l-10.6-13.26a4.68 4.68 0 00-3.65-1.76h-59.93c-.98 0-1.77.79-1.77 1.77v13.26c0 .96.79 1.76 1.77 1.76M19.25 94.75L91.67 4.13c.57-.71.06-1.77-.85-1.77H72.71c-1.42 0-2.77.65-3.65 1.76L17.68 68.4V4.12c0-.98-.79-1.77-1.77-1.77H1.77C.79 2.35 0 3.14 0 4.12v90.62c0 .98.79 1.77 1.77 1.77H15.6c1.42 0 2.76-.65 3.65-1.76M70.31 94.75l24.91-31.17 24.91 31.17a4.685 4.685 0 003.66 1.76h13.83c.98 0 1.77-.79 1.77-1.77V4.12c0-.97-.79-1.77-1.77-1.77h-11.6c-2.84 0-5.53 1.29-7.31 3.51L47.69 94.73c-.57.71-.06 1.77.85 1.77h18.11c1.43.01 2.77-.64 3.66-1.75m51.39-66.21v41.75l-16.68-20.87 16.68-20.88zM526.07 94.73L453.65 4.11A4.68 4.68 0 00450 2.35h-13.84c-.98 0-1.77.79-1.77 1.77v90.62c0 .98.79 1.77 1.77 1.77h13.83c1.42 0 2.77-.65 3.65-1.76l24.91-31.17 24.91 31.17a4.68 4.68 0 003.65 1.76h18.11c.91 0 1.42-1.06.85-1.78m-57.33-45.31L452.05 70.3V28.54l16.69 20.88zM269.45 0c-27.3 0-49.43 22.13-49.43 49.43s22.13 49.43 49.43 49.43 49.43-22.13 49.43-49.43C318.89 22.13 296.75 0 269.45 0m0 82.06c-17.54 0-31.75-14.61-31.75-32.63 0-18.02 14.21-32.64 31.75-32.64S301.2 31.4 301.2 49.43c.01 18.02-14.21 32.63-31.75 32.63M583.38 4.12V68.4L532 4.11a4.68 4.68 0 00-3.65-1.76H514.5c-.97 0-1.77.79-1.77 1.77v43.67c0 1.06.36 2.09 1.03 2.92l14.71 18.41c.65.81 1.95.35 1.95-.68v-38l51.39 64.31a4.68 4.68 0 003.65 1.76h13.83c.98 0 1.77-.79 1.77-1.77V4.12c0-.97-.79-1.77-1.77-1.77h-14.14c-.98 0-1.77.8-1.77 1.77M410.62 23.76V4.12c0-.98-.79-1.77-1.77-1.77h-72.37c-.98 0-1.77.79-1.77 1.77v90.62c0 .98.79 1.77 1.77 1.77h14.14c.98 0 1.77-.79 1.77-1.77V19.16h40.55l-27.37 34.26c-.51.64-.51 1.56 0 2.21l31.27 39.13a4.68 4.68 0 003.65 1.76h18.11c.91 0 1.42-1.06.85-1.77l-32.14-40.21 22.28-27.84c.66-.85 1.03-1.88 1.03-2.94M162.39 96.51h41.96c1.42 0 2.77-.65 3.65-1.76l10.6-13.27c.57-.71.06-1.77-.85-1.77H178.3V4.12c0-.98-.79-1.77-1.77-1.77h-14.14c-.98 0-1.77.79-1.77 1.77v90.62c0 .97.8 1.77 1.77 1.77',
                  fill: '#fff'
                })
              );
            }
          },
          {
            key: 'getStyles',
            value: function () {
              return Ue;
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return '#ff4655';
            }
          }
        ]),
        e
      );
    })(Ce),
    De =
      '/* Style overrides for TFT theme */\n/* Breakpoints */\n/* Common Mixins */\n.theme-button {\n  background-color: #0bc4e2 !important;\n}\n.theme-button:hover {\n  background-color: #00b2cf !important;\n}\n.theme-button:active {\n  background-color: #00a0ba !important;\n}\n\n#riotbar-bar.wildrift-theme .riotbar-active-link, #riotbar-bar.wildrift-theme .riotbar-navbar-link:hover {\n  border-color: #0bc4e2;\n}\n#riotbar-bar.wildrift-theme #riotbar-navbar {\n  left: 200px;\n}\n#riotbar-bar.wildrift-theme .nav-dropdown-active a {\n  border-color: #0bc4e2;\n}\n#riotbar-bar.wildrift-theme .riotbar-navbar-subnav-menu .riotbar-active-sub-item {\n  border-bottom-color: #0bc4e2;\n}\n#riotbar-bar.wildrift-theme .sub-menu-header-icon svg, #riotbar-bar.wildrift-theme .sub-menu-header-icon a {\n  margin-top: 20px;\n  margin-left: 15px;\n  display: block;\n  width: auto;\n  height: 32px;\n}\n\n@media screen and (max-width: 1024px) {\n  #riotbar-bar.wildrift-theme .riotbar-explore-label svg {\n    display: block;\n    margin-top: 18px;\n    width: auto;\n    height: 50px;\n  }\n  #riotbar-bar.wildrift-theme .riotbar-explore-label img {\n    display: block;\n    margin-top: 16px;\n    width: auto;\n  }\n}',
    Ie = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'getName',
            value: function () {
              return 'wildrift';
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return 'wildrift';
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return et(
                'svg',
                U(
                  {
                    width: '32',
                    height: '32.5',
                    viewBox: '0 0 32 32.55',
                    fill: 'none',
                    xmlns: 'http://www.w3.org/2000/svg'
                  },
                  'viewBox',
                  '0 0 32 32.55'
                ),
                et('path', {
                  class: 'cls-1',
                  fill: '#00d6ff',
                  d:
                    'M31.56,5.17,17.12,17,23,11s.73-.74,1.63-1.62c.3-.3.1-1.63,1.19-2.49,1.44-1.15,3.28-2.38,3.26-2.39-1.52-1.52-3-1.84-5.43-2.06L14.36,12s5.21-3.52,5.44-3c.48,1.19-8.28,10.58-8.28,10.58v1.95h0l0,3.49,12.57-6.73s-6.24,2.45-6.46,1.5c-.44-1.87,13.79-9.35,13.79-9.35A2.6,2.6,0,0,1,31.5,8.1C32,7.34,32.26,6.86,31.56,5.17Z',
                  transform: 'translate(0 0.08)'
                }),
                et('path', {
                  class: 'cls-1',
                  fill: '#00d6ff',
                  d: 'M30.08,14.2c0-.29-.24-1.33-.32-1.66L22.27,17.2Z',
                  transform: 'translate(0 0.08)'
                }),
                et('path', {
                  class: 'cls-2',
                  fill: '#c89b3c',
                  d: 'M1.81,9.84a15,15,0,0,0,0,14.28Z',
                  transform: 'translate(0 0.08)'
                }),
                et('path', {
                  class: 'cls-2',
                  fill: '#c89b3c',
                  d: 'M10.13-.08H1.55L3.17,3.28V29.13L1.55,32.48H21.23L23,26.13H10.13Z',
                  transform: 'translate(0 0.08)'
                }),
                et('path', {
                  class: 'cls-2',
                  fill: '#c89b3c',
                  d:
                    'M18.84,4.84l1.49-1.9A15.05,15.05,0,0,0,15,2a15.31,15.31,0,0,0-3.54.42V4.76a12.79,12.79,0,0,1,7.35.08Z',
                  transform: 'translate(0 0.08)'
                }),
                et('path', {
                  class: 'cls-2',
                  fill: '#c89b3c',
                  d:
                    'M30,16.25l-2.28,1.13A12.61,12.61,0,0,1,24.7,25.2l-.37,1.29-.8,2.87A15,15,0,0,0,30.06,17C30.06,16.74,30.05,16.49,30,16.25Z',
                  transform: 'translate(0 0.08)'
                })
              );
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              var t = 'https://cdn.i.leagueoflegends.com/riotbar/staging/master/';
              t.length && '/' !== t[t.length - 1] && (t += '/');
              var e = null,
                n = 'ko_KR';
              return (
                'vi_VN' === n && (e = ''.concat(t, 'images/wildrift/wildRiftFull_VN_VN.png')),
                'zh_TW' === n && (e = ''.concat(t, 'images/wildrift/wildRiftFull_ZH_TW.png')),
                null !== e
                  ? et('img', { src: e, alt: 'Wild Rift' })
                  : et(
                      'svg',
                      {
                        version: '1.1',
                        id: 'Logo',
                        xmlns: 'http://www.w3.org/2000/svg',
                        xmlnsXlink: 'http://www.w3.org/1999/xlink',
                        x: '0px',
                        y: '0px',
                        viewBox: '0 0 75.6 50',
                        style: 'enable-background:new 0 0 75.6 50;',
                        xmlSpace: 'preserve'
                      },
                      et(
                        'g',
                        null,
                        et('path', {
                          fill: '#C89B3C',
                          d:
                            'M71.5,20.8c-1.3-0.3-2.4-0.6-2.4-1.7c0-0.8,0.6-1.4,1.6-1.4c1,0,1.7,0.5,2,1.5l2.5-1.2 c-0.7-1.7-2.3-2.7-4.4-2.7c-2.7,0-4.6,1.7-4.6,4c0,2.9,2.1,3.4,4,3.9c1.3,0.3,2.4,0.6,2.4,1.7c0,0.9-0.7,1.5-1.8,1.5 c-1.1,0-2-0.6-2.3-1.5l-2.5,1.2c0.7,1.7,2.5,2.7,4.7,2.7c2.9,0,4.9-1.6,4.9-4.2C75.5,21.8,73.2,21.3,71.5,20.8z'
                        }),
                        et('polygon', {
                          fill: '#C89B3C',
                          points:
                            '72.1,3.7 72.1,9.6 73.5,9.6 73.5,7.6 75,7.6 75.4,6.3 73.5,6.3 73.5,5 75.6,5 75.6,3.7 \t'
                        }),
                        et('path', {
                          fill: '#C89B3C',
                          d:
                            'M68.4,9.8c1.7,0,3.1-1.4,3.1-3.1c0-1.7-1.4-3.1-3.1-3.1c-1.7,0-3.1,1.4-3.1,3.1C65.3,8.4,66.7,9.8,68.4,9.8z M68.4,5.1c0.9,0,1.6,0.7,1.6,1.6c0,0.9-0.7,1.6-1.6,1.6c-0.9,0-1.6-0.7-1.6-1.6C66.7,5.8,67.4,5.1,68.4,5.1z'
                        }),
                        et('polygon', {
                          fill: '#C89B3C',
                          points:
                            '41.5,16.8 41.5,28.6 44.2,28.6 44.2,20 50.4,28.6 53.2,28.6 53.2,15.5 50.5,15.5 50.5,24.2 44.4,15.5 40.9,15.5 \t'
                        }),
                        et('path', {
                          fill: '#C89B3C',
                          d:
                            'M61.6,16c-0.8-0.3-2-0.5-2.6-0.5h-4.5v13.1H59c0.9,0,1.8-0.2,2.6-0.5c0.8-0.3,1.5-0.8,2.1-1.4 c0.6-0.6,1.1-1.3,1.4-2.1c0.4-0.8,0.5-1.7,0.5-2.6c0-0.9-0.2-1.8-0.5-2.6c-0.4-0.8-0.8-1.5-1.4-2.1C63.1,16.8,62.4,16.4,61.6,16z M62.5,23.6c-0.2,0.5-0.5,0.9-0.8,1.2c-0.3,0.3-0.8,0.6-1.2,0.8c-0.5,0.2-1,0.3-1.5,0.3h-1.6v-7.8h1.6c0.5,0,1,0.1,1.5,0.3 c0.5,0.2,0.9,0.5,1.2,0.8c0.3,0.3,0.6,0.8,0.8,1.2c0.2,0.5,0.3,1,0.3,1.5C62.8,22.6,62.7,23.1,62.5,23.6z'
                        }),
                        et('polygon', {
                          fill: '#C89B3C',
                          points:
                            '17.5,26 12.5,26 12.5,23.1 16.3,23.1 17,20.7 12.5,20.7 12.5,18 17.5,18 17.5,15.5 9.7,15.5 9.7,28.6 17.5,28.6 \t'
                        }),
                        et('polygon', {
                          fill: '#C89B3C',
                          points:
                            '40.2,18 40.2,15.5 32.4,15.5 32.4,28.6 40.2,28.6 40.2,26 35.3,26 35.3,23.1 39.1,23.1 39.8,20.7 35.3,20.7 35.3,18 \t'
                        }),
                        et('path', {
                          fill: '#C89B3C',
                          d:
                            'M46.2,12.1c0.4,0.5,1,0.8,1.6,1.1c0.6,0.3,1.4,0.4,2.2,0.4c0.8,0,1.6-0.1,2.2-0.4c0.7-0.3,1.2-0.6,1.7-1.1 c0.5-0.5,0.8-1,1.1-1.7c0.2-0.7,0.4-1.4,0.4-2.2l0-8.1h-2.9l0,8c0,0.9-0.2,1.6-0.6,2.1c-0.4,0.5-1,0.8-1.8,0.8 c-0.8,0-1.4-0.3-1.8-0.8c-0.4-0.5-0.6-1.2-0.6-2.1l0-7.9h-2.9l0,8.1c0,0.8,0.1,1.6,0.4,2.2C45.4,11.1,45.7,11.6,46.2,12.1z'
                        }),
                        et('path', {
                          fill: '#C89B3C',
                          d:
                            'M32.4,11.5c0.6,0.6,1.3,1.1,2.2,1.5c0.8,0.4,1.7,0.5,2.7,0.5c0.9,0,1.8-0.2,2.6-0.5c0.8-0.3,1.5-0.8,2.1-1.4 c0.6-0.6,1-1.3,1.4-2c0.2-0.6,0.4-1.2,0.5-1.8c0-0.2,0.1-1.3,0-1.8h-6.5l-1.2,2.4h4.6c-0.2,0.7-0.6,1.4-1.2,1.8 c-0.6,0.5-1.4,0.7-2.3,0.7c-0.6,0-1.1-0.1-1.6-0.3c-0.5-0.2-0.9-0.5-1.3-0.9c-0.4-0.4-0.6-0.8-0.8-1.3c-0.2-0.5-0.3-1-0.3-1.6 c0-0.6,0.1-1.1,0.3-1.6c0.2-0.5,0.5-0.9,0.9-1.3c0.4-0.4,0.8-0.7,1.3-0.9c0.5-0.2,1-0.3,1.6-0.3c0.8,0,1.4,0.2,2,0.5 c0.6,0.3,1,0.8,1.4,1.5l2.6-1.2c-0.5-1.1-1.3-1.9-2.4-2.5c-1-0.6-2.2-0.9-3.6-0.9c-0.9,0-1.8,0.2-2.7,0.5c-0.8,0.3-1.6,0.8-2.2,1.4 c-0.6,0.6-1.1,1.3-1.5,2.1c-0.4,0.8-0.6,1.7-0.6,2.6c0,0.9,0.2,1.8,0.5,2.6C31.3,10.2,31.8,10.9,32.4,11.5z'
                        }),
                        et('polygon', {
                          fill: '#C89B3C',
                          points: '0,13.2 8,13.2 8.7,10.6 3.5,10.6 3.5,0.1 0,0.1 0.7,1.4 0.7,11.8 \t'
                        }),
                        et('polygon', {
                          fill: '#C89B3C',
                          points: '0,28.6 8,28.6 8.7,26 3.5,26 3.5,15.4 0,15.4 0.7,16.8 0.7,27.2 \t'
                        }),
                        et('polygon', {
                          fill: '#C89B3C',
                          points:
                            '17.5,10.7 12.5,10.6 12.5,7.8 16.3,7.8 17,5.3 12.5,5.3 12.5,2.7 17.5,2.7 17.5,0.1 9.7,0.1 9.7,13.2 17.5,13.2 \t'
                        }),
                        et('polygon', {
                          fill: '#C89B3C',
                          points:
                            '64.4,10.7 59.4,10.6 59.4,7.8 63.3,7.8 64,5.3 59.4,5.3 59.4,2.7 64.4,2.7 64.4,0.1 56.6,0.1 56.6,13.2 64.4,13.2 \t'
                        }),
                        et('path', {
                          fill: '#C89B3C',
                          d:
                            'M21.2,13.2l1-2.6h5.2l1,2.6h3L26.2,0.1h-4L23,1.8l-4.6,11.4H21.2z M24.9,3.7l1.6,4.4h-3.3L24.9,3.7z'
                        }),
                        et('path', {
                          fill: '#C89B3C',
                          d:
                            'M24.8,21.5l-1.2,2.4h4.6c-0.2,0.7-0.6,1.4-1.2,1.8c-0.6,0.5-1.4,0.7-2.3,0.7c-0.2,0-0.5,0-0.7-0.1l-0.8,2.5 c0.5,0.1,0.9,0.1,1.5,0.2c0.9,0,1.8-0.2,2.6-0.5c0.8-0.3,1.5-0.8,2.1-1.4c0.6-0.6,1-1.3,1.4-2c0.2-0.6,0.4-1.2,0.5-1.8 c0-0.2,0.1-1.3,0-1.8H24.8z'
                        }),
                        et('path', {
                          fill: '#C89B3C',
                          d:
                            'M28.4,16.3c-1-0.6-2.2-0.9-3.6-0.9c-0.9,0-1.8,0.2-2.7,0.5c-0.8,0.3-1.6,0.8-2.2,1.4c-0.6,0.6-1.1,1.3-1.5,2.1 c-0.4,0.8-0.6,1.7-0.6,2.6c0,0.9,0.2,1.8,0.5,2.6c0.4,0.8,0.8,1.5,1.4,2.2c0.5,0.6,1.2,1,1.9,1.3c0.3-0.3,0.6-0.6,0.8-0.8l0.9-1.4 c-0.1,0-0.2-0.1-0.3-0.1c-0.5-0.2-0.9-0.5-1.3-0.9c-0.4-0.4-0.6-0.8-0.8-1.3c-0.2-0.5-0.3-1-0.3-1.6c0-0.6,0.1-1.1,0.3-1.6 c0.2-0.5,0.5-0.9,0.9-1.3c0.4-0.4,0.8-0.7,1.3-0.9c0.5-0.2,1-0.3,1.6-0.3c0.8,0,1.4,0.2,2,0.5c0.6,0.3,1,0.8,1.4,1.5l2.6-1.2 C30.3,17.8,29.5,16.9,28.4,16.3z'
                        }),
                        et(
                          'g',
                          null,
                          et(
                            'g',
                            null,
                            et('path', {
                              fill: '#00D6FF',
                              d:
                                'M20.4,37.9c-0.2,0.1-0.9,1-1.1,1.1c-0.1,0.1-0.4,0-0.5,0.1c-0.1,0.1-2.2,5.4-2.2,5.4c0.2,0.4,1,0.8,1.1,0.6 c0.4-1.1,1.6-4.3,1.5-4.1c-0.1,0.3,1.8-2.9,1.8-3C21.2,37.8,20.6,37.8,20.4,37.9L20.4,37.9z'
                            }),
                            et('path', {
                              fill: '#00D6FF',
                              d:
                                'M22.6,41l0.8-1.5l1-1.8l0.4-0.6c0,0,0.1-0.2,0.1-0.3c0-0.2-0.8,0-0.9,0c-0.1,0-0.4,0.1-0.4,0.2 c0,0-0.3,0.3-0.5,0.7c-0.2,0.4-1.1,2.1-1.7,3.6c-0.2,0.6-0.8,1.7-0.8,1.7s-0.1,0.2-0.1,0.2c0,0.2,0.2,0.7,0.4,0.9 c0.1,0,2.9-0.9,2.9-0.9c0,0,0.5-0.8,0.7-1.3c0,0-1.5,0.4-2.6,0.8L22.6,41z'
                            }),
                            et('path', {
                              fill: '#00D6FF',
                              d:
                                'M24.6,24.4l-2,3.1c-0.7,0.5-1.6,1.9-1.9,2.2c-0.5,0.5-1.3,1.9-1.8,2.5c-0.4,0.6-2.7,4.9-3.1,5.6 c-0.3,0.5-1.4,3.4-2,5.2c-0.3-1.2-0.7-3.4-0.6-4.9c0,0-0.2,0.2-0.4,0.5c-0.2,0.1-0.6,0.1-0.6,0.1c-0.6,0.4-2.5,3.3-4.1,5.7l1.7-5 l1.6-5.2l-1.1,3.2l-2.3,6.7c0.2-1,0.5-2.1,0.7-2.9c0-0.2,0.1-0.5,0.2-0.7c0,0,0.1-0.2,0.1-0.4c0.6-1.9,2.1-5.8,2.1-5.8 s0.7-2.1,0.9-2.9c0.1-0.2-1.5,2.3-1.9,2.6c-0.1,0-0.1,0.1-0.2,0.1l-1.1,3l-0.1,0l0.9-2.9c-0.2,0.2-0.4,0.4-0.6,0.8 c-0.2,0.4-0.6,1.7-1,3c-0.3,1.1-0.4,1.4-0.8,2.6c-0.2,0.8-1.1,4.2-1.3,5.3c-0.1,0.7-0.1,1.3,0,1.7C6.1,49,6.1,50,6.1,50 S9,44.3,12,41c0.4,2.4,1.1,6.1,1.1,6.1l0.5-1.6c0.1,0.2,0.2,1.5,0.3,1.4c0.1-0.1,3.3-8.8,5.2-12.1c1.7-3.6,2.7-4.9,3-5.6l0,0.1 l-0.7,1.5l-0.8,1.5L20,33.6l-0.4,1.3l0.4-0.8l2.3-5L24.6,24.4z'
                            }),
                            et('polygon', {
                              fill: '#00D6FF',
                              points: '7.7,47.6 7.1,48.9 7.5,48.1 10.5,43.4 8.5,46.3 \t\t\t'
                            }),
                            et('path', {
                              fill: '#00D6FF',
                              d:
                                'M26.5,43.5c1.2-0.4,2.4-0.9,3.4-1.7c1.3-0.9,2.1-1.9,2.8-2.7c0.4-0.5,1.1-1.6,1.1-2.7c0-1.1-0.9-1.9-2.9-1.9 c-1.3,0-2.6,0.3-2.6,0.8l-1.9,1c0,0,1.2-0.4,2.6-0.7C28.6,36,27.3,39,27.3,39S26,42,26,42.1c0.2,0.2,0.5,0.3,0.7,0.4L25.3,43 c-0.3,0.1-0.5,0.2-0.5,0.2c0,0.1,0,0.3,0,0.3l2.8-1.1l0,0.1l-2,0.8L23.8,44l-1.7,0.8l4.3-1.2C26.4,43.6,26.4,43.5,26.5,43.5 L26.5,43.5z M27.1,42.3c0.3-0.8,1.5-3.5,1.6-3.7c0.1-0.3,1.2-2.7,1.2-2.7s-0.1-0.2-0.3-0.3c0.3,0,0.5-0.1,0.8-0.1 c1,0,1.9,0.3,1.9,1.2c0,0.6-0.4,1.5-0.7,1.9C30.6,40,29.3,41.3,27.1,42.3L27.1,42.3z'
                            }),
                            et('polygon', {
                              fill: '#00D6FF',
                              points: '27.3,32.5 30.8,31.7 32.9,31.1 33.5,30.7 32.6,30.9 \t\t\t'
                            }),
                            et('path', {
                              fill: '#00D6FF',
                              d:
                                'M41.6,41.2l-0.1-0.3c0,0-1.3-1-2.1-1.6c0.6-0.3,1.3-0.6,1.5-0.8c0.9-0.6,7.8-3.6,7.6-6.5 c-0.1-0.8-1-2.6-7.3-2.1c-7.9,0.6-10.8,2.2-10.8,2.2c3.7-0.7,15.8-2,15.9,0.3c0.1,1.8-6.6,5.2-8.2,6l-0.2-0.2 c0.6-1.7,1.4-3.6,1.4-3.6s0.4-0.9,0.7-1.5c0.1-0.2-0.4,0.4-0.9,0.5c-0.4,0.1-1,0-1.3,0.7c-0.1,0.3-0.7,1.7-1.3,3.4 c-0.5,0-1.1,0.5-1.2,0.6c0,0,0.2,0.2,0.7,0.6c-0.7,1.9-1.4,3.8-1.6,4.5c-0.3,0.9-0.6,1.5-0.6,1.5s0.9,0.2,1.3-0.5 c0.3-0.6,0.8,0.1,0.8-0.1c0-0.1,0.9-2.8,1.4-4.4l6.4,5l-5.8-5c0,0,0.2,0.1,0.3,0.1c0,0,2.9,2.4,3,2.5c0.1,0,0.4,0.3,0.4,0.3 s1.3,1,1.5,0.9c0.2,0,0.3,0,0.3,0s0.1,0.3,0.2,0.3s1.6,0.6,1.6,0.6s2,1.4,2.1,1.4c0.1,0.1,0.5,0.4,0.5,0.3c0,0,0-0.1,0-0.2 L41.6,41.2z'
                            }),
                            et('path', {
                              fill: '#00D6FF',
                              d:
                                'M48.4,35.1l-0.2,0.1c-0.2,0.2-0.3,0.8-0.6,1c-0.1,0.1-0.6,0.1-0.7,0.3c-0.1,0.1-1.4,4.3-1.5,4.6 c-0.1,0.3,1,0.6,1.1,0.4c0.2-0.6,1.3-3.8,1.4-4.1c0.1-0.3,0.8-2,0.9-2.1C48.8,35.1,48.6,35,48.4,35.1L48.4,35.1z'
                            }),
                            et('path', {
                              fill: '#00D6FF',
                              d:
                                'M69,31.3c0,0-1.5-0.1-2.7-0.1c-7,0.1-15.3,0.8-15.4,1.3c0,0.2,0.3,0.4,0.3,0.5c0.1,0.2,0.2,0.6,0.4,1.1 l-0.9,2.1c-0.4,0.1-0.6,0.3-0.6,0.3c0,0.1,0.1,0.5,0.2,0.8c-0.1,0.1-0.1,0.2-0.1,0.3c-0.3,0.6-0.9,2.7-1,3 c-0.2,0.7,1.1,0.8,1.1,0.7c0.1-0.3,0.2-0.5,0.2-0.8l1.2-3.4c1.1-0.5,1.7-0.5,1.7-0.6c0-0.2,0.3-0.3,0.3-0.3s0.2-0.1,0.2-0.2 s0.3-0.1,0.5-0.2c0.2-0.1,0.1-0.2,0-0.2c-0.8,0-1.5,0.1-2.1,0.2c0.2-0.6,0.7-2,1-2.5c1-0.3,2.4-0.5,4-0.8 c-0.5,1.3-1.6,4.3-1.7,4.6c-0.2,0.4-0.3,0.6-0.3,0.7c0,0.2-0.6,1.7-0.8,2.5c-0.2,0.5,1,0.8,1.1,0.7c0.1-0.1,1.2-3.7,1.5-4.4 c0.2-0.6,1.2-3.6,1.6-4.4c2-0.3,4.1-0.5,6-0.7l0,0l-3.5,0.7l0.8,0l6.5-0.8l-3.3,0l0,0C67.3,31.4,69,31.3,69,31.3z'
                            })
                          ),
                          et(
                            'g',
                            null,
                            et('path', { fill: '#00D6FF', d: 'M70,30.8v-0.7h-0.2V30h0.6l0,0.1h-0.2v0.7H70z' }),
                            et('path', {
                              fill: '#00D6FF',
                              d:
                                'M71.4,30c0,0.3,0,0.5,0.1,0.8h-0.1c0-0.2,0-0.3,0-0.5c0-0.1,0-0.1,0-0.2h0c0,0,0,0.1,0,0.2l-0.2,0.5H71 l-0.2-0.5c0,0,0-0.1,0-0.2h0c0,0.1,0,0.1,0,0.2c0,0.1,0,0.3,0,0.5h-0.1c0-0.3,0-0.5,0.1-0.8h0.2l0.2,0.5c0,0,0,0.1,0,0.1h0 c0,0,0-0.1,0-0.1l0.2-0.5H71.4z'
                            })
                          )
                        )
                      )
                    )
              );
            }
          },
          {
            key: 'getStyles',
            value: function () {
              return De;
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return '#0BC4E2';
            }
          }
        ]),
        e
      );
    })(Ce),
    ze = [new Ee(), new Fe(), new Pe(), new Be(), new He(), new Oe(), new Ie()],
    je = (function (t) {
      function e() {
        var t;
        return B(this, e), (t = Z(this, z(e).call(this))), (t.registeredThemeClass = new Fe()), t;
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'init',
            value: function (t) {
              var e = this;
              t &&
                ze.forEach(function (n) {
                  n.getName() === t && ((e.registeredThemeClass = n), xe.appendStyles(n.getStyles()));
                });
            }
          },
          {
            key: 'getClassNamePrefix',
            value: function () {
              return this.registeredThemeClass.getClassNamePrefix();
            }
          },
          {
            key: 'getDesktopLogo',
            value: function () {
              return this.registeredThemeClass.getDesktopLogo();
            }
          },
          {
            key: 'getMobileLogo',
            value: function () {
              return this.registeredThemeClass.getMobileLogo();
            }
          },
          {
            key: 'getAccentColor',
            value: function () {
              return this.registeredThemeClass.getAccentColor();
            }
          },
          {
            key: 'getName',
            value: function () {
              return this.registeredThemeClass.getName();
            }
          }
        ]),
        e
      );
    })(Ce),
    Ge = new je(),
    Ze = {
      navigation: Re,
      global: null,
      authInfo: null,
      localeInfo: null,
      init: function () {
        (this.global = window.RiotBar.config.global),
          Re.init(),
          Ge.init(this.global.theme),
          (this.authInfo = window.RiotBar.plugins.account.extensions.getGlobalAccount());
        var t = {
          availableLanguages: [],
          localeContext: null,
          currentRegion: null,
          allLanguages: RiotBar.plugins.locale.localeUtils.getAllLanguagesWithRegions(window.RiotBar.config.locale)
        };
        window.RiotBar.config.locale &&
          ((t.localeContext = RiotBar.plugins.locale.localeUtils.getRenderContext()),
          (t.currentRegion = t.localeContext.regions.filter(function (e) {
            return e.id == t.localeContext.currentRegion;
          })[0]),
          t.currentRegion &&
            t.currentRegion.locales &&
            t.currentRegion.locales.length > 1 &&
            (t.availableLanguages = t.currentRegion.locales)),
          (this.localeInfo = t);
      }
    },
    Ve = function (t) {
      return (function (e) {
        function n(t) {
          var e;
          return B(this, n), (e = Z(this, z(n).call(this, t))), (e.refCb = e.refCb.bind(G(e))), e;
        }
        return (
          I(n, e),
          H(n, [
            {
              key: 'refCb',
              value: function (t) {
                this.el = t;
              }
            },
            {
              key: 'componentDidMount',
              value: function () {
                var e = this.props._id,
                  n = document.createElementNS('http://www.w3.org/2000/svg', 'use'),
                  r = 'https://cdn.i.leagueoflegends.com/riotbar/staging/master/';
                r.length && '/' !== r[r.length - 1] && (r += '/');
                var a = ''.concat(r, 'svg/').concat(t, '/defs/svg/sprite.defs.svg#').concat(e);
                n.setAttribute('data-href', a), this.el.appendChild(n);
              }
            },
            {
              key: 'render',
              value: function () {
                var t = this.props,
                  e = t.width,
                  n = t.height,
                  r = t.domClassName;
                return et('svg', { ref: this.refCb, width: e, height: n, className: r ? r : '' });
              }
            }
          ]),
          n
        );
      })(ot);
    },
    We = Ve('locale'),
    Ke = Ve('navigation'),
    Xe = (Ve('footer'), Ve('application-switcher')),
    Je = function (t) {
      var e = t.color;
      return et(
        'svg',
        {
          width: '8',
          height: '5',
          viewBox: '0 0 8 5',
          className: 'arrow-down',
          fill: e,
          xmlns: 'http://www.w3.org/2000/svg'
        },
        et('path', {
          d:
            'M0.707109 1.70711L3.29289 4.29289C3.68342 4.68342 4.31658 4.68342 4.70711 4.29289L7.29289 1.70711C7.92286 1.07714 7.47669 0 6.58579 0H1.41421C0.523309 0 0.0771438 1.07714 0.707109 1.70711Z'
        })
      );
    },
    qe = function (t) {
      var e = t.width,
        n = void 0 === e ? '11' : e,
        r = t.height,
        a = void 0 === r ? '9' : r;
      return et(
        'svg',
        { width: n, height: a, viewBox: '0 0 11 9', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
        et(
          'g',
          null,
          et('rect', {
            x: '2.10889',
            y: '7.03003',
            width: '10',
            height: '2',
            transform: 'rotate(-44.6688 2.10889 7.03003)'
          }),
          et('rect', {
            x: '1.42236',
            y: '3.48999',
            width: '5',
            height: '2',
            transform: 'rotate(45.3312 1.42236 3.48999)'
          })
        )
      );
    },
    $e = (function (t) {
      function e() {
        var t;
        return (
          B(this, e),
          (t = Z(this, z(e).call(this))),
          (t.state = { open: !1 }),
          (t.globeClick = t.globeClick.bind(G(t))),
          document.addEventListener('click', function (e) {
            xe.isDescendantOfClass(e.target, 'lang-switch-dropdown') ||
              xe.isDescendantOfClass(e.target, 'icon-lang-switch') ||
              t.setState({ open: !1 });
          }),
          t
        );
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'globeClick',
            value: function (t) {
              this.setState({ open: !this.state.open });
            }
          },
          {
            key: 'render',
            value: function () {
              var t = Ze.localeInfo,
                e = t.localeContext,
                n = t.allLanguages,
                r = this.state.open,
                a = window.RiotBar.config.locale.supportedLocales || [],
                o = window.RiotBar.config.locale.languages || null,
                i = [];
              if (null !== o && Array.isArray(o)) i = o;
              else
                for (var s = 0; s < a.length; s++) {
                  var l = n.find(function (t) {
                    return t.locale === a[s];
                  });
                  l && i.push(l);
                }
              return window.RiotBar.config.locale.switcherEnabled
                ? i.length < 2
                  ? null
                  : et(
                      'div',
                      { className: 'riotbar-locale riotbar-navmenu-right-icon' },
                      et(
                        'div',
                        { className: 'icon-lang-switch' },
                        et(
                          'a',
                          { onClick: this.globeClick, className: 'lang-switch-trigger' },
                          et(We, { width: '14', height: '14', _id: 'globeIcon' })
                        ),
                        et(
                          'div',
                          { className: 'lang-switch-dropdown localization-management-list '.concat(r ? 'active' : '') },
                          et(
                            'ul',
                            null,
                            i.map(function (t) {
                              return et(
                                'li',
                                { className: e.currentLocale === t.locale ? 'active' : '', key: t.locale },
                                et(
                                  'a',
                                  { href: e.currentLocale === t.locale ? void 0 : t.landingUrl },
                                  et('span', { className: 'lang-name' }, t.name),
                                  e.currentLang === t.locale
                                    ? et(We, { width: '14', height: '12', _id: 'checkSmall' })
                                    : null
                                )
                              );
                            })
                          )
                        )
                      )
                    )
                : null;
            }
          }
        ]),
        e
      );
    })(ot),
    Ye = function (t) {
      var e = t.icons;
      Ze.localeInfo;
      return et(
        'div',
        { className: 'riotbar-right-content-icons' },
        e.map(function (t, e) {
          return 'lang-switch' === t.name
            ? et($e, null)
            : et('div', { className: 'riotbar-navmenu-right-icon' }, 'Icon');
        })
      );
    },
    Qe = function () {
      return et(
        at,
        null,
        et(Ke, { width: '10', height: '5', _id: 'mainNavArrowDown', domClassName: 'non-hover arrow-down' }),
        et(Ke, { width: '10', height: '5', _id: 'mainNavArrowDownHover', domClassName: 'hover arrow-down' })
      );
    },
    tn = function () {
      return et(
        at,
        null,
        et(Ke, { width: '7', height: '7', _id: 'mainNavLinkOut', domClassName: 'inline-block link-out non-hover' }),
        et(Ke, { width: '7', height: '7', _id: 'mainNavLinkOutWhite', domClassName: 'inline-block link-out hover' })
      );
    },
    en = { id: '__moreItems__', text: '더 보기', subMenuItems: [] },
    nn = null,
    rn = null,
    an = document.fonts && 'function' == typeof window.Promise,
    on = (function (t) {
      function e(t) {
        var n;
        B(this, e), (n = Z(this, z(e).call(this, t)));
        var r = [];
        return (
          (n.state = { activeDropdownId: null, links: r, ready: !1 }),
          (n.dropdownClicked = n.dropdownClicked.bind(G(n))),
          (n.dropdownMouseOver = n.dropdownMouseOver.bind(G(n))),
          (n.dropdownMouseOut = n.dropdownMouseOut.bind(G(n))),
          (n.adjustMenu = n.adjustMenu.bind(G(n))),
          document.addEventListener('click', function (t) {
            xe.isDescendantOfClass(t.target, 'riotbar-navbar-navitem-container') ||
              n.setState({ activeDropdownId: null });
          }),
          n
        );
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'dropdownClicked',
            value: function (t, e) {
              this.state.activeDropdownId != e.id && this.setState({ activeDropdownId: e.id });
            }
          },
          {
            key: 'dropdownMouseOver',
            value: function (t) {
              this.state.activeDropdownId != t.id && this.setState({ activeDropdownId: t.id });
            }
          },
          {
            key: 'dropdownMouseOut',
            value: function (t) {
              this.state.activeDropdownId == t.id &&
                this.setState({ activeDropdownId: null == this.state.activeDropdownId });
            }
          },
          {
            key: 'componentDidMount',
            value: function () {
              var t = this;
              an
                ? Promise.race([
                    new Promise(function (t) {
                      return setTimeout(function () {
                        return t(0);
                      }, 500);
                    }),
                    this.props.fontLoaded
                  ])
                    .then(function (e) {
                      t.adjustMenu(),
                        t.setState({ ready: !0 }),
                        window.addEventListener('resize', function () {
                          return t.adjustMenu();
                        });
                    })
                    ['catch'](function (t) {
                      console && console.log(t);
                    })
                : setTimeout(function () {
                    return t.setState({ ready: !0 });
                  }, 500);
            }
          },
          {
            key: 'componentDidUpdate',
            value: function (t) {
              var e = this;
              if (
                (this.props.links && (null === t.links || 0 === t.links.length) && this.props.links.length > 0) ||
                (this.props.touchpoints &&
                  (null === t.touchpoints || 0 === t.touchpoints.length) &&
                  this.props.touchpoints.length > 0)
              ) {
                var n = [];
                (this.props.links || this.props.touchpoints || []).forEach(function (t) {
                  return n.push.apply(n, t.links);
                }),
                  n.push(en),
                  this.setState({ links: n }, function () {
                    e.adjustMenu();
                  });
              }
            }
          },
          {
            key: 'adjustMenu',
            value: function () {
              var t = document.getElementById('riotbar-navbar'),
                e = document.getElementsByClassName('riotbar-left-content')[0],
                n = document.getElementsByClassName('riotbar-right-content')[0],
                r = document.getElementsByClassName('riotbar-bar-content')[0],
                a = document.getElementsByClassName('nav-dropdown-overflow')[0],
                o = document.getElementsByClassName('riotbar-explore-label')[0];
              if (t && e && n && r && a && o && 'none' == window.getComputedStyle(o).getPropertyValue('display')) {
                if (
                  (rn ||
                    ((a.style.display = 'inline'),
                    (a.style.visibility = 'hidden'),
                    (rn = a.offsetWidth),
                    (a.style.display = null),
                    (a.style.visibility = null)),
                  !nn)
                ) {
                  nn = {};
                  for (
                    var i = document.getElementsByClassName('riotbar-navbar-navitem-container'), s = 0;
                    s < i.length;
                    s++
                  ) {
                    var l = i[s],
                      c = l.getAttribute('data-link-id');
                    nn[c] = l.offsetWidth;
                  }
                }
                var u = (t.offsetWidth, e.offsetWidth, n.offsetWidth, r.offsetWidth),
                  p = t.offsetLeft + t.offsetWidth + n.offsetWidth,
                  d = this.state.links.concat();
                if (u < p) {
                  var f = p - u,
                    h = document.getElementsByClassName('riotbar-navbar-navitem-container'),
                    b = h.length;
                  en.subMenuItems.length || (f += rn);
                  for (
                    var m = function (t) {
                        var e = h[t],
                          n = e.getAttribute('data-link-id');
                        if ('__moreItems__' == n) return 'continue';
                        f -= e.offsetWidth;
                        var r = d.filter(function (t) {
                          return t.id == n;
                        })[0];
                        (en.subMenuItems = [r].concat(W(en.subMenuItems))),
                          (d = d.filter(function (t) {
                            return t != r;
                          }));
                      },
                      g = b - 1;
                    g >= 0 && f > 0;
                    --g
                  ) {
                    m(g);
                  }
                  this.setState({ links: d, ready: !0 });
                } else if (en.subMenuItems.length) {
                  var v = u - p,
                    _ = !1;
                  1 === en.subMenuItems.length && (v += rn);
                  do {
                    var w = en.subMenuItems[0],
                      y = nn[w.id];
                    if (!(y + 10 < v)) break;
                    en.subMenuItems.shift(), (_ = !0), d.splice(d.length - 1, 0, w), (v -= y);
                  } while (v > 0 && en.subMenuItems.length);
                  _ && this.setState({ links: d, ready: !0 });
                }
              }
            }
          },
          {
            key: 'render',
            value: function (t) {
              var e = this,
                n = (Ze.navigation, Ze.global),
                r = [],
                a = this.state,
                o = a.activeDropdownId,
                i = a.links,
                s = a.ready,
                l = this.props,
                c = l.activeLink,
                u = l.title,
                p = [''.concat(n.theme, '-theme'), an ? 'fade-in' : '', s ? 'ready' : ''];
              return (
                i.forEach(function (t) {
                  if (!t.menuOnly) {
                    var n = (t.subMenuItems || []).length,
                      a = '__moreItems__' == t.id,
                      i = [
                        'riotbar-navbar-navitem-container',
                        o == t.id ? 'nav-dropdown-active' : '',
                        a ? 'nav-dropdown-overflow' : '',
                        a && !t.subMenuItems.length ? 'hide' : ''
                      ],
                      s = c && c.includes('.') ? c.split('.')[0] : c,
                      l = ['riotbar-navbar-link', t.id === s ? 'riotbar-active-link' : ''];
                    r.push(
                      et(
                        'div',
                        {
                          key: t.id,
                          'data-link-id': t.id,
                          className: i.join(' '),
                          onMouseEnter: n
                            ? function (n) {
                                return e.dropdownMouseOver(t);
                              }
                            : null,
                          onMouseLeave: n
                            ? function (n) {
                                return e.dropdownMouseOut(t);
                              }
                            : null
                        },
                        et(
                          'a',
                          {
                            className: l.join(' '),
                            href: n ? void 0 : t.url || void 0,
                            'data-riotbar-link-id': t.id,
                            target: t.target ? t.target : null,
                            onClick: n
                              ? function (n) {
                                  return e.dropdownClicked(n, t);
                                }
                              : null
                          },
                          t.text,
                          n || a ? et(Qe, null) : t.url && t.target && '_self' !== t.target ? et(tn, null) : null
                        ),
                        n
                          ? et(
                              'ul',
                              { className: 'riotbar-navbar-subnav-menu' },
                              t.subMenuItems.map(function (t) {
                                var e = [];
                                return (
                                  t.url || e.push('no-linkage'),
                                  t.id === c && e.push('riotbar-active-sub-item'),
                                  et(
                                    at,
                                    null,
                                    et(
                                      'li',
                                      { key: t.id, className: e.join(' ') },
                                      et(
                                        'a',
                                        {
                                          href: t.url || void 0,
                                          target: t.target || void 0,
                                          className: 'riotbar-navbar-sub-link'
                                        },
                                        t.text,
                                        (t.subMenuItems || []).length
                                          ? et(Ke, {
                                              width: '10',
                                              height: '5',
                                              _id: 'mainNavArrowDown',
                                              domClassName: 'arrow-down'
                                            })
                                          : null
                                      )
                                    ),
                                    (t.subMenuItems || []).map(function (t) {
                                      var e = ['riotbar-navbar-sub-link'];
                                      return (
                                        t.id === c && e.push('riotbar-active-link'),
                                        et(
                                          'li',
                                          { className: 'riotbar-navbar-subnav-subsubmenu' },
                                          et(
                                            'a',
                                            { href: t.url, target: t.target || void 0, className: e.join(' ') },
                                            t.text
                                          )
                                        )
                                      );
                                    })
                                  )
                                );
                              })
                            )
                          : null
                      )
                    );
                  }
                }),
                et(
                  'div',
                  { id: 'riotbar-navbar', className: p.join(' ') },
                  u
                    ? et(
                        'a',
                        {
                          className: 'riotbar-navbar-title',
                          href: u.url ? u.url : void 0,
                          onClick: function (t) {
                            return t.preventDefault();
                          },
                          style: { cursor: u.url ? 'pointer' : 'default' }
                        },
                        u.text || u
                      )
                    : null,
                  et(
                    'div',
                    { className: 'riotbar-explore-label' },
                    Ge.getMobileLogo(),
                    u
                      ? et(
                          'a',
                          {
                            className: 'riotbar-navbar-title mobile',
                            href: u.url ? u.url : void 0,
                            onClick: function (t) {
                              return t.preventDefault();
                            },
                            style: { cursor: u.url ? 'pointer' : 'default' }
                          },
                          u.text || u
                        )
                      : null
                  ),
                  et(at, null, r)
                )
              );
            }
          }
        ]),
        e
      );
    })(ot),
    sn = function (t) {
      return t.href || delete t.href, et('a', t);
    },
    ln = function (t) {
      var e = t.link,
        n = t.activeLink,
        r = t.onClick,
        a = [
          'riotbar-navmenu-link',
          (e.subMenuItems || []).length ? 'sub-menu-trigger' : '',
          t.className || '',
          n == e.id ? 'riotbar-active-link' : ''
        ];
      return et(
        sn,
        {
          className: a.join(' '),
          href: e.url || null,
          'data-riotbar-link-id': e.id || null,
          'data-ping-meta': e.id ? 'riotbar-content=navigation|riotbar-navigation='.concat(e.id) : null,
          onClick: function (n) {
            if (!r || r(n) !== !1) {
              var a = n.target,
                o = a.getAttribute('data-riotbar-account-action');
              if (o) {
                n.preventDefault();
                try {
                  RiotBar.account[o]();
                } catch (i) {
                  xe.logError(i);
                }
              } else (e.subMenuItems || []).length && (n.preventDefault(), t.openSubMenu(e.text, e.subMenuItems));
            }
          },
          'data-riotbar-account-action': e.action || null,
          target: e.target || null
        },
        e.text,
        e.icon || null,
        (e.subMenuItems || []).length
          ? et('span', { className: 'side-menu-icon' }, et(Ke, { width: '5', height: '9', _id: 'subMenuRight' }))
          : e.url && e.target && '_self' !== e.target
          ? et('span', { className: 'side-menu-icon' }, et(Ke, { width: '7', height: '7', _id: 'mainNavLinkOut' }))
          : null,
        t.rightContent || null
      );
    },
    cn = function (t) {
      var e = t.openSubMenu,
        n = Ze.localeInfo,
        r = n.localeContext,
        a = n.availableLanguages;
      if (a.length < 2) return null;
      var o = a.map(function (t) {
        var e = r.currentLang == t.lang;
        return {
          text: t.lang + ' - ' + t.languageName,
          id: t.languageName,
          url: t.landingUrl,
          className: e ? 'disabled' : '',
          rightContent: e ? et(qe, { width: '14', height: '12' }) : null
        };
      });
      return et(
        'div',
        { className: 'sideMenuIcons' },
        et(
          'a',
          {
            onClick: function (t) {
              (t.cancelBubble = !0), e('Language Select', o);
            },
            className: 'lang-switch-trigger'
          },
          et(We, { width: '14', height: '14', _id: 'globeIcon' })
        )
      );
    },
    un = function (t) {
      var e = t.categories,
        n = t.openSubMenu,
        r = t.activeTouchpoint;
      return et(
        at,
        null,
        e.map(function (t, e) {
          return et(
            'div',
            { className: 'riotbar-navmenu-category '.concat(0 === e ? 'riotbar-first-category' : '') },
            t.links.map(function (t, e) {
              return et(ln, { openSubMenu: n, link: t, activeLink: r });
            })
          );
        })
      );
    },
    pn = function (t) {
      var e = t.openSubMenu,
        n = t.sideMenuOpen,
        r = t.closeSideMenu,
        a = t.activeTouchpoint,
        o = t.links,
        i = (t.touchpoints, t.authenticatedAccountLinks),
        s = window.RiotBar.__accountUtils.accountComponentProps(),
        l = s.anonymousLinksOverride,
        c = s.anonymousLinks;
      return et(
        'div',
        {
          id: 'riotbar-navmenu-dropdown',
          className: 'riotbar-navmenu-dropdown '.concat(n ? 'riotbar-click-active' : '')
        },
        et(
          'div',
          { className: 'sub-menu-header' },
          et('span', { className: 'sub-menu-header-icon' }, Ge.getMobileLogo()),
          et(
            'a',
            { onClick: r, className: 'sub-menu-close top-sub-menu-close' },
            et(Ke, { width: '32', height: '32', _id: 'burgerNavClose' })
          )
        ),
        RiotBar.config.account.enabled && RiotBar.account.getAuthState().isAuthenticated && i
          ? et(
              'div',
              { className: 'riotbar-navmenu-category' },
              et(ln, {
                onClick: function (t) {
                  if (!xe.isDescendantOfClass(t.target, 'riotbar-locale'))
                    return t.preventDefault(), e('Account', i), !1;
                },
                link: {
                  text: RiotBar.account.getAuthState().name || '내 정보 ',
                  icon: et(Je, { color: Ge.getAccentColor() })
                },
                className: 'show-auth-sub-menu black-side-menu-option',
                rightContent: et(cn, { openSubMenu: e })
              })
            )
          : null,
        RiotBar.config.account.enabled && !RiotBar.account.getAuthState().isAuthenticated
          ? et(
              'div',
              { className: 'riotbar-navmenu-category' },
              l
                ? c.map(function (t) {
                    return et(ln, { key: t.text, link: t });
                  })
                : et(ln, {
                    link: { text: '로그인', action: 'login' },
                    rightContent: et(cn, { openSubMenu: e }),
                    className: 'black-side-menu-option'
                  })
            )
          : null,
        o
          ? et(
              'div',
              { className: 'riotbar-navmenu-links' },
              et(un, { activeTouchpoint: a, openSubMenu: e, categories: o, sideMenu: !0 })
            )
          : null
      );
    },
    dn = function (t) {
      var e = t.sideMenuOpen,
        n = t.isOpen,
        r = t.closeSubMenu,
        a = t.closeSideMenu,
        o = t.links,
        i = t.text,
        s = ['riotbar-navmenu-dropdown', n ? 'riotbar-click-active ' : '', !n && e ? 'slide-out' : ''];
      return et(
        'div',
        { id: 'riotbar-navmenu-dropdown-2', className: s.join(' ') },
        et(
          'div',
          { className: 'sub-menu-header' },
          et(
            'a',
            { onClick: r, id: 'sub-sub-menu-close', className: 'sub-menu-back sub-sub-menu-close' },
            et(Ke, { width: '5', height: '8', _id: 'subMenuBack' })
          ),
          et('span', { className: 'sub-menu-header-text' }, i),
          et(
            'a',
            { onClick: a, className: 'sub-menu-close top-sub-menu-close' },
            et(Ke, { width: '32', height: '32', _id: 'burgerNavClose' })
          )
        ),
        et(
          'div',
          { className: 'riotbar-navmenu-touchpoints' },
          et(
            'div',
            { className: 'riotbar-navmenu-category' },
            o.map(function (t) {
              return et(ln, { link: t, rightContent: t.rightContent, className: t.className });
            })
          )
        )
      );
    },
    fn = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).call(this));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'render',
            value: function () {
              var t = this.props,
                e = t.sideMenuOpen,
                n = t.closeSideMenu,
                r = (t.activeLink, t.activeTouchpoint),
                a = t.links,
                o = t.touchpoints,
                i = t.authenticatedAccountLinks;
              return et(
                at,
                null,
                et(pn, {
                  activeTouchpoint: r,
                  closeSideMenu: n,
                  sideMenuOpen: e,
                  openSubMenu: this.props.openSubMenu,
                  links: a,
                  touchpoints: o,
                  authenticatedAccountLinks: i
                }),
                et(dn, {
                  activeTouchpoint: r,
                  closeSideMenu: n,
                  sideMenuOpen: this.props.sideMenuOpen,
                  isOpen: this.props.subMenuOpen,
                  closeSubMenu: this.props.closeSubMenu,
                  text: this.props.subItemText,
                  links: this.props.subItemItems
                })
              );
            }
          }
        ]),
        e
      );
    })(ot),
    hn = [],
    bn = ue.__r,
    mn = ue.diffed,
    gn = ue.__c,
    vn = ue.unmount;
  (ue.__r = function (t) {
    bn && bn(t), (we = 0), (ye = t.__c).__H && (ye.__H.u.forEach(Tt), ye.__H.u.forEach(Bt), (ye.__H.u = []));
  }),
    (ue.diffed = function (t) {
      mn && mn(t);
      var e = t.__c;
      if (e) {
        var n = e.__H;
        n &&
          n.u.length &&
          ((1 !== hn.push(e) && ke === ue.requestAnimationFrame) ||
            (
              (ke = ue.requestAnimationFrame) ||
              function (t) {
                var e,
                  n = function () {
                    clearTimeout(r), cancelAnimationFrame(e), setTimeout(t);
                  },
                  r = setTimeout(n, 100);
                'undefined' != typeof window && (e = requestAnimationFrame(n));
              }
            )(Pt));
      }
    }),
    (ue.__c = function (t, e) {
      e.some(function (t) {
        t.__h.forEach(Tt),
          (t.__h = t.__h.filter(function (t) {
            return !t.i || Bt(t);
          }));
      }),
        gn && gn(t, e);
    }),
    (ue.unmount = function (t) {
      vn && vn(t);
      var e = t.__c;
      if (e) {
        var n = e.__H;
        n &&
          n.t.forEach(function (t) {
            return t.m && t.m();
          });
      }
    });
  var _n =
    ((function (t) {
      function e(e) {
        var n;
        return ((n = t.call(this, e) || this).isPureReactComponent = !0), n;
      }
      var n, r;
      return (
        (r = t),
        ((n = e).prototype = Object.create(r.prototype)),
        (n.prototype.constructor = n),
        (n.__proto__ = r),
        (e.prototype.shouldComponentUpdate = function (t, e) {
          return Ot(this.props, t) || Ot(this.state, e);
        }),
        e
      );
    })(ot),
    ue.vnode);
  ue.vnode = function (t) {
    t.type && t.type.t && t.ref && ((t.props.ref = t.ref), (t.ref = null)), _n && _n(t);
  };
  var wn = ue.__e;
  (ue.__e = function (t, e, n) {
    if (t.then) for (var r, a = e; (a = a.__); ) if ((r = a.__c) && r.l) return r.l(t, e.__c);
    wn(t, e, n);
  }),
    ((It.prototype = new ot()).l = function (t, e) {
      var n = this,
        r = zt(n.__v),
        a = !1,
        o = function () {
          a || ((a = !0), r ? r(i) : i());
        };
      (e.__c = e.componentWillUnmount),
        (e.componentWillUnmount = function () {
          o(), e.__c && e.__c();
        });
      var i = function () {
        --n.__u || ((n.__v.__k[0] = n.state.o), n.setState({ o: (n.__b = null) }));
      };
      n.__u++ || n.setState({ o: (n.__b = n.__v.__k[0]) }), t.then(o, o);
    }),
    (It.prototype.render = function (t, e) {
      return (
        this.__b && ((this.__v.__k[0] = Dt(this.__b)), (this.__b = null)),
        [et(ot, null, e.o ? null : t.children), e.o && t.fallback]
      );
    });
  var yn = function (t, e, n) {
    if ((++n[1] === n[0] && t.i['delete'](e), t.props.revealOrder && ('t' !== t.props.revealOrder[0] || !t.i.size)))
      for (n = t.u; n; ) {
        for (; n.length > 3; ) n.pop()();
        if (n[1] < n[0]) break;
        t.u = n = n[2];
      }
  };
  ((jt.prototype = new ot()).o = function (t) {
    var e = this,
      n = zt(e.__v),
      r = e.i.get(t);
    return (
      r[0]++,
      function (a) {
        var o = function () {
          e.props.revealOrder ? (r.push(a), yn(e, t, r)) : a();
        };
        n ? n(o) : o();
      }
    );
  }),
    (jt.prototype.render = function (t) {
      (this.u = null), (this.i = new Map());
      var e = pt(t.children);
      t.revealOrder && 'b' === t.revealOrder[0] && e.reverse();
      for (var n = e.length; n--; ) this.i.set(e[n], (this.u = [1, 0, this.u]));
      return t.children;
    }),
    (jt.prototype.componentDidUpdate = jt.prototype.componentDidMount = function () {
      var t = this;
      t.i.forEach(function (e, n) {
        yn(t, n, e);
      });
    });
  var kn = (function () {
      function t() {}
      var e = t.prototype;
      return (
        (e.getChildContext = function () {
          return this.props.context;
        }),
        (e.render = function (t) {
          return t.children;
        }),
        t
      );
    })(),
    xn = /^(?:accent|alignment|arabic|baseline|cap|clip|color|fill|flood|font|glyph|horiz|marker|overline|paint|stop|strikethrough|stroke|text|underline|unicode|units|v|vector|vert|word|writing|x)[A-Z]/;
  ot.prototype.isReactComponent = {};
  var Ln = ('undefined' != typeof Symbol && Symbol['for'] && Symbol['for']('react.element')) || 60103,
    Rn = ue.event;
  ue.event = function (t) {
    return Rn && (t = Rn(t)), (t.persist = function () {}), (t.nativeEvent = t);
  };
  var Cn = {
      configurable: !0,
      get: function () {
        return this['class'];
      }
    },
    Mn = ue.vnode;
  ue.vnode = function (t) {
    t.$$typeof = Ln;
    var e = t.type,
      n = t.props;
    if ('function' != typeof e) {
      var r, a, o;
      for (o in (n.defaultValue && (n.value || 0 === n.value || (n.value = n.defaultValue), delete n.defaultValue),
      Array.isArray(n.value) &&
        n.multiple &&
        'select' === e &&
        (pt(n.children).forEach(function (t) {
          -1 != n.value.indexOf(t.props.value) && (t.props.selected = !0);
        }),
        delete n.value),
      n))
        if ((r = xn.test(o))) break;
      if (r) for (o in ((a = t.props = {}), n)) a[xn.test(o) ? o.replace(/([A-Z0-9])/, '-$1').toLowerCase() : o] = n[o];
    }
    (n['class'] || n.className) &&
      ((Cn.enumerable = 'className' in n),
      n.className && (n['class'] = n.className),
      Object.defineProperty(n, 'className', Cn)),
      (function (e) {
        var n = t.type,
          r = t.props;
        if (r && 'string' == typeof n) {
          var a = {};
          for (var o in r)
            /^on(Ani|Tra|Tou)/.test(o) && ((r[o.toLowerCase()] = r[o]), delete r[o]), (a[o.toLowerCase()] = o);
          if (
            (a.ondoubleclick && ((r.ondblclick = r[a.ondoubleclick]), delete r[a.ondoubleclick]),
            a.onbeforeinput && ((r.onbeforeinput = r[a.onbeforeinput]), delete r[a.onbeforeinput]),
            a.onchange && ('textarea' === n || ('input' === n.toLowerCase() && !/^fil|che|ra/i.test(r.type))))
          ) {
            var i = a.oninput || 'oninput';
            r[i] || ((r[i] = r[a.onchange]), delete r[a.onchange]);
          }
        }
      })(),
      'function' == typeof e &&
        !e.m &&
        e.prototype &&
        (Vt(e.prototype, 'componentWillMount'),
        Vt(e.prototype, 'componentWillReceiveProps'),
        Vt(e.prototype, 'componentWillUpdate'),
        (e.m = !0)),
      Mn && Mn(t);
  };
  var En = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          { key: 'componentDidUpdate', value: function (t) {} },
          {
            key: 'render',
            value: function () {
              var t = this.props.card,
                e = 'riotbar-application-switcher-card-'.concat(t.type.replace('_card', ''));
              (e += ' riotbar-application-switcher-card'),
                t.isPlaceholder && (e += ' riotbar-application-switcher-card-placeholder');
              var n = null;
              return (
                t.asset && ((n = t.asset.url), n.includes('format=') || (n += '?format=pjpg&quality=85')),
                et(
                  'div',
                  { className: e },
                  et(
                    'a',
                    { href: t.link, target: '_blank' },
                    et(
                      'div',
                      { className: 'riotbar-application-switcher-card-image-wrapper' },
                      null !== n
                        ? et('img', { src: n, alt: t.title, title: t.title })
                        : et(Xe, { width: '44', height: '33', _id: 'placeholderFist' })
                    ),
                    et('div', { className: 'riotbar-application-card-title-wrapper' }, et('h4', null, t.title)),
                    this.renderPlatformAvailability()
                  )
                )
              );
            }
          },
          {
            key: 'renderPlatformAvailability',
            value: function () {
              var t = this.props.card;
              return t.isPlaceholder
                ? et('div', { className: 'riotbar-platform-availability-wrapper' })
                : t.platform_availability && 0 !== t.platform_availability.length
                ? et(
                    'div',
                    { className: 'riotbar-platform-availability-wrapper' },
                    t.platform_availability.map(function (t) {
                      return et(
                        'div',
                        { className: 'riotbar-platform-availability-icon-wrapper platform-'.concat(t.toLowerCase()) },
                        et(Xe, {
                          _id: Wt(t),
                          width: '8',
                          height: '8',
                          domClassName: 'riotbar-application-platform-icon'
                        })
                      );
                    })
                  )
                : null;
            }
          }
        ]),
        e
      );
    })(ot),
    Sn = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'render',
            value: function () {
              var t = this.props.cards;
              return (
                0 === t.length && t.push.apply(t, W(this.getDefaultCards())),
                et(
                  'div',
                  {
                    className: 'riotbar-application-switcher-cards-wrapper riotbar-switcher-cards-wrapper-selected-'.concat(
                      this.props.selectedTab.replace(/_/g, '-')
                    )
                  },
                  t.map(function (t) {
                    return et(En, { card: t, key: t.title });
                  })
                )
              );
            }
          },
          {
            key: 'getDefaultCards',
            value: function () {
              var t = [],
                e = this.props.selectedTab;
              if ('riot_games' === e) {
                for (; t.length < 2; ) t.push({ type: 'promo_card', isPlaceholder: !0 });
                for (; t.length < 6; ) t.push({ type: 'game_card', isPlaceholder: !0 });
              } else if ('explore' === e) for (; t.length < 6; ) t.push({ type: 'explore', isPlaceholder: !0 });
              return t;
            }
          }
        ]),
        e
      );
    })(ot),
    Fn = (function (t) {
      function e(t) {
        var n;
        return (
          B(this, e),
          (n = Z(this, z(e).call(this, t))),
          (n.state = {
            errorLoadingEntities: !1,
            loading: !1,
            entriesByType: {},
            selectedCards: [],
            selectedTab: 'riot_games'
          }),
          n
        );
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'getDefaultProps',
            value: function () {
              return { isOpen: !1, handleClose: function () {} };
            }
          },
          {
            key: 'componentDidMount',
            value: function () {
              this.loadEntities();
            }
          },
          {
            key: 'render',
            value: function () {
              var t = this;
              return this.props.isOpen
                ? et(
                    'div',
                    { id: 'riotbar-application-switcher', className: 'riotbar-base-element' },
                    et(Xe, {
                      width: '24',
                      height: '24',
                      _id: 'applicationSwitcherArrow',
                      domClassName: 'riotbar-application-switcher-arrow'
                    }),
                    et(
                      'div',
                      { className: 'riotbar-application-switcher-mobile-header' },
                      et(
                        'div',
                        { className: 'riotbar-application-switcher-mobile-logo-wrapper' },
                        et(Xe, { height: '32', width: '99', _id: 'riotGamesFull' })
                      ),
                      et(
                        'div',
                        {
                          className: 'riotbar-application-switcher-close',
                          onClick: function () {
                            return t.props.handleClose();
                          }
                        },
                        et(Xe, { height: '32', width: '32', _id: 'applicationSwitcherClose' })
                      )
                    ),
                    this.renderContent()
                  )
                : null;
            }
          },
          {
            key: 'renderContent',
            value: function () {
              return et(
                'div',
                { className: 'riotbar-pagewidth riotbar-subcontent', id: 'riotbar-application-switcher-content' },
                this.renderTabs(),
                et(Sn, { cards: this.state.selectedCards, selectedTab: this.state.selectedTab })
              );
            }
          },
          {
            key: 'renderTabs',
            value: function () {
              var t = this;
              return et(
                'div',
                {
                  className: 'riotbar-application-switcher-tabs-wrapper riotbar-switcher-tabs-wrapper-selected-'.concat(
                    this.state.selectedTab.replace(/_/g, '-')
                  )
                },
                et(
                  'span',
                  {
                    onClick: function () {
                      return t.handleSelectTab('riot_games');
                    },
                    className: 'riot_games' === this.state.selectedTab ? 'riotbar-application-selected-tab' : ''
                  },
                  '라이엇 게임즈'
                ),
                et(
                  'div',
                  {
                    id: 'riotbar-application-switcher-desktop-close',
                    onClick: function () {
                      return t.props.handleClose();
                    }
                  },
                  et(Xe, { width: '24', height: '24', _id: 'applicationSwitcherDesktopClose' })
                )
              );
            }
          },
          {
            key: 'handleSelectTab',
            value: function (t) {
              var e = this.state.entriesByType[t] || [];
              this.setState({ selectedTab: t, selectedCards: e });
            }
          },
          {
            key: 'loadEntities',
            value: function () {
              var t = this;
              this.state.loading ||
                this.setState({ loading: !0, errorLoadingEntities: !1 }, function () {
                  fetch(t.getManifestURL())
                    .then(function (t) {
                      return t.json();
                    })
                    .then(function (e) {
                      var n = {};
                      if (e.sections)
                        for (var r = 0; r < e.sections.length; r++) {
                          var a = e.sections[r];
                          n[a.type] = a.cards;
                        }
                      t.setState({ loading: !1, entriesByType: n }, function () {
                        t.handleSelectTab('riot_games');
                      });
                    })
                    ['catch'](function (e) {
                      console.log(e), t.setState({ loading: !1, errorLoadingEntities: !0 });
                    });
                });
            }
          },
          {
            key: 'getManifestURL',
            value: function () {
              var t = RiotBar.config.applicationSwitcher.contentManifestCDN;
              return ''.concat(t, 'ko_KR.json');
            }
          }
        ]),
        e
      );
    })(ot),
    An = Lt(null),
    Pn = Ze.navigation,
    Tn = An.Consumer,
    Bn = (function (t) {
      function e() {
        var t;
        return (
          B(this, e),
          (t = Z(this, z(e).call(this))),
          (t.state = { isApplicationSwitcherOpen: !1 }),
          (t.listenForClickClose = t.listenForClickClose.bind(G(t))),
          (t.listenForEscapeKeyClose = t.listenForEscapeKeyClose.bind(G(t))),
          (t.toggleApplicationSwitcher = t.toggleApplicationSwitcher.bind(G(t))),
          (t.closeApplicationSwitcher = t.closeApplicationSwitcher.bind(G(t))),
          t
        );
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'componentDidMount',
            value: function () {
              document.addEventListener('click', this.listenForClickClose),
                document.addEventListener('keyup', this.listenForEscapeKeyClose);
            }
          },
          {
            key: 'componentWillUnmount',
            value: function () {
              document.removeEventListener('click', this.listenForClickClose),
                document.removeEventListener('keyup', this.listenForEscapeKeyClose);
            }
          },
          {
            key: 'renderThemeLogo',
            value: function () {
              return Pn.getHomepageUrl()
                ? et('a', { href: Pn.getHomepageUrl() }, Ge.getDesktopLogo())
                : Ge.getDesktopLogo();
            }
          },
          {
            key: 'render',
            value: function () {
              var t = this,
                e = RiotBar.config.applicationSwitcher || {},
                n = e.enabled ? Pn.getHomepageUrl() || null : 'https://www.riotgames.com';
              return et(
                'div',
                {
                  id: 'riotbar-navmenu',
                  className: ''.concat(Ge.getClassNamePrefix(), '-theme riotbar-branding-switcher')
                },
                et(
                  'span',
                  { style: 'display: inline-block' },
                  et(
                    'a',
                    { className: 'riotbar-logo', href: n, onClick: this.toggleApplicationSwitcher },
                    et('span', { className: 'riot-bar-fist-logo' }),
                    e.enabled
                      ? et(Ke, { width: '8', height: '5', _id: 'mainMenuDownNonHover', domClassName: 'non-hover drop' })
                      : null,
                    e.enabled
                      ? et(Ke, { width: '8', height: '5', _id: 'mainMenuDownHover', domClassName: 'hover drop' })
                      : null
                  ),
                  et('span', { className: 'riotbar-navbar-separator main-logo-separator' }),
                  et('span', { className: 'second-logo' }, this.renderThemeLogo())
                ),
                et(Tn, null, function (n) {
                  return e.enabled
                    ? Zt(
                        et(Fn, { isOpen: t.state.isApplicationSwitcherOpen, handleClose: t.closeApplicationSwitcher }),
                        n && n.el.current ? n.el.current : document.body
                      )
                    : null;
                })
              );
            }
          },
          {
            key: 'toggleApplicationSwitcher',
            value: function (t) {
              var e = this;
              try {
                if (!RiotBar.config.applicationSwitcher.enabled) return !1;
              } catch (n) {
                return !1;
              }
              try {
                t.preventDefault();
              } catch (r) {}
              this.setState({ isApplicationSwitcherOpen: !this.state.isApplicationSwitcherOpen }, function () {
                e.state.isApplicationSwitcherOpen
                  ? ((document.body.className += ' riotbar-application-switcher-open'), e.addOverlayNode())
                  : ((document.body.className = document.body.className
                      .replace(/riotbar-application-switcher-open/g, '')
                      .trim()),
                    e.removeOverlayNode());
              });
            }
          },
          {
            key: 'closeApplicationSwitcher',
            value: function () {
              var t = this;
              this.setState({ isApplicationSwitcherOpen: !1 }, function () {
                (document.body.className = document.body.className
                  .replace(/riotbar-application-switcher-open/g, '')
                  .trim()),
                  t.removeOverlayNode();
              });
            }
          },
          {
            key: 'listenForClickClose',
            value: function (t) {
              t.defaultPrevented ||
                xe.isDescendantOfClass(t.target, 'riotbar-subcontent') ||
                xe.isDescendantOfClass(t.target, 'riotbar-branding-switcher') ||
                xe.isDescendantOfId(t.target, 'riotbar-application-switcher') ||
                this.closeApplicationSwitcher();
            }
          },
          {
            key: 'listenForEscapeKeyClose',
            value: function (t) {
              if (!t.defaultPrevented) {
                var e = t.key || t.keyCode;
                ('Esc' !== e && 'Escape' !== e && 27 !== e) || this.closeApplicationSwitcher();
              }
            }
          },
          {
            key: 'addOverlayNode',
            value: function () {
              if (!document.getElementById('riotbar-page-overlay')) {
                var t = document.createElement('div');
                (t.id = 'riotbar-page-overlay'), document.body.appendChild(t);
              }
            }
          },
          {
            key: 'removeOverlayNode',
            value: function () {
              try {
                var t = document.getElementById('riotbar-page-overlay');
                t && t.parentNode.removeChild(t);
              } catch (e) {}
            }
          }
        ]),
        e
      );
    })(ot),
    Nn = function (t) {
      var e = t.authenticatedLinks,
        n = t.accountHandler;
      return et(
        at,
        null,
        e
          ? e.map(function (t) {
              var e = ['riotbar-account-link', t.action ? 'riotbar-account-action' : ''];
              return et(
                'a',
                { className: e.join(' '), onClick: n, href: t.url || null, 'data-riotbar-account-action': t.action },
                t.text
              );
            })
          : null
      );
    },
    Hn = Lt({ isAuthenicated: !1, name: !1, tag: !1, region: null }),
    Un = function (t) {
      var e = t.anonymousLinks,
        n = t.accountHandler;
      return et(
        at,
        null,
        e
          ? e.map(function (t) {
              var e = t.id,
                r = t.action,
                a = t.text,
                o = ['riotbar-anonymous-link', r ? 'riotbar-account-action' : '', 'signup' === e ? 'theme-button' : ''];
              return et(
                'a',
                {
                  'data-riotbar-link-id': e,
                  className: o.join(' '),
                  'data-riotbar-account-action': r || null,
                  href: t.url || null,
                  onClick: n
                },
                a
              );
            })
          : null
      );
    },
    On = (function (t) {
      function e() {
        var t;
        return (
          B(this, e), (t = Z(this, z(e).call(this))), (t.state = { open: !1 }), (t.toggle = t.toggle.bind(G(t))), t
        );
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'toggle',
            value: function () {
              this.setState({ open: !this.state.open });
            }
          },
          {
            key: 'render',
            value: function () {
              var t = this.props.message,
                e = t.content.substring(0, 40),
                n = this.state.open,
                r = t.severity;
              return et(
                'div',
                {
                  id: 'message-'.concat(t.id),
                  className: 'status-message '.concat(n ? '' : 'closed'),
                  'data-ping-meta': 'riotbar-serviceStatusMessageId='
                    .concat(t.id, '|riotbar-serviceStatusMessageTitle=')
                    .concat(e)
                },
                et(
                  'div',
                  {
                    className: 'message-summary',
                    'data-ping-meta': 'riotbar-serviceStatusAction=toggleMessage',
                    onClick: this.toggle
                  },
                  et(
                    'div',
                    { className: 'message-toggle' },
                    et(
                      'svg',
                      {
                        width: '8',
                        height: '5',
                        viewBox: '0 0 8 5',
                        fill: 'none',
                        xmlns: 'http://www.w3.org/2000/svg'
                      },
                      et('path', {
                        d:
                          'M0.707109 1.70711L3.29289 4.29289C3.68342 4.68342 4.31658 4.68342 4.70711 4.29289L7.29289 1.70711C7.92286 1.07714 7.47669 0 6.58579 0H1.41421C0.523309 0 0.0771438 1.07714 0.707109 1.70711Z',
                        fill: 'white'
                      })
                    )
                  ),
                  et(
                    'div',
                    { className: 'severity-indicator' },
                    et(Ke, { width: '24', height: '24', _id: 'serviceStatus-'.concat(r) })
                  ),
                  et('div', { className: 'message-title' }, t.content)
                ),
                et(
                  'a',
                  {
                    className: 'message-text',
                    href: '//status.leagueoflegends.com/?ko_KR#kr',
                    'data-ping-meta': 'riotbar-serviceStatusAction=statusPage'
                  },
                  t.content
                )
              );
            }
          }
        ]),
        e
      );
    })(ot),
    Dn = (function (t) {
      function e() {
        var t;
        return (
          B(this, e),
          (t = Z(this, z(e).call(this))),
          (t.state = { open: !1 }),
          (t.toggle = t.toggle.bind(G(t))),
          document.addEventListener('click', function (e) {
            xe.isDescendantOfId(e.target, 'riotbar-service-status-icon') ||
              xe.isDescendantOfId(e.target, 'riotbar-service-status-messages') ||
              t.setState({ open: !1 });
          }),
          t
        );
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'toggle',
            value: function () {
              this.setState({ open: !this.state.open });
            }
          },
          {
            key: 'summarySeverity',
            value: function (t) {
              var e = { info: 1, warn: 2, error: 3 },
                n = Math.max.apply(
                  null,
                  t.map(function (t) {
                    return e[t.severity] || 1;
                  })
                ),
                r =
                  Object.keys(e).filter(function (t) {
                    return e[t] == n;
                  })[0] || 'info';
              return r;
            }
          },
          {
            key: 'render',
            value: function () {
              var t = this.props.packet,
                e = this.state.open,
                n = t || {},
                r = n.messages;
              if (!r || !r.length) return null;
              var a = this.summarySeverity(r);
              return et(
                at,
                null,
                et(
                  'div',
                  { id: 'riotbar-service-status-icon', className: 'severity-'.concat(a), onClick: this.toggle },
                  'info' == a
                    ? et(
                        at,
                        null,
                        et(Ke, { width: '24', height: '24', _id: 'serviceStatus-info', domClassName: 'base' }),
                        et(Ke, { width: '24', height: '24', _id: 'serviceStatus-info-hover', domClassName: 'hover' })
                      )
                    : null,
                  'warn' == a
                    ? et(
                        at,
                        null,
                        et(Ke, { width: '24', height: '24', _id: 'serviceStatus-warn', domClassName: 'base' }),
                        et(Ke, { width: '24', height: '24', _id: 'serviceStatus-warn-hover', domClassName: 'hover' })
                      )
                    : null,
                  'error' == a
                    ? et(
                        at,
                        null,
                        et(Ke, { width: '24', height: '24', _id: 'serviceStatus-error', domClassName: 'base' }),
                        et(Ke, { width: '24', height: '24', _id: 'serviceStatus-error-hover', domClassName: 'hover' })
                      )
                    : null
                ),
                et(
                  'div',
                  { id: 'riotbar-service-status-messages', className: e ? 'active' : '' },
                  r.map(function (t, e) {
                    return et(On, { message: t });
                  })
                )
              );
            }
          }
        ]),
        e
      );
    })(ot),
    In = (function (t) {
      function e() {
        return B(this, e), Z(this, z(e).apply(this, arguments));
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'render',
            value: function () {
              var t = this.props.serviceStatusPacket;
              return RiotBar.config.serviceStatus.enabled
                ? et(
                    'div',
                    { id: 'riotbar-service-status', 'data-ping-meta': 'riotbar-content=service-status' },
                    et(Dn, { packet: t })
                  )
                : null;
            }
          }
        ]),
        e
      );
    })(ot),
    zn = document.createEvent('Event');
  zn.initEvent('riotbar_account_authstate_change', !1, !0), xe.appendStyles(Le);
  var jn = null;
  window.Promise &&
    document.fonts &&
    (jn = new Promise(function (t) {
      document.fonts.onloadingdone = function (e) {
        e.fontfaces.filter(function (t) {
          return 'FF Mark W05' == t.family;
        }) && t(1);
      };
    }));
  var Gn = An.Provider,
    Zn = (function (t) {
      function e() {
        var t;
        B(this, e), (t = Z(this, z(e).call(this)));
        var n = window.RiotBar.config,
          r = [
            'account',
            'alerts',
            'applicationSwitcher',
            'bar',
            'cookiePolicyV2',
            'footer',
            'locale',
            'navigation',
            'ping',
            'serviceStatus'
          ].reduce(function (t, e) {
            return n[e] && !n[e].enabled && (t[e] = !0), t;
          }, {});
        return (
          (t.state = {
            sideMenuOpen: !1,
            subMenuOpen: !1,
            subItemText: '',
            subItemItems: [],
            activeLink: null,
            activeTouchpoint: null,
            hasSiteSpecificLinks: !1,
            hasTitle: !1,
            title: null,
            links: null,
            touchpoints: null,
            disabled: r,
            authenticatedAccountLinks: null,
            warningPacket: null,
            notificationsPacket: null,
            serviceStatusPacket: null
          }),
          (t.openSideMenu = t.openSideMenu.bind(G(t))),
          (t.closeSideMenu = t.closeSideMenu.bind(G(t))),
          (t.openSubMenu = t.openSubMenu.bind(G(t))),
          (t.closeSubMenu = t.closeSubMenu.bind(G(t))),
          (t.updateParentMargin = t.updateParentMargin.bind(G(t))),
          (t.rootRenderElementRef = rt()),
          (window.RiotBar.updateParentMargin = t.updateParentMargin),
          document.addEventListener('click', function (e) {
            xe.isDescendantOfClass(e.target, 'riotbar-navmenu-dropdown') ||
              xe.isDescendantOfId(e.target, 'riotbar-explore') ||
              t.closeSideMenu();
          }),
          (window.RiotBar.navigation = {
            setTitleAndLinks: t.setTitleAndLinks.bind(G(t)),
            setActiveLink: t.setActiveLink.bind(G(t)),
            setActiveTouchpoint: t.setActiveTouchpoint.bind(G(t))
          }),
          (window.RiotBar.__internalSetState = t.setState.bind(G(t))),
          (window.RiotBar.__internalShowWarning = function (e) {
            return t.setState({ warningPacket: e });
          }),
          (window.RiotBar.__internalUpdateNotificationsPacket = function (e) {
            return t.setState({ notificationsPacket: D({}, t.state.notificationsPacket || {}, {}, e) });
          }),
          (window.RiotBar.__internalSetServiceStatusPacket = function (e) {
            return t.setState({ serviceStatusPacket: e });
          }),
          t
        );
      }
      return (
        I(e, t),
        H(e, [
          {
            key: 'setTitleAndLinks',
            value: function (t, e) {
              var n = window.RiotBar.data.touchpoints.links;
              this.setState({
                hasSiteSpecificLinks: Boolean(e),
                hasTitle: Boolean(t),
                title: t,
                links: this.groupLinksByCategory(e),
                touchpoints: this.groupLinksByCategory(n)
              });
            }
          },
          {
            key: 'setActiveLink',
            value: function (t) {
              this.setState({ activeLink: t });
            }
          },
          {
            key: 'setActiveTouchpoint',
            value: function (t) {
              this.setState({ activeTouchpoint: t });
            }
          },
          {
            key: 'openSubMenu',
            value: function (t, e) {
              this.setState({ subMenuOpen: !0, subItemText: t, subItemItems: e });
            }
          },
          {
            key: 'closeSubMenu',
            value: function () {
              this.setState({ subMenuOpen: !1 });
            }
          },
          {
            key: 'openSideMenu',
            value: function (t) {
              this.setState({ sideMenuOpen: !0 });
            }
          },
          {
            key: 'closeSideMenu',
            value: function () {
              this.setState({ sideMenuOpen: !1, subMenuOpen: !1 });
            }
          },
          {
            key: 'groupLinksByCategory',
            value: function (t) {
              if (t) {
                var e = {},
                  n = [];
                return (
                  t.forEach(function (t) {
                    var r = t.category || '',
                      a = e[r];
                    a || n.push((a = e[r] = { name: r, links: [] })), a.links.push(t);
                  }),
                  n
                );
              }
            }
          },
          {
            key: 'componentDidMount',
            value: function () {
              var t = window.RiotBar.config.navigation;
              this.setActiveLink(t.activeLink),
                this.setActiveTouchpoint(null != t.activeTouchpoint ? t.activeTouchpoint : t.activeLink),
                this.setTitleAndLinks(t.title, t.links),
                (window.RiotBar.renderRoot = this.rootRenderElementRef.current),
                this.updateParentMargin();
            }
          },
          {
            key: 'componentDidUpdate',
            value: function () {
              this.updateParentMargin();
            }
          },
          {
            key: 'render',
            value: function () {
              var t = this.state,
                e = t.activeLink,
                n = t.activeTouchpoint,
                r = t.hasSiteSpecificLinks,
                a = (t.hasTitle, t.sideMenuOpen),
                o = t.title,
                i = t.disabled,
                s = t.warningPacket,
                l = t.notificationsPacket,
                c = t.serviceStatusPacket,
                u = this.state,
                p = u.links,
                d = u.touchpoints;
              if (i.bar) return null;
              var f = this.props.icons,
                h = window.RiotBar.config.mobileResponsive,
                b = [
                  'i18n-ko',
                  ''.concat(Ge.getClassNamePrefix(), '-theme riotbar-base-element'),
                  h ? 'riotbar-mobile-responsive' : 'riotbar-not-responsive',
                  a ? 'sidebar-open' : ''
                ],
                m = ['riotbar-right-content', r ? 'riotbar-show-links' : ''],
                g = i.account && r && (!p || 0 === p.length);
              if (!p && d) {
                p = [{ name: 'lol', links: [] }];
                for (var v = 0; v < d.length; v++)
                  for (var _ = 0; _ < d[v].links.length; _++) p[0].links.push(d[v].links[_]);
              }
              return et(
                'div',
                { id: 'riotbar-bar-wrapper', ref: this.rootRenderElementRef },
                et(
                  Jt,
                  null,
                  et(
                    Gn,
                    { value: { el: this.rootRenderElementRef, updateParentMargin: this.updateParentMargin } },
                    et(
                      'div',
                      { id: 'riotbar-bar', className: b.join(' '), lang: 'ko' },
                      et('div', { id: 'riotbar-left-content', className: 'riotbar-left-content' }, et(Bn, null)),
                      et(
                        'div',
                        { id: 'riotbar-bar-content', className: 'riotbar-bar-content' },
                        et(on, { fontLoaded: jn, activeLink: e, links: p, touchpoints: d, title: o })
                      ),
                      et('div', { className: 'riotbar-right-mobile-content' }, et(In, { serviceStatusPacket: c })),
                      et(
                        'div',
                        { id: 'riotbar-right-content', className: m.join(' ') },
                        g
                          ? null
                          : et(
                              'div',
                              { id: 'riotbar-mobile-nav' },
                              et(
                                'a',
                                { id: 'riotbar-explore', class: 'riotbar-explore', onClick: this.openSideMenu },
                                et(Ke, { width: '32', height: '32', _id: 'burgerNav' })
                              )
                            ),
                        et(In, { serviceStatusPacket: c }),
                        l ? et(l.Component, { packet: l }) : null,
                        et(Ye, { icons: f }),
                        et(Xt, {
                          extensions: RiotBar.account,
                          authenticatedLinks: this.state.authenticatedAccountLinks
                        }),
                        et(fn, {
                          openSubMenu: this.openSubMenu,
                          closeSideMenu: this.closeSideMenu,
                          closeSubMenu: this.closeSubMenu,
                          sideMenuOpen: this.state.sideMenuOpen,
                          subMenuOpen: this.state.subMenuOpen,
                          subItemText: this.state.subItemText,
                          subItemItems: this.state.subItemItems,
                          activeLink: e,
                          activeTouchpoint: n,
                          links: p,
                          touchpoints: d,
                          authenticatedAccountLinks: this.state.authenticatedAccountLinks
                        })
                      )
                    ),
                    et(
                      'div',
                      { id: 'riotbar-subbar', lang: 'ko', className: 'i18n-ko riotbar-base-element' },
                      et(
                        'div',
                        { id: 'riotbar-subbar-content', className: 'riotbar-pagewidth riotbar-subcontent' },
                        s ? et(s.Component, { packet: s.packet }) : null
                      )
                    )
                  )
                )
              );
            }
          },
          {
            key: 'updateParentMargin',
            value: function () {
              if (!this.rootRenderElementRef || !this.rootRenderElementRef.current) return null;
              var t = this.rootRenderElementRef.current.parentNode;
              RiotBar.config.global && RiotBar.config.global.renderInto
                ? document.body.style.marginTop && (document.body.style.marginTop = 'initial')
                : (t = document.body);
              var e = this.rootRenderElementRef.current.getBoundingClientRect().height;
              document.getElementById('riotbar-application-switcher') &&
                (e -= document.getElementById('riotbar-application-switcher').getBoundingClientRect().height),
                (t.style.marginTop = e + 'px');
            }
          }
        ]),
        e
      );
    })(ot),
    Vn = function (t, e) {
      var n = !1;
      e.hasOwnProperty('renderInto') && (n = xe.determineRenderIntoElement(e.renderInto)),
        n || ((n = document.createElement('div')), (n.className = 'riotbar-root'), document.body.appendChild(n));
      var r = [];
      Ze.init(),
        r.unshift({ name: 'lang-switch' }),
        e.hasOwnProperty('renderInto') &&
          (e.renderInto instanceof String
            ? (n = document.getElementById(e.renderInto))
            : e.renderInto instanceof Object && (n = e.renderInto)),
        kt(et(Zn, { icons: r }), n);
      var a = document.getElementById('riotbar-bar');
      (t.base = a),
        (t.barContent = document.getElementById('riotbar-bar-content')),
        (t.rightBarContent = document.getElementById('riotbar-right-content')),
        (t.subBarContent = document.getElementById('riotbar-subbar-content')),
        (document.getElementsByTagName('body')[0].className += ' riotbar-present');
    },
    Wn = S(function (t) {
      var e = [],
        n = (t.exports = {
          gatherDefaults: function (t, e) {
            return (
              xe.map(e, function (n) {
                t[n] = e[n].defaults;
              }),
              t
            );
          },
          getPluginStyles: function (t, e) {
            e.styles && t.push(e.styles);
          },
          addPluginExtensions: function (t, e, n) {
            n.extensions && (t[e] = n.extensions);
          },
          toggleActive: function (t, n) {
            var r = { trigger: t, target: n };
            e.push(r), xe.toggleClass(n, 'active');
          },
          removeActive: function (t) {
            for (var n = t.target || window.event.srcElement, r = 0; r < e.length; r++) {
              var a = e[r],
                o = a.trigger,
                i = a.target;
              i.contains(n) ||
                o.contains(n) ||
                ((i.className = i.className.replace('active', '')), e.splice(r, 1), r--);
            }
          }
        });
      xe.addEvent(document.body, 'click', n.removeActive);
    }),
    Kn =
      (Wn.gatherDefaults,
      Wn.getPluginStyles,
      Wn.addPluginExtensions,
      Wn.toggleActive,
      Wn.removeActive,
      S(function (t) {
        var e;
        try {
          e = JSON.parse(
            '{"auto2":{"authMode":"stage","name":"AUTO2","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"auto3":{"authMode":"stage","name":"AUTO3","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev1":{"authMode":"stage","name":"DEV1","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev3":{"authMode":"stage","name":"DEV3","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev5":{"authMode":"stage","name":"DEV5","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev7":{"authMode":"stage","name":"DEV7","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev8":{"authMode":"stage","name":"DEV8","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev17":{"authMode":"stage","name":"DEV17","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev20":{"authMode":"stage","name":"DEV20","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev23":{"authMode":"stage","name":"DEV23","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev28":{"authMode":"stage","name":"DEV28","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev34":{"authMode":"stage","name":"DEV34","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev36":{"authMode":"stage","name":"DEV36","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"dev37":{"authMode":"stage","name":"DEV37","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"korea2":{"authMode":"stage","name":"KOREA2","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"korea3":{"authMode":"stage","name":"KOREA3","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"garena2":{"authMode":"stage","name":"GARENA2","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"garena3":{"authMode":"stage","name":"GARENA3","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"tencent2":{"authMode":"stage","name":"TENCENT2","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"tencent3":{"authMode":"stage","name":"TENCENT3","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"riot1":{"authMode":"stage","name":"RIOT1","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"riot2":{"authMode":"stage","name":"RIOT2","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"riot3":{"authMode":"stage","name":"RIOT3","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"riot4":{"authMode":"stage","name":"RIOT4","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"riot5":{"authMode":"stage","name":"RIOT5","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox3":{"authMode":"stage","name":"SANDBOX3","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox5":{"authMode":"stage","name":"SANDBOX5","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox13":{"authMode":"stage","name":"SANDBOX13","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox14":{"authMode":"stage","name":"SANDBOX14","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox15":{"authMode":"stage","name":"SANDBOX15","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox17":{"authMode":"stage","name":"SANDBOX17","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox24":{"authMode":"stage","name":"SANDBOX24","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox25":{"authMode":"stage","name":"SANDBOX25","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox26":{"authMode":"stage","name":"SANDBOX26","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox28":{"authMode":"stage","name":"SANDBOX28","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"sandbox32":{"authMode":"stage","name":"SANDBOX32","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"staging":{"authMode":"stage","name":"STAGING","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"preprod1":{"authMode":"stage","name":"PREPROD1","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]},"preprod2":{"authMode":"stage","name":"PREPROD2","locales":["ar_AE","cs_CZ","de_DE","en_AU","el_GR","en_GB","en_PL","en_US","es_AR","es_ES","es_MX","fr_FR","hu_HU","it_IT","ja_JP","ko_KR","pl_PL","pt_BR","ro_RO","ru_RU","tr_TR"]}}'
          );
        } catch (n) {
          e = {};
        }
        for (
          var r = {
              na: ['en_US'],
              euw: ['en_GB', 'de_DE', 'es_ES', 'fr_FR', 'it_IT'],
              eune: ['en_PL', 'pl_PL', 'el_GR', 'ro_RO', 'hu_HU', 'cs_CZ'],
              lan: ['es_MX'],
              las: ['es_AR'],
              br: ['pt_BR'],
              jp: ['ja_JP'],
              ru: ['ru_RU'],
              tr: ['tr_TR'],
              oce: ['en_AU'],
              kr: ['ko_KR'],
              pbe: ['en_BE'],
              garena: ['en_PH', 'en_SGMY', 'ms_MY', 'id_ID', 'th_TH', 'vn_VN', 'zh_TW']
            },
            a = [],
            o = Object.keys(r),
            i = ['garena', 'pbe'],
            s = 0;
          s < o.length;
          s++
        )
          i.includes(o[s]) || a.push.apply(a, W(r[o[s]]));
        var l = {
            na: 'North America',
            euw: 'EU West',
            eune: 'EU Nordic & East',
            lan: 'Latin America North',
            las: 'Latin America South',
            br: 'Brazil',
            tr: 'Turkey',
            ru: 'Russia',
            oce: 'Oceania',
            kr: '한국',
            jp: '日本',
            napreprod: 'NA PreProd',
            pbe: 'PBE',
            garena: ''
          },
          c = {
            ar: 'اللغة العربية',
            en: 'English',
            de: 'Deutsch',
            es: 'Español',
            fr: 'Français',
            it: 'Italiano',
            pl: 'Polski',
            ru: 'Русский',
            pt: 'Português',
            el: 'Ελληνικά',
            ro: 'Română',
            tr: 'Türkçe',
            hu: 'Magyar',
            cs: 'Čeština',
            ko: '한국어',
            ja: '日本語',
            zh: '繁體中文',
            vn: 'Tiếng Việt',
            th: 'คนไทย',
            id: 'Indonesian',
            ms: 'Malaysian'
          },
          u = (t.exports = {
            includeInternalRegions: function () {
              r = {};
              for (var t = Object.keys(e), n = 0; n < t.length; n++) {
                var a = t[n];
                void 0 === r[a] && (r[a] = e[a].locales), void 0 === l[a] && (l[a] = e[a].name);
              }
            },
            getAllLanguagesWithRegions: function (t) {
              void 0 === t && (t = u.getDefaultLandingURLPattern());
              for (var e = [], n = Object.keys(r), a = 0; a < n.length; a++)
                for (var o = n[a], i = r[o], s = 0; s < i.length; s++) {
                  var l = i[s],
                    p = l.substr(0, 2).toLowerCase(),
                    d = c[p] || c.en,
                    f = t
                      .replace('{{lang}}', p)
                      .replace('{{region}}', o)
                      .replace('{{locale}}', l)
                      .replace('{{locale-hyphen}}', l.toLowerCase().replace('_', '-'));
                  e.push({ region: o, locale: l, name: d + ' (' + o.toUpperCase() + ')', landingUrl: f });
                }
              return e;
            },
            getLocaleMappings: function () {
              return r;
            },
            getRegionNames: function () {
              return l;
            },
            getLanguageNames: function () {
              return c;
            },
            isRegion: function (t) {
              return l.hasOwnProperty(t);
            },
            getRegionLang: function (t, e) {
              var n;
              return (n = u.supportsLanguage(t, e) ? e : r[t][0]), n.substr(0, 2);
            },
            supportsLanguage: function (t, e) {
              if (!u.isRegion(t) || !r.hasOwnProperty(t)) return !0;
              for (var n = r[t], a = e.substr(0, 2), o = 0, i = n.length; o < i; o++)
                if (0 === n[o].indexOf(a)) return !0;
              return !1;
            },
            localeForRegion: function (t, e) {
              if (!u.isRegion(t)) return 'ko_KR';
              var n = r[t];
              if (!n) return 'ko_KR';
              var a,
                o = n.length,
                i = n[0];
              if (o > 1)
                for (var s = 0; s < o; s += 1)
                  if (((a = n[s]), a.substr(0, 2) === e)) {
                    i = a;
                    break;
                  }
              return i;
            },
            getDefaultLocales: function () {
              return xe.deepOverride(
                u.getLocaleMappings(),
                {
                  kr: [
                    {
                      locale: 'ko_KR',
                      lang: 'ko',
                      languageName: '한국어',
                      landingUrl: 'https://www.leagueoflegends.co.kr/'
                    }
                  ],
                  pbe: void 0
                },
                1
              );
            },
            getDefaultLocaleCodes: function () {
              return a;
            },
            getDefaultLandingURLPattern: function () {
              return 'http://{{region}}.leagueoflegends.com/{{lang}}';
            }
          });
      })),
    Xn =
      (Kn.includeInternalRegions,
      Kn.getAllLanguagesWithRegions,
      Kn.getLocaleMappings,
      Kn.getRegionNames,
      Kn.getLanguageNames,
      Kn.isRegion,
      Kn.getRegionLang,
      Kn.supportsLanguage,
      Kn.localeForRegion,
      Kn.getDefaultLocales,
      Kn.getDefaultLocaleCodes,
      Kn.getDefaultLandingURLPattern,
      {
        config: {},
        defaults: { enabled: !0 },
        init: function (t, e, n) {
          this.config = t;
        }
      }),
    Jn =
      (Xn.config,
      Xn.defaults,
      Xn.init,
      '/* Breakpoints */\n/* Common Mixins */\n#riotbar-alerts {\n  position: relative;\n  width: 100%;\n  z-index: 10;\n  /* Any immediate elements will fade in - these are the actual alerts */\n}\n#riotbar-alerts:lang(ar) {\n  direction: rtl;\n}\n#riotbar-alerts > div, #riotbar-alerts > p, #riotbar-alerts > span {\n  animation: fadeIn 0.75s;\n}\n#riotbar-alerts a:link,\n#riotbar-alerts a:visited {\n  color: #71B5BD;\n  text-decoration: none;\n}\n#riotbar-alerts a.active,\n#riotbar-alerts a:hover,\n#riotbar-alerts a:active {\n  color: #FFFFFF;\n}\n#riotbar-alerts a.btn-gold-trim:link,\n#riotbar-alerts a.btn-gold-trim:visited {\n  /* These custom font references are actually loaded in when the `customFonts` plugin is enabled */\n  /* Main font by default */\n  font-family: "FF Mark W05";\n  color: #dec58d;\n  font-weight: bold;\n  padding: 2px 10px;\n  border-radius: 4px;\n  text-decoration: none;\n  display: inline;\n  display: inline-block;\n  cursor: pointer;\n  background-color: #151515;\n  background: linear-gradient(to bottom, #313131 0%, #000000 100%);\n  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#313131", endColorstr="#000000",GradientType=0 );\n  border: 1px solid #766e4c;\n  text-transform: uppercase;\n  margin: -2px 0 -2px 10px;\n}\n#riotbar-alerts a.btn-gold-trim:link:lang(ar),\n#riotbar-alerts a.btn-gold-trim:visited:lang(ar) {\n  font-family: "Cairo", Tahoma, sans-serif;\n}\n#riotbar-alerts a.btn-gold-trim:link:lang(ru),\n#riotbar-alerts a.btn-gold-trim:visited:lang(ru) {\n  font-family: "Neue Frutiger World W05", Tahoma, sans-serif;\n}\n#riotbar-alerts a.btn-gold-trim:link:lang(ko), #riotbar-alerts a.btn-gold-trim:link:lang(kr),\n#riotbar-alerts a.btn-gold-trim:visited:lang(ko),\n#riotbar-alerts a.btn-gold-trim:visited:lang(kr) {\n  font-family: "RixSGo", Tahoma, sans-serif;\n}\n#riotbar-alerts a.btn-gold-trim:link:lang(ar),\n#riotbar-alerts a.btn-gold-trim:visited:lang(ar) {\n  margin: -2px 10px -2px 0;\n}\n#riotbar-alerts a.btn-gold-trim.active,\n#riotbar-alerts a.btn-gold-trim:hover,\n#riotbar-alerts a.btn-gold-trim:active {\n  color: #F7DA9B;\n}'),
    qn = Object.freeze({ default: Jn }),
    $n = function (t) {
      var e = t.alerts,
        n = {
          alerts: {},
          showAlert: function (t, r) {
            if ('undefined' == typeof n.alerts[r]) {
              var a = document.createElement('div');
              (a.id = 'alert-' + r),
                (a.style.visibility = 'hidden'),
                (a.innerHTML = t),
                e.insertBefore(a, e.firstChild);
              try {
                RiotBar.updateParentMargin();
              } catch (o) {}
              xe.addEvent(window, 'load', function () {
                try {
                  RiotBar.updateParentMargin();
                } catch (t) {}
              }),
                (a.style.visibility = 'visible'),
                (n.alerts[r] = a);
            }
          },
          hideAlert: function (t) {
            var r = n.alerts[t];
            if ('undefined' != typeof r) {
              e.removeChild(r);
              try {
                RiotBar.updateParentMargin();
              } catch (a) {}
              delete n.alerts[t];
            }
          }
        };
      return (
        window.addEventListener('resize', function () {
          try {
            RiotBar.updateParentMargin();
          } catch (t) {}
        }),
        n
      );
    },
    Yn = F(qn),
    Qn = S(function (t) {
      var e = (t.exports = {
        config: {},
        styles: Yn,
        defaults: { enabled: !0 },
        init: function (t, n) {
          var r = document.createElement('div');
          (r.id = 'riotbar-alerts'),
            (r.className += ' riotbar-alerts'),
            (r.className += ' riotbar-base-element'),
            (n.alerts = r);
          var a = RiotBar.renderRoot || document.body;
          a.prepend(r), (e.extensions = $n(n));
        },
        extensions: null
      });
    }),
    tr =
      (Qn.config,
      Qn.styles,
      Qn.defaults,
      Qn.init,
      Qn.extensions,
      '/* Breakpoints */\n/* Common Mixins */\n#riotbar-application-switcher {\n  position: relative;\n  top: 0;\n  width: 100%;\n  z-index: 3000001;\n  pointer-events: none;\n  text-align: left;\n  background-color: #F9F9F9;\n  animation: fadeIn 0.25s forwards, appSwitcherSlideDown 0.2s forwards;\n}\n#riotbar-application-switcher:lang(ar) {\n  direction: rtl;\n}\n#riotbar-application-switcher .riotbar-subcontent {\n  pointer-events: auto;\n  color: #111111;\n  width: 100%;\n  padding: 0;\n}\n#riotbar-application-switcher #riotbar-application-switcher-content {\n  min-height: 320px;\n  max-height: 620px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-tabs-wrapper {\n  opacity: 0;\n  animation: fadeIn 0.25s forwards;\n  animation-delay: 0.2s;\n  margin: 0;\n  padding: 24px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-tabs-wrapper > span {\n  margin: 0 16px;\n  font-size: 24px;\n  font-weight: 950;\n  font-family: "FF Mark W05", sans-serif;\n  cursor: pointer;\n  color: #C7C7C7;\n  transition: color 0.1s linear;\n}\n#riotbar-application-switcher .riotbar-application-switcher-tabs-wrapper > span:hover {\n  color: #7E7E7E;\n}\n#riotbar-application-switcher .riotbar-application-switcher-tabs-wrapper > span.riotbar-application-selected-tab {\n  color: #333333;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper {\n  overflow-y: hidden;\n  overflow-x: auto;\n  padding: 0 32px 32px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card {\n  float: left;\n  padding: 0 0.4%;\n  -webkit-animation: fadeIn 0.25s forwards;\n  animation: fadeIn 0.25s forwards;\n  opacity: 0;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card:nth-child(1) {\n  animation-delay: 0.15s;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card:nth-child(2) {\n  animation-delay: 0.2s;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card:nth-child(3) {\n  animation-delay: 0.25s;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card:nth-child(4) {\n  animation-delay: 0.3s;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card:nth-child(5) {\n  animation-delay: 0.35s;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card:nth-child(6) {\n  animation-delay: 0.4s;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card img {\n  width: 100%;\n  height: auto;\n  border-radius: 4px;\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);\n  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);\n  display: block;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card img:hover {\n  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card > a {\n  display: block;\n  position: relative;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card h4 {\n  color: #7E7E7E;\n  font-size: 11px;\n  margin-top: 16px;\n  font-weight: 800;\n  letter-spacing: 0.03em;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card h4:lang(ar) {\n  letter-spacing: 0;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card .riotbar-platform-availability-wrapper {\n  color: #5f5c5c;\n  margin-top: 8px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card .riotbar-platform-availability-wrapper .riotbar-platform-availability-icon-wrapper {\n  float: left;\n  padding-right: 8px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card .riotbar-platform-availability-wrapper .riotbar-platform-availability-icon-wrapper .riotbar-application-platform-icon {\n  height: 11px;\n  width: 11px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card .riotbar-platform-availability-wrapper:after {\n  content: " ";\n  clear: both;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper:after {\n  content: " ";\n  clear: both;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-promo {\n  width: 25%;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-game {\n  width: 12.5%;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-explore {\n  width: 16.6%;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-explore .riotbar-application-switcher-card-image-wrapper {\n  position: relative;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-explore .riotbar-application-card-title-wrapper {\n  position: absolute;\n  bottom: 8%;\n  left: 8%;\n  right: 8%;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-explore h4 {\n  font-family: "FF Mark W05", sans-serif;\n  font-weight: 900;\n  color: #f9f9f9;\n  font-size: 20px;\n  line-height: 1.014rem;\n  text-shadow: 1px 1px 1px rgba(51, 51, 51, 0.4);\n  letter-spacing: -0.01rem;\n  transition: transform 0.15s linear;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-explore h4:lang(ar) {\n  letter-spacing: 0;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-placeholder {\n  /* Shimmer styles here */\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-placeholder .riotbar-application-switcher-card-image-wrapper {\n  width: 100%;\n  height: 250px;\n  position: relative;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-placeholder .riotbar-application-switcher-card-image-wrapper svg {\n  position: absolute;\n  left: calc(50% - 16px);\n  top: calc(50% - 22px);\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-placeholder .riotbar-application-card-title-wrapper {\n  width: 60%;\n  height: 30px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-placeholder .riotbar-platform-availability-wrapper {\n  width: 30%;\n  height: 22px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-placeholder .riotbar-application-switcher-card-image-wrapper,\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-placeholder .riotbar-application-card-title-wrapper,\n#riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-placeholder .riotbar-platform-availability-wrapper {\n  animation-name: placeholderShimmer;\n  animation-duration: 3s;\n  animation-fill-mode: forwards;\n  animation-iteration-count: infinite;\n  animation-timing-function: linear;\n  background: #7E7E7E;\n  opacity: 0.1;\n  background-image: linear-gradient(to right, #7E7E7E 0%, #C7C7C7 5%, #7E7E7E 10%, #7E7E7E 100%);\n  background-size: 800px 104px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-arrow {\n  position: absolute;\n  top: -16px;\n  left: 45px;\n}\n#riotbar-application-switcher .riotbar-application-switcher-mobile-header {\n  display: none;\n}\n#riotbar-application-switcher #riotbar-application-switcher-desktop-close {\n  position: absolute;\n  top: 30px;\n  right: 40px;\n  cursor: pointer;\n}\n\n#riotbar-page-overlay {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100vh;\n  background-color: #111111;\n  opacity: 0;\n  transition: opacity 0.15s;\n}\n\nbody.riotbar-application-switcher-open #riotbar-page-overlay {\n  z-index: 500;\n  opacity: 0.7;\n}\n\n@media (max-width: 1024px) {\n  body.riotbar-application-switcher-open {\n    overflow-y: hidden;\n  }\n\n  #riotbar-application-switcher {\n    width: 75vw;\n    height: 100vh;\n    left: -76vw;\n    top: -80px;\n    pointer-events: all;\n    animation: slideInFromLeft 0.3s forwards;\n    overflow-y: auto;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-tabs-wrapper > span {\n    font-size: 18px;\n  }\n  #riotbar-application-switcher #riotbar-application-switcher-desktop-close {\n    display: none;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-arrow {\n    display: none;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper.riotbar-switcher-cards-wrapper-selected-riot-games .riotbar-application-switcher-card {\n    margin: 12px auto;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper.riotbar-switcher-cards-wrapper-selected-explore .riotbar-application-switcher-card {\n    margin: 8px auto;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card h4 {\n    font-size: 12px;\n    margin: 6px 0px 0px;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-game {\n    width: 50%;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-promo {\n    width: 100%;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-explore {\n    width: 50%;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-explore h4 {\n    font-size: 24px;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-mobile-header {\n    display: block;\n    width: 100%;\n    position: relative;\n    padding: 24px;\n    border-bottom: 1px solid #e8e8e8;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-mobile-header:after {\n    content: " ";\n    clear: both;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-mobile-logo-wrapper {\n    display: inline-block;\n    height: 32px;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-close {\n    float: right;\n    top: 32px;\n    right: 32px;\n    cursor: pointer;\n    z-index: 300;\n  }\n}\n@media (max-width: 768px) {\n  #riotbar-application-switcher .riotbar-application-switcher-tabs-wrapper {\n    margin: 24px 24px 18px 4px;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper {\n    overflow-y: hidden;\n    overflow-x: auto;\n    padding: 0 12px 32px;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper.riotbar-switcher-cards-wrapper-selected-explore div.riotbar-application-switcher-card {\n    margin: 6px auto;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper.riotbar-switcher-cards-wrapper-selected-riot-games div.riotbar-application-switcher-card {\n    margin: 12px auto;\n    padding: 0 1.2%;\n  }\n  #riotbar-application-switcher .riotbar-application-switcher-cards-wrapper .riotbar-application-switcher-card-explore h4 {\n    font-size: 14px;\n  }\n}\n/* cards dropdown slide down effect */\n@keyframes appSwitcherSlideDown {\n  0% {\n    top: -80px;\n  }\n  100% {\n    top: 0;\n  }\n}\n@-moz-keyframes appSwitcherSlideDown {\n  0% {\n    top: -80px;\n  }\n  100% {\n    top: 0;\n  }\n}\n@-webkit-keyframes appSwitcherSlideDown {\n  0% {\n    top: -80px;\n  }\n  100% {\n    top: 0;\n  }\n}\n/* Card placeholder shimmer effect */\n@keyframes placeholderShimmer {\n  0% {\n    background-position: -600px 0;\n  }\n  100% {\n    background-position: 600px 0;\n  }\n}\n@-moz-keyframes placeholderShimmer {\n  0% {\n    background-position: -600px 0;\n  }\n  100% {\n    background-position: 600px 0;\n  }\n}\n@-webkit-keyframes placeholderShimmer {\n  0% {\n    background-position: -600px 0;\n  }\n  100% {\n    background-position: 600px 0;\n  }\n}'),
    er = Object.freeze({
      default: tr
    }),
    nr = F(er),
    rr = {
      config: {},
      styles: nr,
      defaults: {
        enabled: !0,
        contentManifestCDN: 'https://cdn.i.leagueoflegends.com/riotbar/staging/master/../content-manifests/'
      },
      init: function (t, e, n) {
        this.config = t;
      }
    },
    ar =
      (rr.config,
      rr.styles,
      rr.defaults,
      rr.init,
      S(function (t) {
        t.exports = {
          initialized: !1,
          config: {},
          defaults: { enabled: !1, settings: null },
          init: function (e) {
            xe.ensureScript('RiotBar.plugins.cookiePolicyV2Deferred', {
              url: 'https://cdn.i.leagueoflegends.com/riotbar/staging/master/ko_KR-defer-cookie-policy-v2.js'
            }).then(function () {
              if (!t.exports.initialized) {
                t.exports.initialized = !0;
                var n = window.RiotBar.plugins.cookiePolicyV2Deferred;
                n.init(e);
              }
            });
          }
        };
      })),
    or =
      (ar.initialized,
      ar.config,
      ar.defaults,
      ar.init,
      '/* Breakpoints */\n/* Common Mixins */\n#riotbar-navbar {\n  display: table;\n  font-size: 0;\n  height: 80px;\n  margin-right: 0;\n  max-width: 100%;\n  opacity: 1;\n  text-align: center;\n  position: absolute;\n  left: 280px;\n}\n@media (max-width: 1024px) {\n  #riotbar-navbar {\n    display: inline-block;\n  }\n}\n#riotbar-navbar.fade-in {\n  opacity: 0;\n}\n#riotbar-navbar.fade-in.ready {\n  opacity: 1;\n  transition: opacity 0.3s cubic-bezier(0.06, 0.81, 0, 0.98) 0s;\n}\n#riotbar-navbar .link-out, #riotbar-navbar .link-out-white {\n  margin-left: 4px;\n}\n#riotbar-navbar .riotbar-explore-label {\n  display: none;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container {\n  display: inline-block;\n  position: relative;\n  height: 80px;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container:lang(ar) {\n  direction: rtl;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container .riotbar-navbar-subnav-menu {\n  visibility: hidden;\n  position: absolute;\n  left: 0;\n  top: 80px;\n  background-color: #111111;\n  text-align: left;\n  font-size: 1rem;\n  min-width: 200px;\n  opacity: 0;\n  transition: opacity 0.5s cubic-bezier(0.06, 0.81, 0, 0.98) 0s, visibility 0s;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container .riotbar-navbar-subnav-menu .riotbar-navbar-sub-link {\n  padding: 12px 32px;\n  width: 100%;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container .riotbar-navbar-subnav-menu li {\n  color: #1d1d1d;\n  border-bottom: solid 2px #111111;\n  list-style-type: none;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container .riotbar-navbar-subnav-menu li:hover {\n  cursor: pointer;\n  background-color: #333333;\n  color: #FFFFFF;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container .riotbar-navbar-subnav-menu li.riotbar-navbar-subnav-subsubmenu {\n  padding-left: 45px;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container .riotbar-navbar-subnav-menu li.no-linkage {\n  cursor: default;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container .riotbar-navbar-subnav-menu li.no-linkage:hover {\n  background-color: #111111;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container .riotbar-navbar-external-link {\n  width: 20px;\n  height: 20px;\n  vertical-align: middle;\n  margin-bottom: 5px;\n  display: inline-block;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container svg.arrow-down {\n  display: inline-block;\n  margin-bottom: 1px;\n  margin-left: 5px;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container svg.hover {\n  display: none;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container:hover svg.non-hover, #riotbar-navbar .riotbar-navbar-navitem-container.nav-dropdown-active svg.non-hover {\n  display: none;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container:hover svg.hover, #riotbar-navbar .riotbar-navbar-navitem-container.nav-dropdown-active svg.hover {\n  display: inline-block;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container.nav-dropdown-active .riotbar-navbar-subnav-menu {\n  transition: opacity 0.5s cubic-bezier(0.06, 0.81, 0, 0.98) 0s, visibility 0s;\n  opacity: 1;\n  visibility: visible;\n}\n#riotbar-navbar .riotbar-navbar-navitem-container.nav-dropdown-overflow.hide {\n  display: none;\n}\n#riotbar-navbar a {\n  border-bottom: 2px solid transparent;\n  color: #F9F9F9;\n  display: inline-block;\n  font-size: 11px;\n  font-weight: 800;\n  line-height: 76px;\n  margin: 0 0.85em;\n  overflow: hidden;\n  padding-left: 12px;\n  padding-right: 12px;\n  padding-top: 2px;\n  letter-spacing: 0.1em;\n  text-transform: uppercase;\n  transition: color 1s cubic-bezier(0.06, 0.81, 0, 0.98), border-color 0.5s cubic-bezier(0.06, 0.81, 0, 0.98);\n}\n#riotbar-navbar a:lang(ar) {\n  letter-spacing: 0;\n}\n#riotbar-navbar a.riotbar-navbar-title {\n  font-size: 18px;\n  color: #F9F9F9;\n  line-height: 80px;\n  margin-left: 0;\n  padding-left: 0;\n  padding-top: 0;\n  border: none;\n}\n#riotbar-navbar a.riotbar-navbar-title.mobile {\n  display: none;\n}\n@media (max-width: 1024px) {\n  #riotbar-navbar a.riotbar-navbar-title {\n    display: none;\n  }\n  #riotbar-navbar a.riotbar-navbar-title.mobile {\n    display: inline-block;\n    margin-left: 20px;\n    margin-right: 0;\n    padding-right: 0;\n    margin-top: -6px;\n    vertical-align: middle;\n  }\n}\n#riotbar-navbar a.riotbar-active-link {\n  color: #F9F9F9;\n}\n#riotbar-navbar.riotbar-fade-left {\n  margin-right: 10%;\n  opacity: 0;\n}\n#riotbar-navbar.riotbar-fade-right {\n  margin-right: -10%;\n  opacity: 0;\n}\n@media (max-width: 1115px) {\n  #riotbar-navbar a {\n    margin: 0 0.6375em;\n  }\n  #riotbar-navbar .riotbar-navbar-separator.riotbar-title-separator {\n    margin-right: 0.6375em;\n  }\n}\n@media (max-width: 1024px) {\n  #riotbar-navbar {\n    position: static;\n  }\n  #riotbar-navbar .riotbar-navbar-separator, #riotbar-navbar .riotbar-navbar-link, #riotbar-navbar .riotbar-navbar-navitem-container {\n    display: none;\n  }\n  #riotbar-navbar .riotbar-explore-label {\n    display: block;\n    line-height: 80px;\n    height: 80px;\n  }\n  #riotbar-navbar .riotbar-explore-label svg {\n    display: block;\n    margin-top: 22px;\n  }\n}\n#riotbar-navbar .riotbar-navbar-sub-link {\n  border: none;\n  line-height: 1.5em;\n  margin: 0;\n  padding-left: 0;\n  padding-right: 0;\n  padding-top: 0;\n}\n\n#riotbar-navmenu {\n  color: #F9F9F9;\n  float: right;\n  font-size: 14px;\n  height: 50px;\n  margin-top: 14px;\n}\n#riotbar-navmenu .riotbar-explore {\n  cursor: pointer;\n  display: inline-block;\n  font-size: 0;\n  font-weight: bold;\n  height: 50px;\n  line-height: 1;\n  min-width: 36px;\n}\n#riotbar-navmenu .riotbar-logo {\n  position: relative;\n  display: inline-block;\n  height: 50px;\n  margin-left: 32px;\n  width: 75px;\n}\n#riotbar-navmenu .riotbar-logo .riot-bar-fist-logo {\n  width: 160px;\n  height: 160px;\n  background: url("https://cdn.i.leagueoflegends.com/riotbar/staging/master/images/navigation/fistsprite.png") 0 -2400px no-repeat;\n  transition: background-position 0.25s steps(15, end);\n  transform: scale(0.33);\n  position: absolute;\n  top: -55px;\n  left: -55px;\n}\n#riotbar-navmenu .riotbar-logo .riot-bar-fist-logo:hover, .riotbar-application-switcher-open #riotbar-navmenu .riotbar-logo .riot-bar-fist-logo {\n  background-position: 0 0;\n}\n#riotbar-navmenu .riotbar-logo svg {\n  position: absolute;\n  top: 3px;\n  left: 51px;\n}\n#riotbar-navmenu .riotbar-logo svg.hover {\n  display: none;\n}\n#riotbar-navmenu .riotbar-logo svg.drop {\n  margin-top: 22px !important;\n  margin-bottom: 0;\n  margin-left: 5px;\n}\n#riotbar-navmenu .riotbar-logo:hover svg.hover {\n  display: inline-block;\n}\n#riotbar-navmenu .riotbar-logo:hover svg.non-hover {\n  display: none;\n}\n#riotbar-navmenu .riotbar-navbar-separator.main-logo-separator {\n  margin-left: 0;\n  margin-right: 32px;\n  margin-top: 10px;\n}\n#riotbar-navmenu .second-logo {\n  display: inline-block;\n  height: 50px;\n}\n#riotbar-navmenu .second-logo svg {\n  margin-top: 9px;\n  display: block;\n}\n@media (max-width: 1024px) {\n  .riotbar-mobile-responsive #riotbar-navmenu {\n    content: "sidebar";\n  }\n  .riotbar-mobile-responsive #riotbar-navmenu .second-logo {\n    display: none;\n  }\n}\n\n#riotbar-bar {\n  border-bottom: 2px solid rgba(51, 51, 51, 0.25);\n}\n#riotbar-bar .riotbar-navmenu-category {\n  overflow: visible;\n  position: relative;\n}\n#riotbar-bar .riotbar-navmenu-category .black-side-menu-option {\n  background-color: #111111 !important;\n}\n#riotbar-bar .riotbar-navmenu-category .riotbar-category-name {\n  color: #f1e6d0;\n  font-size: 14px;\n  margin-bottom: 9px;\n  text-transform: uppercase;\n}\n#riotbar-bar .riotbar-navmenu-category.riotbar-first-category {\n  border-top: 0;\n  margin-top: 0;\n  padding-top: 0;\n}\n#riotbar-bar .riotbar-navmenu-category:before, #riotbar-bar .riotbar-navmenu-category:after {\n  content: " ";\n  display: table;\n}\n#riotbar-bar .riotbar-navmenu-category:after {\n  clear: both;\n}'),
    ir = Object.freeze({ default: or }),
    sr = F(ir),
    lr = S(function (t) {
      var e =
        ('transition' in document.documentElement.style,
        (t.exports = {
          activeLink: null,
          config: {},
          linksById: {},
          styles: sr,
          defaults: {
            enabled: !0,
            title: null,
            links: null,
            activeLink: null,
            activeTouchpoint: null,
            homepageUrl: null
          },
          init: function (t, n, r) {
            (e.theme = r.theme),
              (e.renderRegions = n),
              !t.activeTouchpoint &&
                !t.activeLink &&
                window.riotBarConfig &&
                window.riotBarConfig.touchpoints &&
                (t.activeTouchpoint = window.riotBarConfig.touchpoints.activeTouchpoint),
              (e.config = t);
          }
        }));
    }),
    cr =
      (lr.activeLink,
      lr.config,
      lr.linksById,
      lr.styles,
      lr.defaults,
      lr.init,
      '/* Breakpoints */\n/* Common Mixins */\n#riotbar-account {\n  float: left;\n  white-space: nowrap;\n}\n#riotbar-account .riotbar-anonymous-link {\n  cursor: pointer;\n  display: inline-block;\n  margin: 0 15px;\n  transition: color 300ms cubic-bezier(0.06, 0.81, 0, 0.98);\n  vertical-align: middle;\n  font-size: 11px;\n  line-height: 80px;\n}\n#riotbar-account .riotbar-anonymous-link[data-riotbar-link-id=signup] {\n  float: right;\n  height: 41px;\n  margin: 18px 0 0 17px;\n  padding: 12px 0;\n  position: relative;\n  width: 129px;\n  z-index: 0;\n  line-height: normal;\n}\n.i18n-hu #riotbar-account .riotbar-anonymous-link[data-riotbar-link-id=signup] {\n  font-size: 13px;\n  letter-spacing: -0.6px;\n}\n#riotbar-account .riotbar-anonymous-link[data-riotbar-link-id=signup]:hover:before {\n  opacity: 1;\n}\n#riotbar-account [data-riotbar-link-id=login] {\n  font-size: 11px;\n  font-weight: 800;\n  letter-spacing: 0.1em;\n  text-transform: uppercase;\n}\n#riotbar-account [data-riotbar-link-id=login]:lang(ar) {\n  letter-spacing: 0;\n}\n#riotbar-account .riotbar-summoner-name {\n  color: #F9F9F9;\n  font-size: 16px;\n  margin-top: 7px;\n  font-weight: bold;\n  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.7);\n}\n#riotbar-account .riotbar-summoner-name:lang(ar) {\n  line-height: 1.4em;\n}\n@media (max-width: 640px) {\n  .riotbar-mobile-responsive #riotbar-account .riotbar-anonymous-link {\n    display: none;\n  }\n  .riotbar-mobile-responsive #riotbar-account .riotbar-anonymous-link.riotbar-account-action {\n    display: inline-block;\n  }\n}\n\n#riotbar-account-bar {\n  cursor: pointer;\n  float: right;\n  height: 80px;\n  margin-top: 0;\n  padding-top: 23px;\n  padding-right: 16px;\n  padding-left: 16px;\n}\n#riotbar-account-bar .riotbar-summoner-info {\n  float: left;\n  margin: -3px 0 -10px 0;\n}\n#riotbar-account-bar .riotbar-summoner-info svg {\n  margin-left: 11px;\n  margin-bottom: 2px;\n  transition: transform 0.25s ease-out;\n}\n#riotbar-account-bar.active .riotbar-summoner-info svg {\n  transform: rotate(180deg);\n}\n.riotbar-not-responsive #riotbar-account-bar:hover .riotbar-summoner-info svg {\n  transform: rotate(180deg);\n}\n@media (min-width: 1025px) {\n  .riotbar-mobile-responsive #riotbar-account-bar:hover .riotbar-summoner-info svg, .riotbar-mobile-responsive #riotbar-account-bar.YES .riotbar-summoner-info svg {\n    transform: rotate(180deg);\n  }\n}\n@media (max-width: 640px) {\n  .riotbar-mobile-responsive #riotbar-account-bar {\n    border-left: none;\n  }\n  .riotbar-mobile-responsive #riotbar-account-bar .riotbar-summoner-info {\n    display: none;\n  }\n}\n\n#riotbar-account-dropdown-plugins {\n  height: 50px;\n  position: absolute;\n  right: 0;\n  top: 12px;\n}\n\n#riotbar-account-dropdown {\n  background: #111;\n  border-top: 1px solid #262626;\n  box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.5);\n  right: -5px;\n  margin: 0;\n  opacity: 0;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  text-align: left;\n  transform: translateY(-5%);\n  transition: transform 300ms, opacity 300ms;\n  top: 79px;\n  visibility: hidden;\n  z-index: 10;\n}\n#riotbar-account-dropdown .riotbar-account-info {\n  border-bottom: 1px solid #262626;\n  display: none;\n  margin-bottom: 18px;\n  padding-bottom: 18px;\n}\n#riotbar-account-dropdown .riotbar-account-info .riotbar-summoner-name {\n  max-width: 175px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n@media (max-width: 640px) {\n  .riotbar-mobile-responsive #riotbar-account-dropdown .riotbar-account-info {\n    display: block;\n  }\n}\n#riotbar-account.active #riotbar-account-dropdown {\n  opacity: 1;\n  transform: translateX(0%);\n  visibility: visible;\n  width: 200px;\n}\n@media (min-width: 1025px) {\n  #riotbar-account-dropdown {\n    width: 200px;\n  }\n  #riotbar-account:hover #riotbar-account-dropdown {\n    opacity: 1;\n    transform: translateX(0%);\n    visibility: visible;\n  }\n}\n@media (max-width: 1024px) {\n  .riotbar-mobile-responsive #riotbar-account-dropdown {\n    content: "sidebar";\n    transform: none;\n    transition: width 300ms, opacity 300ms;\n    width: 0;\n  }\n}\n\n#riotbar-account-dropdown-links {\n  font-size: 0;\n  position: relative;\n  white-space: normal;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link {\n  cursor: pointer;\n  display: block;\n  font-size: 11px;\n  line-height: 1.5em;\n  text-transform: uppercase;\n  transition: color 300ms cubic-bezier(0.06, 0.81, 0, 0.98);\n  width: 200px;\n  padding: 15px 0 15px 15px;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link:lang(ar) {\n  padding: 15px 15px 15px 0;\n  text-align: right;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link .riotbar-link-subtext {\n  color: #a09a8e;\n  display: block;\n  font-size: 11px;\n  text-transform: none;\n  /* These custom font references are actually loaded in when the `customFonts` plugin is enabled */\n  /* Main font by default */\n  font-family: "FF Mark W05";\n}\n#riotbar-account-dropdown-links a.riotbar-account-link .riotbar-link-subtext:lang(ar) {\n  font-family: "Cairo", Tahoma, sans-serif;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link .riotbar-link-subtext:lang(ru) {\n  font-family: "Neue Frutiger World W05", Tahoma, sans-serif;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link .riotbar-link-subtext:lang(ko), #riotbar-account-dropdown-links a.riotbar-account-link .riotbar-link-subtext:lang(kr) {\n  font-family: "RixSGo", Tahoma, sans-serif;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link .riotbar-link-subtext:lang(ar) {\n  text-align: right;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link:first-child {\n  margin-top: 0;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link:last-child {\n  margin-bottom: 0;\n}\n#riotbar-account-dropdown-links a.riotbar-account-link:hover {\n  background-color: #333333;\n}\n\n#riotbar-account-warning {\n  background-color: #352039;\n  color: #F9F9F9;\n  /* These custom font references are actually loaded in when the `customFonts` plugin is enabled */\n  /* Main font by default */\n  font-family: "FF Mark W05";\n  font-size: 12px;\n  font-weight: 600;\n  min-height: 20px;\n  padding: 1px 0 0;\n  position: absolute;\n  width: 100%;\n  margin: 0;\n  text-align: center;\n}\n#riotbar-account-warning:lang(ar) {\n  font-family: "Cairo", Tahoma, sans-serif;\n}\n#riotbar-account-warning:lang(ru) {\n  font-family: "Neue Frutiger World W05", Tahoma, sans-serif;\n}\n#riotbar-account-warning:lang(ko), #riotbar-account-warning:lang(kr) {\n  font-family: "RixSGo", Tahoma, sans-serif;\n}\n#riotbar-account-warning svg {\n  display: inline-block;\n  line-height: 16px;\n  vertical-align: middle;\n  margin: 0 15px 0 0;\n}\n#riotbar-account-warning svg:lang(ar) {\n  margin: 0 0 0 15px;\n}\n#riotbar-account-warning a {\n  display: inline-block;\n  line-height: 16px;\n  color: #F9F9F9;\n  padding-bottom: 3px;\n  padding-left: 10%;\n  padding-right: 10%;\n}\n#riotbar-account-warning a span {\n  margin-left: 10px;\n  color: #F9F9F9;\n}'),
    ur = Object.freeze({ default: cr }),
    pr = function (t, e) {
      function n(t, e) {
        return t + e.toUpperCase();
      }
      var r = 'kr',
        a = 'leagueoflegends.com',
        o = t.currentDomain;
      null === o && (o = xe.getCurrentDomain(a));
      var i = Kn.getRegionNames(),
        s = [r];
      xe.map(i, function (t) {
        t !== r && s.push(t);
      });
      var l = {
          token: 'PVPNET_TOKEN_',
          user: 'PVPNET_ACCT_',
          lang: 'PVPNET_LANG',
          region: 'PVPNET_REGION',
          userid: 'PVPNET_ID_',
          idHint: 'id_hint'
        },
        c = { name: !1, tag: !1, region: 'kr', isAuthenticated: !1 },
        u = Object.assign({}, c),
        p = {
          cookies: {
            token: n(l.token, r),
            user: n(l.user, r),
            lang: l.lang,
            region: l.region,
            userid: n(l.userid, r),
            idHint: l.idHint
          },
          login: function (n) {
            var r = n || t.returnUrl || xe.window.location.href,
              a = encodeURIComponent(r);
            xe.window.location = 'https://stage.auth.kr.riotgames.com/rso/login?returnUrl=' + a;
            // xe.window.location = e.urls.login
            //   .replace('{{login_prefix}}', qt())
            //   .replace('{{current_domain}}', 'leagueoflegends.com')
            //   .replace('{{then}}', a);
          },
          logout: function () {
            var t = encodeURIComponent(xe.window.location.href),
              n = p.getGlobalAccount();

            xe.window.location = 'https://stage.auth.kr.riotgames.com/rso/logon-terminate?returnUrl=' + t;
            // xe.window.location = e.urls.logout
            //   .replace('{{summonerRegion}}', n.region)
            //   .replace('{{login_prefix}}', qt())
            //   .replace('{{current_domain}}', 'leagueoflegends.com')
            //   .replace('{{then}}', t);
          },
          recoverPassword: function () {
            xe.window.location = e.urls.passwordRecovery;
          },
          recoverUsername: function () {
            xe.window.location = e.urls.usernameRecovery;
          },
          management: function () {
            xe.window.location = e.urls.management;
          },
          getRegion: function () {
            return r;
          },
          getGlobalAccount: function () {
            var t, e, n;
            (t = r), (e = p.getLang());

            // console.warn(p.cookies.user);

            var user = xe.getCookie(p.cookies.user);
            var token = xe.getCookie(p.cookies.token);
            if (token && (!user || user == 'j:null')) {
              var t = new Date();
              t.setTime(new Date().getTime() + 12096e5),
                xe.setCookie(
                  p.cookies.idHint,
                  'sub=TEMP-PUUID&lang=ko&game_name=가짜이름고정&tag_line=temp-tag&id=12345&summoner=가짜이름&region=KR&tag=kr',
                  {
                    expires: t.toGMTString(),
                    path: '/',
                    domain: 'riotgames.com'
                  }
                );
            }

            var a = xe.getCookie(l.idHint);
            if (a) {
              var o = $t(a, 'tag');
              o && Kn.isRegion(o) && (t = o);
              var i = $t(a, 'lang');
              i && Kn.getLanguageNames().hasOwnProperty(i) && (e = i);
            }
            return (
              (n = Kn.localeForRegion(t, e)),
              { summoner: u.name, gameTag: u.name, region: u.region, lang: e, locale: n }
            );
          },
          getRegionName: function (t) {
            return (t = t || r), i.hasOwnProperty(t) ? i[t] : r;
          },
          getLocale: function () {
            return 'ko_KR';
          },
          getLang: function () {
            return 'ko';
          },
          getSummonerName: function () {
            return u.name;
          },
          getURL: function (t) {
            return e.urls.hasOwnProperty(t) ? e.urls[t] : '#';
          },
          setURL: function (t, n) {
            e.urls[t] = n;
          },
          supportsPlayerRegion: function () {
            var e, n, r, a, o;
            if (((e = t.supportedRegions), (n = p.getGlobalAccount()), (r = n.region), xe.isArray(e) && n.summoner)) {
              for (a = 0, o = e.length; a < o; a++) if (e[a] === r) return !0;
              return !1;
            }
            return !0;
          },
          getPvpnetId: function () {
            var t = xe.getCookie(p.cookies.token),
              e = xe.getCookie(p.cookies.userid);
            return t && e;
          },
          isEmail: function (t) {
            var e = /^([^@\s\t]+@[^@\s\t\.]+(\.[^@\s\t\.]*)?\.[^@\s\t\.]+)$/;
            return e.test(t);
          },
          getIDHint: function () {
            var t = xe.getCookie(l.idHint);
            return !!t && t;
          },
          isRegionlessAccount: function () {
            var t = p.getIDHint();
            if (!t) return !1;
            var e = $t(t, 'region');
            return !e;
          },
          initializeAuthState: function () {
            u = Object.assign({}, c);

            var t = xe.getCookie(l.idHint);
            if (t) {
              u.isAuthenticated = !0;
              var e = $t(t, 'game_name');
              e && ((e = decodeURIComponent(e.replace(/\+/g, ' ').replace(/[<">]/g, ''))), (u.name = e));
              var n = $t(t, 'tag');
              n && Kn.isRegion(n) && (u.region = n);
              var r = $t(t, 'tag_line');
              return r && (u.tag = r), u;
            }
          },
          getAuthState: function () {
            return u;
          },
          __setAuthState: function (t) {
            if (!t || 'object' !== A(t)) throw new Error('Argument passed must be an object.');
            for (var e = Object.keys(u), n = ['isAuthenticated'], r = Object.keys(t), a = 0; a < r.length; a++)
              if (!e.includes(r[a])) throw new Error("Invalid property '" + r[a] + "'");
            for (var a = 0; a < n.length; a++)
              if (!r.includes(n[a])) throw new Error("Missing required property '" + n[a] + "'");
            return (u = Object.assign(c, t));
          }
        };
      return p;
    },
    dr = { invalidSummonerName: 'j:null' },
    fr =
      (dr.invalidSummonerName,
      function (t, e) {
        var n = xe.getCurrentDomain(),
          r = 'ko_KR',
          a = 'kr',
          o = {
            ensurePVPNetCookies: function () {
              o.ensureLangCookie(), o.ensureRegionCookie();
            },
            ensureLangCookie: function () {
              if (!xe.getCookie(e.cookies.lang)) {
                var t = new Date();
                t.setTime(new Date().getTime() + 12096e5),
                  xe.setCookie(e.cookies.lang, r, { expires: t.toGMTString(), path: '/', domain: n });
              }
            },
            ensureRegionCookie: function () {
              if (!xe.getCookie(e.cookies.region)) {
                var t = new Date();
                t.setTime(new Date().getTime() + 12096e5),
                  xe.setCookie(e.cookies.region, a, { expires: t.toGMTString(), path: '/', domain: n });
              }
            },
            accountComponentProps: function () {
              var t = { player: null, extensions: e, anonymousLinksOverride: !1 },
                n = window.RiotBar.data.account.anonymousLinks;
              window.RiotBar.config &&
                window.RiotBar.config.account &&
                window.RiotBar.config.account.anonymousLinks &&
                ((n = window.RiotBar.config.account.anonymousLinks), (t.anonymousLinksOverride = !0)),
                (t.anonymousLinks = n);
              var r = e.getGlobalAccount(),
                a = r.summoner;
              if (a !== !1) {
                var o = r.region;
                (t.player = {
                  gameName: a,
                  region: e.getRegionName(o),
                  avatarUrl: 'https://avatar.leagueoflegends.com/' + o + '/' + encodeURIComponent(a) + '.png',
                  summonerName: a
                }),
                  (a !== dr.invalidSummonerName && '' !== a) || (t.player.summonerName = '내 정보');
              }
              return t;
            }
          };
        return o;
      });
  (window.pvpnet_localizations = window.pvpnet_localizations || {
    login: '로그인',
    logout: '로그아웃',
    support: '고객지원',
    register: '회원가입'
  }),
    (window.sw_cfg = window.sw_cfg || { regions: { kr: { languages: ['ko_KR'] } } });
  var hr = function (t) {
      var e = {
        pvpnetMenus: {
          show: function (e) {
            return 'login' == e
              ? t.login()
              : 'logout' == e
              ? t.logout()
              : 'recoverPassword' == e
              ? t.recoverPassword()
              : 'recoverUsername' == e
              ? t.recoverUsername()
              : 'management' == e
              ? t.management()
              : void 0;
          },
          showAndGoTo: function () {
            return t.management();
          }
        },
        getLang: function () {
          return t.getLocale();
        },
        setLang: function () {
          console.log('Warning: changing locale is not supported.');
        },
        getRegion: function () {
          return t.getRegion();
        },
        setRegion: function () {
          console.log('Warning: changing region is not supported.');
        },
        getAvailableLanguages: function () {},
        getSummonerName: function () {
          return t.getSummonerName();
        },
        isEmail: function (e) {
          return t.isEmail(e);
        },
        formatLanguageCode: function () {
          return t.getLocale();
        },
        cookies: {
          token: 'PVPNET_TOKEN',
          user: 'PVPNET_ACCT',
          lang: 'PVPNET_LANG',
          region: 'PVPNET_REGION',
          userid: 'PVPNET_ID'
        }
      };
      return e;
    },
    br = F(ur),
    mr = S(function (t) {
      var e = null,
        n = (t.exports = {
          config: {},
          styles: br,
          defaults: {
            authMode: 'prod',
            enabled: !0,
            currentDomain: null,
            utilityLinks: !1,
            returnUrl: null,
            boardsProfileLink: !1,
            supportedRegions: ['kr'],
            loginVariant: 'text',
            initializeAuthState: !0
          },
          hasLoadedDeferredScript: !1,
          preInit: function (t) {
            var e = window.RiotBar.data.account;
            (n.extensions = pr(t, e)),
              (this.accountUtils = fr(t, n.extensions)),
              (window.RiotBar.__accountUtils = this.accountUtils),
              (n.extensions.setAuthState = this.setAuthState.bind(this)),
              (this.initializeDeferredScripts = this.initializeDeferredScripts.bind(this)),
              Wn.addPluginExtensions(RiotBar, 'account', n);
          },
          init: function (t, r, a) {
            var o = this.accountUtils;
            (n.config = t),
              (e = hr(n.extensions)),
              void 0 === window.Riot && (window.Riot = {}),
              void 0 === window.Riot.Sandworm && (window.Riot.Sandworm = e),
              o.ensurePVPNetCookies(),
              t.initializeAuthState && (n.extensions.initializeAuthState(), document.dispatchEvent(zn)),
              this.initializeDeferredScripts();
          },
          setAuthState: function (t) {
            n.extensions.__setAuthState(t),
              t.isAuthenticated && this.initializeDeferredScripts(),
              document.dispatchEvent(zn);
          },
          initializeDeferredScripts: function () {
            var t = this,
              e = n.extensions.getAuthState();
            if (e.isAuthenticated !== !1 && !this.hasLoadedDeferredScript) {
              var r = n.extensions.getGlobalAccount();
              xe.ensureScript('RiotBar.plugins.accountDeferred', {
                url: 'https://cdn.i.leagueoflegends.com/riotbar/staging/master/' + r.locale + '-defer-account.js'
              }).then(function () {
                var e = window.RiotBar.plugins.accountDeferred,
                  a = [];
                e.init(n.config, { account: n.extensions, player: r }),
                  Wn.getPluginStyles(a, e),
                  xe.appendStyles(a.join('')),
                  (t.hasLoadedDeferredScript = !0),
                  document.dispatchEvent(zn);
              });
            }
          }
        });
    }),
    gr =
      (mr.config,
      mr.styles,
      mr.defaults,
      mr.hasLoadedDeferredScript,
      mr.preInit,
      mr.init,
      mr.setAuthState,
      mr.initializeDeferredScripts,
      '/* Breakpoints */\n/* Common Mixins */\n#riotbar-service-status {\n  float: left;\n  margin: 24px 10px 10px 0;\n  position: relative;\n}\n#riotbar-service-status:lang(ar) {\n  direction: rtl;\n}\n\n.service-status-indicator {\n  position: relative;\n}\n\n#riotbar-service-status-icon {\n  margin-top: 3px;\n  cursor: pointer;\n  text-indent: -9999px;\n  text-align: left;\n  overflow: hidden;\n  width: 24px;\n  height: 24px;\n}\n#riotbar-service-status-icon:lang(ar) {\n  text-align: right;\n}\n#riotbar-service-status-icon svg.base {\n  display: block;\n}\n#riotbar-service-status-icon svg.hover {\n  display: none;\n}\n#riotbar-service-status-icon:hover svg.base {\n  display: none;\n}\n#riotbar-service-status-icon:hover svg.hover {\n  display: block;\n}\n\n#riotbar-service-status-messages {\n  position: absolute;\n  z-index: 10;\n  display: none;\n  width: 350px;\n  right: -120px;\n  top: 48px;\n  border-radius: 5px;\n  background-color: #111111;\n  border: 1px solid #F9F9F9;\n  box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.3), -1px 1px 10px 0px rgba(0, 0, 0, 0.8);\n  color: #ffffff;\n}\n#riotbar-service-status-messages.active {\n  display: block;\n}\n#riotbar-service-status-messages .status-message {\n  border-bottom: 1px solid #4A5355;\n  border-bottom: 1px solid rgba(47, 96, 109, 0.3);\n  padding: 10px;\n  position: relative;\n}\n#riotbar-service-status-messages .status-message:last-child {\n  border-bottom: none;\n}\n#riotbar-service-status-messages .status-message.closed .message-toggle {\n  transform: rotate(180deg);\n}\n#riotbar-service-status-messages .status-message.closed .message-toggle svg {\n  margin-bottom: 5px;\n}\n#riotbar-service-status-messages .status-message.closed .message-text {\n  display: none;\n}\n#riotbar-service-status-messages .status-message .message-summary {\n  cursor: pointer;\n  position: relative;\n}\n#riotbar-service-status-messages .status-message .message-toggle {\n  position: absolute;\n  top: 2px;\n  left: 0px;\n  height: 16px;\n  width: 16px;\n  border-radius: 10px;\n  border: 1px solid #F9F9F9;\n  border-color: #F9F9F9;\n}\n#riotbar-service-status-messages .status-message .message-toggle svg {\n  margin: 5px 3px;\n}\n#riotbar-service-status-messages .status-message .severity-indicator {\n  position: absolute;\n  top: -5px;\n  right: -5px;\n  height: 30px;\n  width: 30px;\n  -webkit-transform: scale(0.75, 0.75);\n  -moz-transform: scale(0.75, 0.75);\n  -ms-transform: scale(0.75, 0.75);\n  -o-transform: scale(0.75, 0.75);\n  transform: scale(0.75, 0.75);\n}\n#riotbar-service-status-messages .status-message .message-title {\n  margin: 0 30px;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  color: #E4DFD3;\n  font-weight: bold;\n}\n#riotbar-service-status-messages .status-message a.message-text {\n  display: block;\n  margin: 4px 10px 0 30px;\n  color: #F9F9F9;\n  /* These custom font references are actually loaded in when the `customFonts` plugin is enabled */\n  /* Main font by default */\n  font-family: "FF Mark W05";\n  font-weight: 400;\n  font-size: 12px;\n  line-height: 1.4em;\n}\n#riotbar-service-status-messages .status-message a.message-text:lang(ar) {\n  font-family: "Cairo", Tahoma, sans-serif;\n}\n#riotbar-service-status-messages .status-message a.message-text:lang(ru) {\n  font-family: "Neue Frutiger World W05", Tahoma, sans-serif;\n}\n#riotbar-service-status-messages .status-message a.message-text:lang(ko), #riotbar-service-status-messages .status-message a.message-text:lang(kr) {\n  font-family: "RixSGo", Tahoma, sans-serif;\n}\n\n#riotbar-account-anonymous #riotbar-service-status {\n  top: 9px;\n}'),
    vr = Object.freeze({ default: gr }),
    _r = function (t) {
      var e = {
        fetchStatus: function () {
          var n = t.statusUrl;
          try {
            var r = xe.createCORSRequest('GET', n);
            if (!r) return;
            (r.onload = e.handleStatusResponse), r.send();
          } catch (a) {}
        },
        handleStatusResponse: function () {
          var t = this;
          try {
            var n = JSON.parse(t.responseText);
            n.messages &&
              n.messages.length > 0 &&
              ((n = e.processData(n)), window.RiotBar.__internalSetServiceStatusPacket(n));
          } catch (r) {}
        },
        processData: function (t) {
          return (
            (t.summarySeverity = 'info'),
            t.messages &&
              t.messages.length &&
              t.messages.forEach(function (n) {
                e.processTranslations(n), (t.summarySeverity = e.moreSevere(t.summarySeverity, n.severity));
              }),
            t
          );
        },
        moreSevere: function (t, e) {
          var n = { info: 1, warn: 2, error: 3 };
          return n[t] > n[e] ? t : e;
        },
        processTranslations: function (t) {
          var e = 'ko_KR';
          if (Array.isArray(t.translations))
            for (var n = 0, r = t.translations.length; n < r; n++)
              t.translations[n].locale === e && (t.content = t.translations[n].content);
        },
        makeToggleable: function (t) {
          var e = t.getElementsByClassName('message-summary')[0];
          xe.addEvent(e, 'click', function () {
            xe.toggleClass(t, 'closed');
          });
        }
      };
      return e;
    },
    wr = F(vr),
    yr = S(function (t) {
      var e = (t.exports = {
        config: {},
        styles: wr,
        defaults: { enabled: !0, statusUrl: '//status.leagueoflegends.com/shards/kr/summary' },
        init: function (t, n) {
          (e.config = t), (e.utils = _r(e.config)), e.utils.fetchStatus();
        }
      });
    }),
    kr =
      (yr.config,
      yr.styles,
      yr.defaults,
      yr.init,
      S(function (t) {
        t.exports = {
          config: {},
          styles: null,
          defaults: {
            enabled: !0,
            jQueryJS: 'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            apolloWidgetsJS: 'https://cdn.leagueoflegends.com/apollo/widgets/1.0.14/apollo.min.js',
            notificationsServiceUrl: 'https://notifications.leagueoflegends.com/api/1.0/',
            updatesCountEnabled: !0,
            apolloServiceUrl: 'https://apollo.na.leagueoflegends.com/apollo/',
            boardsUrl: 'https://boards.na.leagueoflegends.com/'
          },
          init: function (t, e) {
            return;
          }
        };
      })),
    xr =
      (kr.config,
      kr.styles,
      kr.defaults,
      kr.init,
      '/* Breakpoints */\n/* Common Mixins */\n.localization-management-list {\n  /* These custom font references are actually loaded in when the `customFonts` plugin is enabled */\n  /* Main font by default */\n  font-family: "FF Mark W05";\n  display: none;\n  width: 240px;\n  background-color: #F9F9F9;\n  border-radius: 4px;\n}\n.localization-management-list:lang(ar) {\n  font-family: "Cairo", Tahoma, sans-serif;\n}\n.localization-management-list:lang(ru) {\n  font-family: "Neue Frutiger World W05", Tahoma, sans-serif;\n}\n.localization-management-list:lang(ko), .localization-management-list:lang(kr) {\n  font-family: "RixSGo", Tahoma, sans-serif;\n}\n.localization-management-list.active {\n  display: block;\n}\n.localization-management-list ul {\n  margin: 0;\n  padding: 0;\n  text-align: left;\n  font-size: 12px;\n  max-height: 360px;\n  overflow-x: auto;\n}\n.localization-management-list ul:lang(ar) {\n  direction: rtl;\n  text-align: right;\n}\n.localization-management-list ul li {\n  display: block;\n  height: 40px;\n  line-height: 40px;\n  padding: 0;\n  cursor: pointer;\n}\n.localization-management-list ul li .lang-name {\n  text-transform: uppercase;\n  display: inline-block;\n  width: 20px;\n}\n.localization-management-list ul li a, .localization-management-list ul li a:visited, .localization-management-list ul li a:link {\n  transition: color 0.3s linear;\n  position: relative;\n  display: inline-block;\n  text-decoration: none;\n  color: #999999;\n  width: 100%;\n  padding: 0 0 0 20px;\n}\n.localization-management-list ul li a:lang(ar), .localization-management-list ul li a:visited:lang(ar), .localization-management-list ul li a:link:lang(ar) {\n  padding: 0 20px 0 0;\n}\n.localization-management-list ul li a svg, .localization-management-list ul li a:visited svg, .localization-management-list ul li a:link svg {\n  position: absolute;\n  top: 35%;\n  right: 20px;\n  width: 14px;\n  height: 12px;\n  fill: #F9F9F9;\n}\n.localization-management-list ul li a svg:lang(ar), .localization-management-list ul li a:visited svg:lang(ar), .localization-management-list ul li a:link svg:lang(ar) {\n  right: auto;\n  left: 20px;\n}\n.localization-management-list ul li:hover a {\n  color: #111111;\n}\n.localization-management-list ul li.active {\n  cursor: default;\n}\n.localization-management-list ul li.active a {\n  color: #c4202b;\n}\n\n.riotbar-locale .icon-lang-switch {\n  position: relative;\n  /* These custom font references are actually loaded in when the `customFonts` plugin is enabled */\n  /* Main font by default */\n  font-family: "FF Mark W05";\n}\n.riotbar-locale .icon-lang-switch:lang(ar) {\n  font-family: "Cairo", Tahoma, sans-serif;\n}\n.riotbar-locale .icon-lang-switch:lang(ru) {\n  font-family: "Neue Frutiger World W05", Tahoma, sans-serif;\n}\n.riotbar-locale .icon-lang-switch:lang(ko), .riotbar-locale .icon-lang-switch:lang(kr) {\n  font-family: "RixSGo", Tahoma, sans-serif;\n}\n.riotbar-locale .icon-lang-switch .lang-switch-dropdown:before {\n  content: " ";\n  height: 0;\n  position: absolute;\n  width: 0;\n  left: 113px;\n  top: -20px;\n  border: 10px solid transparent;\n  border-bottom-color: #F9F9F9;\n}\n@media (max-width: 1024px) {\n  .riotbar-locale .icon-lang-switch .lang-switch-dropdown:before {\n    left: 153px;\n  }\n}\n.riotbar-locale .icon-lang-switch .lang-switch-dropdown {\n  position: absolute;\n  top: 90px;\n  left: -115px;\n  padding: 8px 0 8px 8px;\n}\n@media (max-width: 1024px) {\n  .riotbar-locale .icon-lang-switch .lang-switch-dropdown {\n    left: -155px;\n  }\n}\n.sideMenuIcons .riotbar-locale .icon-lang-switch .lang-switch-dropdown {\n  top: 50px;\n  right: 0;\n  left: unset;\n}\n.sideMenuIcons .riotbar-locale .icon-lang-switch .lang-switch-dropdown:before {\n  left: unset;\n  right: 20px;\n  top: -20px;\n}\n.riotbar-locale:lang(ar) {\n  direction: rtl;\n}'),
    Lr = Object.freeze({
      default: xr
    }),
    Rr = Kn.getRegionNames(),
    Cr =
      (Kn.getLanguageNames(),
      function (t) {
        var e = {
          regionNames: Kn.getRegionNames(),
          languageNames: Kn.getLanguageNames(),
          allLanguagesWithRegions: Kn.getAllLanguagesWithRegions(),
          getRenderContext: function () {
            return {
              currentRegion: 'kr',
              currentLang: 'ko',
              currentLocale: 'ko_KR',
              currentRegionName: e.regionNames.kr,
              currentLanguageName: e.languageNames.ko,
              regions: Object.keys(Rr)
            };
          },
          getAllLanguagesWithRegions: function (t) {
            return Kn.getAllLanguagesWithRegions(t && 'object' === A(t) ? t.landingUrlPattern : void 0);
          },
          processRegionConfig: function () {
            var n = [];
            return (
              xe.map(t.locales, function (t, r) {
                for (var a = [], o = 0; o < r.length; o++) a[o] = e.processLangConfig(r[o], t);
                n.push({ id: t, regionName: e.regionNames[t], locales: a });
              }),
              n
            );
          },
          processLangConfig: function (n, r) {
            var a,
              o = t.landingUrlPattern;
            if ('string' == typeof n) {
              var i = n.substring(0, 2),
                s = o
                  .replace('{{lang}}', i)
                  .replace('{{region}}', r)
                  .replace('{{locale}}', n)
                  .replace('{{locale-hyphen}}', n.toLowerCase().replace('_', '-'));
              a = { locale: n, lang: i, languageName: e.languageNames[i], landingUrl: s };
            } else 'object' === A(n) && (a = n);
            return a;
          }
        };
        return e;
      }),
    Mr = F(Lr),
    Er =
      (Kn.getDefaultLocales(),
      {
        config: {},
        styles: Mr,
        defaults: {
          enabled: !0,
          landingUrlPattern: Kn.getDefaultLandingURLPattern(),
          languages: null,
          switcherEnabled: !0,
          supportedLocales: Kn.getDefaultLocaleCodes()
        },
        init: function (t, e) {
          this.config = t;
          var n = Cr(this.config);
          this.localeUtils = n;
        }
      }),
    Sr =
      (Er.config,
      Er.styles,
      Er.defaults,
      Er.init,
      {
        config: {},
        defaults: { enabled: !0, pingUrl: 'https://lolstatic-a.akamaihd.net/ping/ping-0.1.663.min.js', pCfg: null },
        init: function (t) {
          (this.config = t), !window.pCfg && t.pCfg && (window.pCfg = t.pCfg), xe.appendScript(this.config.pingUrl);
        }
      }),
    Fr =
      (Sr.config,
      Sr.defaults,
      Sr.init,
      {
        enabled: !1,
        disableRender: !1,
        container: { renderFooterInto: null, marginTop: null, disableFooterBackground: !1 },
        footerLinks: { enabled: !0 },
        socialLinks: { enabled: !1 }
      }),
    Ar =
      (Fr.enabled,
      Fr.disableRender,
      Fr.container,
      Fr.footerLinks,
      Fr.socialLinks,
      S(function (t) {
        t.exports = {
          initialized: !1,
          config: {},
          defaults: Fr,
          init: function (e, n, r) {
            e.enabled &&
              !t.exports.initialized &&
              xe
                .ensureScript('RiotBar.plugins.footerContent', {
                  url: 'https://cdn.i.leagueoflegends.com/riotbar/staging/master/ko_KR-defer-footer.js'
                })
                .then(function () {
                  if (!t.exports.initialized) {
                    t.exports.initialized = !0;
                    var a = window.RiotBar.plugins.footerContent;
                    a.init(e, n, r);
                  }
                });
          }
        };
      })),
    Pr = (Ar.initialized, Ar.config, Ar.defaults, Ar.init, F(se));
  if ((ce({ polyfill: !0 }), void 0 === window.RiotBar)) {
    var Tr = Wn,
      Br = xe;
    Br.appendStyles(Pr);
    var Nr = Kn,
      Hr = (window.RiotBar = {
        util: Br,
        data: {
          bar: { games: [{ id: 'lol', text: '리그 오브 레전드', url: 'https://kr.leagueoflegends.com/' }] },
          touchpoints: {
            links: [
              {
                id: 'home',
                text: '홈',
                subtext: '리그 오브 레전드 살펴보기',
                category: '리그 오브 레전드',
                url: 'https://kr.leagueoflegends.com/ko/',
                menuOnly: !0
              },
              {
                id: 'game',
                text: '게임정보',
                subtext: '리그 오브 레전드 자세히 알아보기',
                category: '리그 오브 레전드',
                url: 'https://kr.leagueoflegends.com/ko-kr/how-to-play/'
              },
              {
                id: 'champions',
                text: '챔피언',
                category: '리그 오브 레전드',
                url: 'https://kr.leagueoflegends.com/ko-kr/champions/'
              },
              {
                id: 'news',
                text: '새소식',
                subtext: '리그 오브 레전드의 소식을 전해드립니다',
                category: '리그 오브 레전드',
                url: 'https://kr.leagueoflegends.com/ko-kr/news/',
                subMenuItems: [
                  { id: 'news.all', text: '전체', url: 'https://kr.leagueoflegends.com/ko-kr/news/' },
                  {
                    id: 'news.gameupdates',
                    text: '게임 업데이트',
                    url: 'https://kr.leagueoflegends.com/ko-kr/news/game-updates'
                  },
                  { id: 'news.notices', text: '공지', url: 'https://kr.leagueoflegends.com/ko-kr/news/notices' },
                  { id: 'news.dev', text: '개발자 블로그', url: 'https://kr.leagueoflegends.com/ko-kr/news/dev' },
                  { id: 'news.lore', text: '세계관', url: 'https://kr.leagueoflegends.com/ko-kr/news/lore' },
                  { id: 'news.media', text: '미디어', url: 'https://kr.leagueoflegends.com/ko-kr/news/media' },
                  { id: 'news.merch', text: '온라인 스토어', url: 'https://kr.leagueoflegends.com/ko-kr/news/merch' },
                  { id: 'news.esports', text: 'e스포츠', url: 'https://kr.leagueoflegends.com/ko-kr/news/esports' },
                  {
                    id: 'news.riotgames',
                    text: '라이엇 게임즈',
                    url: 'https://kr.leagueoflegends.com/ko-kr/news/riot-games'
                  },
                  { id: 'news.community', text: '커뮤니티', url: 'https://kr.leagueoflegends.com/ko-kr/news/community' }
                ]
              },
              {
                id: 'patch_notes',
                text: '패치 노트',
                category: '리그 오브 레전드',
                url: 'https://kr.leagueoflegends.com/ko-kr/news/tags/patch-notes'
              },
              {
                id: 'downloadGame',
                text: '다운로드',
                category: '리그 오브 레전드',
                url: 'https://download.kr.riotgames.com/league',
                target: '_blank'
              },
              {
                id: 'esports',
                text: 'e스포츠',
                subtext: '환호와 갈채 속 영광의 순간들',
                category: '리그 오브 레전드',
                url: 'https://www.leagueoflegends.co.kr/?m=esports&mod=esports',
                target: '_blank'
              },
              {
                id: 'discover',
                text: '알아보기',
                category: '리그 오브 레전드',
                subMenuItems: [
                  {
                    id: 'discover.league_displays',
                    text: 'LoL 디스플레이',
                    url: 'https://event.leagueoflegends.co.kr/league-displays',
                    target: '_blank'
                  },
                  { id: 'store', text: '모바일 상점', url: 'https://store.leagueoflegends.co.kr', target: '_blank' }
                ]
              },
              {
                id: 'universe',
                text: '유니버스',
                subtext: '세계관 속으로 빠져 보세요',
                category: '리그 오브 레전드',
                url: 'https://universe.leagueoflegends.com/ko_KR/',
                target: '_blank'
              },
              {
                id: 'shop',
                text: '상점',
                category: '리그 오브 레전드',
                url: 'https://www.leagueoflegends.co.kr/?m=riot-store',
                target: '_blank'
              },
              {
                id: 'support',
                text: '고객지원',
                category: '리그 오브 레전드',
                url: 'https://support.riotgames.com/hc/ko',
                target: '_blank'
              }
            ]
          },
          account: {
            anonymousLinks: [
              { id: 'signup', text: '회원가입', url: 'https://signup.kr.riotgames.com' },
              { id: 'login', text: '로그인', action: 'login' }
            ],
            utilityLinks: [{ id: 'support', text: '고객지원', url: 'https://support.riotgames.com/hc/ko' }],
            urls: {
              login: 'https://{{login_prefix}}{{current_domain}}/?region=kr&lang=ko_KR&redirect_uri={{then}}',
              logout: 'https://{{login_prefix}}{{current_domain}}/{{summonerRegion}}/out?redirect_uri={{then}}',
              passwordRecovery: 'https://account.leagueoflegends.co.kr/password',
              management: 'https://signup.leagueoflegends.co.kr/',
              usernameRecovery: 'https://account.leagueoflegends.co.kr/id-finder',
              warning: !1
            }
          },
          footer: {
            riotgames: { main: { id: 'riot', text: 'Riot Games', url: 'https://www.riotgames.com/' } },
            copyright: {
              riotCopyright: '© %year% Riot Games . All rights reserved.',
              leagueCopyright:
                '™ & © %year% Riot Games, Inc.  League of Legends and all related logos, characters, names and distinctive likenesses thereof are exclusive property of Riot Games, Inc.  All Rights Reserved.',
              messages: [
                '라이엇게임즈코리아 유한회사 서울특별시 강남구 테헤란로 521 30층 (삼성동, 파르나스타워)',
                '대표자 : 오진호 | 대표전화 : 02-3454-1560 | FAX : 02-3454-1565',
                '',
                '사업자등록번호 : 120-87-68488 | {bizinfo_link} ',
                '통신판매업신고 : 2011-서울강남-02718',
                '',
                '© %year% Riot Games . All rights reserved.',
                'League of Legends and Riot games are trademarks or registered trademarks of Riot Games'
              ],
              messageLinks: [
                {
                  token: 'bizinfo_link',
                  text: '사업자정보확인',
                  url:
                    'http://ftc.go.kr/www/bizCommView.do?key=232&apv_perm_no=2011322016230202718&searchKrwd=1208768488&pageIndex=1'
                }
              ]
            },
            footerLinks: [
              { id: 'support', url: 'https://support.riotgames.com/hc/ko', text: '고객지원' },
              {
                id: 'parentSupport',
                url: 'https://parents.kr.riotgames.com',
                text: '학부모 전용 고객센터',
                target: '_blank'
              },
              { id: 'pcBang', url: 'https://pcbang.leagueoflegends.co.kr/', text: 'PC방', target: '_blank' }
            ],
            socialLinks: [
              { id: 'youtube', url: 'https://www.youtube.com/channel/UCooLkG0FfrkPBQsSuC95L6w', text: 'YouTube' },
              { id: 'facebook', url: 'https://www.facebook.com/LeagueofLegendsKor?fref=ts', text: 'Facebook' },
              { id: 'instagram', url: 'https://www.instagram.com/leagueoflegendskorea/', text: 'Instagram' }
            ],
            legalLinks: [
              { id: 'eula', url: 'https://legal.kr.riotgames.com/tos', text: '서비스 약관' },
              { id: 'privacyPolicy', url: 'https://legal.kr.riotgames.com/privacy', text: '개인정보 처리방침' },
              { id: 'gameStatus', url: 'https://status.riotgames.com/?locale=ko_KR&region=kr', text: '서비스 상태' },
              { id: 'gameTime', url: 'https://parents.kr.riotgames.com', text: '게임시간 선택제' },
              { id: 'socialResponsibility', url: 'https://www.leagueoflegends.co.kr/?m=contribute', text: '사회공헌' },
              {
                id: 'bizInfo',
                url:
                  'http://ftc.go.kr/www/bizCommView.do?key=232&apv_perm_no=2011322016230202718&searchKrwd=1208768488&pageIndex=1',
                text: '사업자정보확인'
              }
            ],
            gameRatings: [{ id: 'kr-rating', url: '', logoAltText: '' }]
          }
        },
        plugins: {
          bar: Xn,
          alerts: Qn,
          applicationSwitcher: rr,
          cookiePolicyV2: ar,
          navigation: lr,
          account: mr,
          serviceStatus: yr,
          notifications: kr,
          locale: Er,
          ping: Sr,
          footer: Ar
        },
        defaults: { mobileResponsive: !0, plugins: null, onLoad: null, global: { theme: 'lol', renderInto: null } },
        localeDefaults: { notifications: { enabled: !1 } },
        config: {},
        show: function (t) {
          function e(t) {
            var e = Hr.plugins[t],
              n = Hr.config[t],
              r = Hr.config.global;
            n.enabled &&
              (e.init(n, i, r), Tr.getPluginStyles(s, e), 'account' != t && Tr.addPluginExtensions(Hr, t, e));
          }
          t || (t = {});
          var n = t.beforeLoad;
          if (
            (n && 'function' == typeof n && t.beforeLoad(),
            (Hr.defaults = Tr.gatherDefaults(Hr.defaults, Hr.plugins)),
            (Hr.defaults = Br.deepOverride(Hr.defaults, Hr.localeDefaults, 1)),
            (Hr.config = Br.deepOverride(Hr.defaults, t, 1)),
            t.global)
          )
            for (var r in t.global)
              t.global.hasOwnProperty(r) && (Hr.config.global.hasOwnProperty(r) || (Hr.config.global[r] = t.global[r]));
          if (t.footer) {
            for (var r in t.footer)
              t.footer.hasOwnProperty(r) &&
                (Hr.defaults.footer.hasOwnProperty(r) || (Hr.defaults.footer[r] = t.footer[r]));
            Hr.config.footer = Br.deepOverride(Hr.defaults.footer, t.footer, 0);
          }
          if (t.global)
            for (var r in t.global)
              t.global.hasOwnProperty(r) && (Hr.config.global.hasOwnProperty(r) || (Hr.config.global[r] = t.global[r]));
          var a = ['locale'];
          Br.map(Hr.plugins, function (e, n) {
            if ('footer' !== e) {
              var r = Hr.defaults[e] || { enabled: !0 },
                o = t[e] || {};
              for (var i in o) r.hasOwnProperty(i) || (r[i] = o[i]);
              var s = Br.deepOverride(r, o, 0);
              (Hr.config[e] = s), a.includes(e) && (Hr.config[e].enabled = !0);
            }
          });
          var o = Hr.config.account.authMode || 'prod';
          'prod' !== o && (Nr.includeInternalRegions(), (Hr.config.locale.locales = Nr.getLocaleMappings()));
          var i = {},
            s = [];
          Br.map(Hr.config.plugins, function (e, n) {
            Hr.plugins[e] = n;
            var r = n.defaults || { enabled: !0 },
              a = t[e] || {};
            for (var o in a) r.hasOwnProperty(o) || (r[o] = a[o]);
            var i = Br.deepOverride(r, a, 0);
            Hr.config[e] = i;
          }),
            Hr.plugins.locale && e('locale'),
            Hr.plugins.account.preInit(Hr.config.account);
          var l = Hr.config.global;
          Vn(i, l),
            Br.map(Hr.plugins, function (t) {
              'locale' !== t && e(t);
            }),
            Br.appendStyles(s.join(''));
          var c = Hr.config.onLoad;
          c && 'function' == typeof c && Hr.config.onLoad();
        }
      });
    void 0 !== window.riotBarConfig && Hr.show(window.riotBarConfig);
  }
  var Ur = {};
  return Ur;
})();
