export default {
  identityAuth: {
    serviceCode: process.env.REACT_APP_IDENTITY_AUTH_SERVICE_CODE,
    env: process.env.REACT_APP_ENV
  },
  BEARERTOKEN: 'BearerToken',
  accountApiHost: process.env.REACT_APP_ACCOUNT_API_SERVER,
  PVPNET_TOKEN_KEY: process.env.REACT_APP_PVPNET_TOKEN_KEY
};
