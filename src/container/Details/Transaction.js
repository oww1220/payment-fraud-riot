import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import Moment from 'react-moment';
import numeral from 'numeral';

import { ListSelect } from 'reducer/ListReducer';
import { authCheck } from 'reducer/AuthReducer';
import ico01 from 'assets/images/item_list_ico_01.png';
import ico02 from 'assets/images/item_list_ico_04.png';
import ico03 from 'assets/images/item_list_ico_05.png';
import ico04 from 'assets/images/item_list_ico_02.png';

function Transaction(props) {
  //console.log(props);

  const goLink = (link) => () => {
    props.history.push(link);
  };

  /**
   * @param {string} agreeState 동의상태->evidence: 휴대폰 납입확인증, proxy: 대리결제 , other: 기타
   **/
  const chkAuth = (agreeState) => (e) => {
    props.authCheck(agreeState, props.history);
    e.preventDefault();
  };

  useEffect(() => {
    if (window.sessionStorage.getItem('selectList')) {
      const selectList = JSON.parse(window.sessionStorage.getItem('selectList'));
      props.ListSelect(selectList);
    }
  }, []);

  return (
    <>
      {/*}
    {Object.keys(props.selectList).length !== 0 ?
    {*/}
      {props.paymentState ? (
        <div className="inner">
          <div className="col-cont-tit-box">
            <p className="col-cont-tit">아래에서 해당하는 항목을 선택하세요.</p>
            <p className="col-cont-txt">
              {<Moment format="YYYY-MM-DD">{props.selectList.txDate}</Moment>}에 휴대폰 번호 {props.selectList.phone}
              으로 결제된 {numeral(props.selectList.unpaidAmount).format('0,0')}원중에서
              <br /> {numeral(props.selectList.chargeAmount).format('0,0')}원이 미납된 건입니다.
            </p>
          </div>
          <ul className="col-cont-item-list-box">
            <li>
              <a href="#" onClick={chkAuth('evidence')}>
                <i>
                  <img src={ico01} alt="휴대전화 요금" />
                </i>
                <p>
                  <em>
                    재화를 결제한 본인 명의 <br />
                    휴대폰 요금이 <br />
                    미납되었습니다.
                  </em>
                </p>
                <p>
                  본인 미납은 해당 휴대전화 통신
                  <br />
                  사에 미납 요금을 납부하신 후 본 <br />
                  절차에 따라 납부확인서를 보내
                  <br />
                  주시면, 계정 잠금을 해제하실 수 <br />
                  있습니다.
                </p>
              </a>
            </li>
            <li>
              <Link to="/Details/ProxyPay">
                <i>
                  <img src={ico02} alt="대리 결제 이용 (증빙 있음)" />
                </i>
                <p>
                  <em>
                    비인가 대리 충전업체를 <br />
                    이용하였고
                    <br />
                    거래 증빙이 있습니다.
                  </em>
                </p>
                <p>
                  본 절차에 따라 관련 내용 동의 및<br />
                  증빙자료 제출 이후에 미납요금까지
                  <br />
                  납부하시면, 계정잠금을 해제하실 수<br />
                  있습니다. 단, 서비스 약관 및 운영정책에
                  <br />
                  따라 7일간 게임이용이 제한됩니다.
                </p>
              </Link>
            </li>
            <li>
              <Link to="/Details/ProxyNoEvidence">
                <i>
                  <img src={ico03} alt="대리 결제 이용 (증빙 없음)" />
                </i>
                <p>
                  <em>
                    비인가 대리 충전업체를 <br />
                    이용하였으나
                    <br />
                    거래 증빙이 없습니다.
                  </em>
                </p>
                <p>
                  거래 증빙이 없더라도 미납요금을
                  <br />
                  납부하시고 계정잠금을 해제하실 수<br />
                  있습니다. 단, 서비스 약관 및 운영정책에 <br />
                  따라 7일간 게임이용이 제한됩니다.
                </p>
              </Link>
            </li>
            <li>
              <a href="#" onClick={chkAuth('other')}>
                <i>
                  <img src={ico04} alt="기타" />
                </i>
                <p>
                  <em>결제에 대한 증빙이 없습니다.</em>
                </p>
                <p>
                  기타 사유 (본인이 미납하였으나 납부
                  <br />
                  확인서를 발급받지 못하는 경우,
                  <br />
                  지인의 휴대전화로 결제하였으나
                  <br />
                  요금이 미납된 경우 등)로 미납된
                  <br />
                  경우, 본 절차에 따라 미납요금 전액을
                  <br />
                  납부하고 계정잠금을 해제하실 수<br />
                  있습니다.
                </p>
              </a>
            </li>
          </ul>
          <div className="col-cont-btn-box">
            <button type="button" className="col-cont-btn" onClick={goLink('/Details')}>
              이전으로 돌아가기
            </button>
          </div>
        </div>
      ) : (
        <Redirect to="/" />
      )}
    </>
  );
}

//export default Transaction;
export default connect(
  ({ ListReducer, AuthReducer, PageStateReducer }) => ({
    selectList: ListReducer.selectList,
    loginState: AuthReducer.loginState,
    paymentState: PageStateReducer.paymentState
  }),
  (dispatch) => ({
    ListSelect: bindActionCreators(ListSelect, dispatch),
    authCheck: bindActionCreators(authCheck, dispatch)
  })
)(Transaction);
