import React, { useEffect } from 'react';

const DetailListRowTooltip = (props) => {
  const { data } = props;

  return <p>{data}</p>;
};

export default DetailListRowTooltip;
