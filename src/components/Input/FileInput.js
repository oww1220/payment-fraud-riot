import React, { useEffect, useState } from 'react';
import cx from 'classnames';

import 'assets/scss/contents/Input/fileInput.scss';

function FileInput(props) {
  const { name, register, getValues, setValue, errors } = props;
  const [inputFile, setInputFile] = useState([]);

  const hanleChange = (e) => {
    //console.log(getValues(name));
    //getValues(name);
    setInputFile(getValues(name));
  };
  const deleteFile = (e) => {
    setValue(name, null);
    setInputFile([]);
    e.stopPropagation();
  };

  return (
    <div className="input_file">
      <div className="input_file_in">
        <input
          type="file"
          name={name}
          id={name}
          ref={register({
            required: '파일을 첨부해 주세요!',
            pattern: {
              value: /(.*?)\.(jpg|jpeg|png|zip|7z|rar)$/,
              message: '파일 형식이 틀립니다.'
            },
            validate: {
              maxSize: (value) => {
                const maxSize = 30 * 1024 * 1024;
                const isFileTooLarge = value.length > 0 && value[0].size > maxSize;
                return !isFileTooLarge || '용량이 너무 큽니다.';
              }
            }
          })}
          onChange={hanleChange}
        />
        <label htmlFor={name} className={cx(['col-cont-btn', 'type-02'], { on: !(inputFile.length && !errors[name]) })}>
          첨부하기
        </label>

        <div className="file_item">
          {inputFile.length && !errors[name] ? (
            <>
              <span>{inputFile[0].name}</span>
              <button type="button" className="delete" onClick={deleteFile}>
                삭제
              </button>
            </>
          ) : (
            errors[name] && <p className="err">{errors[name].message}</p>
          )}
        </div>
      </div>

      <div className="alert">
        최대 30mb 이내의 JPG, PNG 파일을 등록하실 수 있으며 파일이 여러개일 경우 .zip / .7z / .rar 과 같은 압축 파일로도
        등록 가능합니다.{' '}
      </div>
    </div>
  );
}

export default FileInput;
