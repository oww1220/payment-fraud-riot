import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import 'assets/scss/contents/Input/checkbox.scss';

function Checkbox({ name, number, span, defaultValue, register, value }) {
  return (
    <div className="input_c">
      <input
        type="checkbox"
        name={name}
        id={number ? name + '-' + number : name}
        value={value}
        ref={register}
        defaultChecked={defaultValue}
      />
      <label htmlFor={number ? name + '-' + number : name}>{span}</label>
    </div>
  );
}

export default Checkbox;
